﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EIS.Net.Repository;
using MySql.Data.MySqlClient;

namespace EIS.Net
{
    public partial class frmTKX : Form
    {
        public frmTKX()
        {
            InitializeComponent();
        }
        EISRepository Fungsi = new EISRepository();
        private void nsTheme1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void nsTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCari_Click(object sender, EventArgs e)
        {
            OpenFileDialog OP = new OpenFileDialog();
            OP.Filter = "CSV Files (.CSV)|*.CSV";
            OP.FilterIndex = 1;
            //bool ? userClickedOK = OP.ShowDialog();
            DialogResult result = OP.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                txtFileTkx.Text = OP.FileName;
            }
        }

        private void btnProses_Click(object sender, EventArgs e)
        {
            try
            {
                if (Fungsi.Konversi_CSV("TKX", txtFileTkx.Text, ",", Fungsi.Periode))
                {
                    //Fungsi.Pesan("KONVERSI OK", "INF");
                    using (DataTable dt = Fungsi.GetDataTable("SELECT * FROM TKX" + Fungsi.Periode + "_TEMP"))
                    {
                        MessageBox.Show(dt.Rows.Count.ToString());
                        progressBar1.Value = 0;
                        progressBar1.Maximum = dt.Rows.Count;
                       
                        if (Fungsi.db.State != ConnectionState.Open)
                        {
                            Fungsi.db.Open();
                        }
                        foreach (DataRow dr in dt.Rows)
                        {
                            progressBar1.Value += 1;
                            lblProses.Text = dr["KDTK"].ToString() + "-" + dr["NAMA"].ToString();
                            lblProses.Refresh();
                            Application.DoEvents();
                            Fungsi.SQL = "INSERT INTO MTOKO" + Fungsi.Periode + " (TOKO_CODE, TYPE_TOKO, NAMA_TOKO, ALAMAT, KOTA, JENIS, TGL_TUTUP, TGL_BUKA, STATUS, KODE_CID)";
                            Fungsi.SQL += "VALUES (@TOKO_CODE, @TYPE_TOKO, @NAMA_TOKO, @ALAMAT, @KOTA, @JENIS, @TGL_TUTUP, @TGL_BUKA, @STATUS, @KODE_CID) ";
                            Fungsi.SQL += "ON DUPLICATE KEY UPDATE TYPE_TOKO=@TYPE_TOKO, NAMA_TOKO=@NAMA_TOKO, ALAMAT=@ALAMAT, KOTA=@KOTA, JENIS=@JENIS, TGL_TUTUP=@TGL_TUTUP, TGL_BUKA=@TGL_BUKA, STATUS=@STATUS, KODE_CID=@KODE_CID;";
                            Fungsi.cmd = new MySqlCommand(Fungsi.SQL, Fungsi.db);
                            string Jenis = dr["KDTK"].ToString().Substring(0, 1);
                            if (Jenis == "T")
                            {
                                Jenis = "0";
                            }
                            else if (Jenis == "F")
                            {
                                Jenis = "1";
                            }
                            else if (Jenis == "R")
                            {
                                Jenis = "2";
                            }
                            Fungsi.cmd.Parameters.AddWithValue("@TOKO_CODE", dr["KDTK"].ToString());
                            Fungsi.cmd.Parameters.AddWithValue("@TYPE_TOKO", dr["TYPE_TOKO"].ToString());
                            Fungsi.cmd.Parameters.AddWithValue("@NAMA_TOKO", dr["KDTK"].ToString() + "-" + dr["NAMA"].ToString());
                            Fungsi.cmd.Parameters.AddWithValue("@ALAMAT", dr["ALMT"].ToString());
                            Fungsi.cmd.Parameters.AddWithValue("@KOTA", dr["KOTA"].ToString());
                            Fungsi.cmd.Parameters.AddWithValue("@JENIS", Jenis);
                            Fungsi.cmd.Parameters.AddWithValue("@TGL_TUTUP", null);
                            Fungsi.cmd.Parameters.AddWithValue("@TGL_BUKA", DateTime.Parse(dr["BUKA"].ToString()).ToString("yyyyMMdd"));
                            Fungsi.cmd.Parameters.AddWithValue("@STATUS", null);
                            Fungsi.cmd.Parameters.AddWithValue("@KODE_CID", dr["KODE_CID"].ToString());
                            Fungsi.cmd.ExecuteNonQuery();
                        }
                        lblProses.Text = "Upload Data TKX Beres";
                        lblProses.Refresh();
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }
    }
}
