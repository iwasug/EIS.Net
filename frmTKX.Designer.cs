﻿namespace EIS.Net
{
    partial class frmTKX
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nsTheme1 = new NSTheme();
            this.lblProses = new System.Windows.Forms.Label();
            this.progressBar1 = new NSProgressBar();
            this.btnProses = new NSButton();
            this.btnCari = new NSButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFileTkx = new NSTextBox();
            this.nsControlButton1 = new NSControlButton();
            this.nsTheme1.SuspendLayout();
            this.SuspendLayout();
            // 
            // nsTheme1
            // 
            this.nsTheme1.AccentOffset = 0;
            this.nsTheme1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.nsTheme1.BorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.nsTheme1.Colors = new Bloom[0];
            this.nsTheme1.Controls.Add(this.nsControlButton1);
            this.nsTheme1.Controls.Add(this.lblProses);
            this.nsTheme1.Controls.Add(this.progressBar1);
            this.nsTheme1.Controls.Add(this.btnProses);
            this.nsTheme1.Controls.Add(this.btnCari);
            this.nsTheme1.Controls.Add(this.label1);
            this.nsTheme1.Controls.Add(this.txtFileTkx);
            this.nsTheme1.Customization = "";
            this.nsTheme1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nsTheme1.Font = new System.Drawing.Font("Verdana", 8F);
            this.nsTheme1.Image = null;
            this.nsTheme1.Location = new System.Drawing.Point(0, 0);
            this.nsTheme1.Movable = true;
            this.nsTheme1.Name = "nsTheme1";
            this.nsTheme1.NoRounding = false;
            this.nsTheme1.Padding = new System.Windows.Forms.Padding(10, 40, 10, 10);
            this.nsTheme1.Sizable = true;
            this.nsTheme1.Size = new System.Drawing.Size(415, 145);
            this.nsTheme1.SmartBounds = true;
            this.nsTheme1.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation;
            this.nsTheme1.TabIndex = 0;
            this.nsTheme1.Text = "Upload TKX";
            this.nsTheme1.TransparencyKey = System.Drawing.Color.Empty;
            this.nsTheme1.Transparent = false;
            this.nsTheme1.Click += new System.EventHandler(this.nsTheme1_Click);
            // 
            // lblProses
            // 
            this.lblProses.AutoSize = true;
            this.lblProses.ForeColor = System.Drawing.Color.White;
            this.lblProses.Location = new System.Drawing.Point(26, 116);
            this.lblProses.Name = "lblProses";
            this.lblProses.Size = new System.Drawing.Size(12, 13);
            this.lblProses.TabIndex = 5;
            this.lblProses.Text = "-";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(29, 90);
            this.progressBar1.Maximum = 100;
            this.progressBar1.Minimum = 0;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(279, 23);
            this.progressBar1.TabIndex = 4;
            this.progressBar1.Text = "nsProgressBar1";
            this.progressBar1.Value = 0;
            // 
            // btnProses
            // 
            this.btnProses.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProses.Location = new System.Drawing.Point(327, 43);
            this.btnProses.Name = "btnProses";
            this.btnProses.Size = new System.Drawing.Size(75, 70);
            this.btnProses.TabIndex = 3;
            this.btnProses.Text = "Proses";
            this.btnProses.Click += new System.EventHandler(this.btnProses_Click);
            // 
            // btnCari
            // 
            this.btnCari.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCari.Location = new System.Drawing.Point(192, 61);
            this.btnCari.Name = "btnCari";
            this.btnCari.Size = new System.Drawing.Size(53, 23);
            this.btnCari.TabIndex = 2;
            this.btnCari.Text = "...";
            this.btnCari.Click += new System.EventHandler(this.btnCari_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(26, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "File TKX :";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtFileTkx
            // 
            this.txtFileTkx.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtFileTkx.Location = new System.Drawing.Point(29, 61);
            this.txtFileTkx.MaxLength = 32767;
            this.txtFileTkx.Multiline = false;
            this.txtFileTkx.Name = "txtFileTkx";
            this.txtFileTkx.ReadOnly = true;
            this.txtFileTkx.Size = new System.Drawing.Size(157, 23);
            this.txtFileTkx.TabIndex = 0;
            this.txtFileTkx.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtFileTkx.UseSystemPasswordChar = false;
            this.txtFileTkx.TextChanged += new System.EventHandler(this.nsTextBox1_TextChanged);
            // 
            // nsControlButton1
            // 
            this.nsControlButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nsControlButton1.ControlButton = NSControlButton.Button.Close;
            this.nsControlButton1.Location = new System.Drawing.Point(387, 5);
            this.nsControlButton1.Margin = new System.Windows.Forms.Padding(0);
            this.nsControlButton1.MaximumSize = new System.Drawing.Size(18, 20);
            this.nsControlButton1.MinimumSize = new System.Drawing.Size(18, 20);
            this.nsControlButton1.Name = "nsControlButton1";
            this.nsControlButton1.Size = new System.Drawing.Size(18, 20);
            this.nsControlButton1.TabIndex = 6;
            this.nsControlButton1.Text = "nsControlButton1";
            // 
            // frmTKX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 145);
            this.Controls.Add(this.nsTheme1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmTKX";
            this.Text = "frmTKX";
            this.nsTheme1.ResumeLayout(false);
            this.nsTheme1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private NSTheme nsTheme1;
        private NSTextBox txtFileTkx;
        private System.Windows.Forms.Label label1;
        private NSButton btnCari;
        private NSButton btnProses;
        private System.Windows.Forms.Label lblProses;
        private NSProgressBar progressBar1;
        private NSControlButton nsControlButton1;
    }
}