﻿namespace EIS.Net
{
    partial class frmLoading
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.nsTheme1 = new NSTheme();
            this.nsLabel1 = new NSLabel();
            this.nsLabel3 = new NSLabel();
            this.nsLabel2 = new NSLabel();
            this.lblVersi = new NSLabel();
            this.pgBar = new NSProgressBar();
            this.nsTheme1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // nsTheme1
            // 
            this.nsTheme1.AccentOffset = 0;
            this.nsTheme1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.nsTheme1.BorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.nsTheme1.Colors = new Bloom[0];
            this.nsTheme1.Controls.Add(this.nsLabel1);
            this.nsTheme1.Controls.Add(this.nsLabel3);
            this.nsTheme1.Controls.Add(this.nsLabel2);
            this.nsTheme1.Controls.Add(this.lblVersi);
            this.nsTheme1.Controls.Add(this.pgBar);
            this.nsTheme1.Customization = "";
            this.nsTheme1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nsTheme1.Font = new System.Drawing.Font("Verdana", 8F);
            this.nsTheme1.Image = null;
            this.nsTheme1.Location = new System.Drawing.Point(0, 0);
            this.nsTheme1.Movable = true;
            this.nsTheme1.Name = "nsTheme1";
            this.nsTheme1.NoRounding = false;
            this.nsTheme1.Padding = new System.Windows.Forms.Padding(10, 40, 10, 10);
            this.nsTheme1.Sizable = true;
            this.nsTheme1.Size = new System.Drawing.Size(656, 247);
            this.nsTheme1.SmartBounds = true;
            this.nsTheme1.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.nsTheme1.TabIndex = 0;
            this.nsTheme1.Text = "nsTheme1";
            this.nsTheme1.TransparencyKey = System.Drawing.Color.Empty;
            this.nsTheme1.Transparent = false;
            // 
            // nsLabel1
            // 
            this.nsLabel1.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel1.Location = new System.Drawing.Point(145, 90);
            this.nsLabel1.Name = "nsLabel1";
            this.nsLabel1.Size = new System.Drawing.Size(388, 33);
            this.nsLabel1.TabIndex = 1;
            this.nsLabel1.Text = "nsLabel1";
            this.nsLabel1.Value1 = "Excecutive Information";
            this.nsLabel1.Value2 = " System";
            // 
            // nsLabel3
            // 
            this.nsLabel3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel3.Location = new System.Drawing.Point(12, 210);
            this.nsLabel3.Name = "nsLabel3";
            this.nsLabel3.Size = new System.Drawing.Size(194, 13);
            this.nsLabel3.TabIndex = 1;
            this.nsLabel3.Text = "nsLabel1";
            this.nsLabel3.Value1 = "Loading.....";
            this.nsLabel3.Value2 = "";
            // 
            // nsLabel2
            // 
            this.nsLabel2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel2.Location = new System.Drawing.Point(455, 210);
            this.nsLabel2.Name = "nsLabel2";
            this.nsLabel2.Size = new System.Drawing.Size(194, 13);
            this.nsLabel2.TabIndex = 1;
            this.nsLabel2.Text = "nsLabel1";
            this.nsLabel2.Value1 = "Copyright © 2016 - Iwa Sugriwa";
            this.nsLabel2.Value2 = "";
            // 
            // lblVersi
            // 
            this.lblVersi.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersi.Location = new System.Drawing.Point(253, 129);
            this.lblVersi.Name = "lblVersi";
            this.lblVersi.Size = new System.Drawing.Size(126, 23);
            this.lblVersi.TabIndex = 1;
            this.lblVersi.Text = "nsLabel1";
            this.lblVersi.Value1 = "EIS.Net ";
            this.lblVersi.Value2 = " v.1.0.0.0";
            // 
            // pgBar
            // 
            this.pgBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pgBar.Location = new System.Drawing.Point(10, 226);
            this.pgBar.Maximum = 100;
            this.pgBar.Minimum = 0;
            this.pgBar.Name = "pgBar";
            this.pgBar.Size = new System.Drawing.Size(636, 11);
            this.pgBar.TabIndex = 0;
            this.pgBar.Text = "nsProgressBar1";
            this.pgBar.Value = 0;
            // 
            // frmLoading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 247);
            this.Controls.Add(this.nsTheme1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmLoading";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmLoading";
            this.nsTheme1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private NSTheme nsTheme1;
        private NSProgressBar pgBar;
        private System.Windows.Forms.Timer timer1;
        private NSLabel lblVersi;
        private NSLabel nsLabel1;
        private NSLabel nsLabel3;
        private NSLabel nsLabel2;
    }
}