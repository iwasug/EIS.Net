﻿namespace EIS.Net
{
    partial class frmSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nsTheme1 = new NSTheme();
            this.nsControlButton1 = new NSControlButton();
            this.nsGroupBox1 = new NSGroupBox();
            this.Prd = new System.Windows.Forms.DateTimePicker();
            this.btnSimpan = new NSButton();
            this.txtCabang = new NSTextBox();
            this.txtIpAspera = new NSTextBox();
            this.txtInterval = new System.Windows.Forms.NumericUpDown();
            this.txtIpFTPDC = new NSTextBox();
            this.txtOraDC = new NSTextBox();
            this.txtFolderTemp = new NSTextBox();
            this.nsButton2 = new NSButton();
            this.nsButton1 = new NSButton();
            this.nsLabel8 = new NSLabel();
            this.txtFolderHarian = new NSTextBox();
            this.nsLabel4 = new NSLabel();
            this.nsLabel2 = new NSLabel();
            this.nsLabel1 = new NSLabel();
            this.nsLabel7 = new NSLabel();
            this.nsLabel6 = new NSLabel();
            this.nsLabel5 = new NSLabel();
            this.nsLabel3 = new NSLabel();
            this.nsTheme1.SuspendLayout();
            this.nsGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInterval)).BeginInit();
            this.SuspendLayout();
            // 
            // nsTheme1
            // 
            this.nsTheme1.AccentOffset = 0;
            this.nsTheme1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.nsTheme1.BorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.nsTheme1.Colors = new Bloom[0];
            this.nsTheme1.Controls.Add(this.nsControlButton1);
            this.nsTheme1.Controls.Add(this.nsGroupBox1);
            this.nsTheme1.Customization = "";
            this.nsTheme1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nsTheme1.Font = new System.Drawing.Font("Verdana", 8F);
            this.nsTheme1.Image = null;
            this.nsTheme1.Location = new System.Drawing.Point(0, 0);
            this.nsTheme1.Movable = true;
            this.nsTheme1.Name = "nsTheme1";
            this.nsTheme1.NoRounding = false;
            this.nsTheme1.Padding = new System.Windows.Forms.Padding(10, 40, 10, 10);
            this.nsTheme1.Sizable = true;
            this.nsTheme1.Size = new System.Drawing.Size(452, 419);
            this.nsTheme1.SmartBounds = true;
            this.nsTheme1.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.nsTheme1.TabIndex = 0;
            this.nsTheme1.Text = "Setup";
            this.nsTheme1.TransparencyKey = System.Drawing.Color.Empty;
            this.nsTheme1.Transparent = false;
            // 
            // nsControlButton1
            // 
            this.nsControlButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nsControlButton1.ControlButton = NSControlButton.Button.Close;
            this.nsControlButton1.Location = new System.Drawing.Point(425, 3);
            this.nsControlButton1.Margin = new System.Windows.Forms.Padding(0);
            this.nsControlButton1.MaximumSize = new System.Drawing.Size(18, 20);
            this.nsControlButton1.MinimumSize = new System.Drawing.Size(18, 20);
            this.nsControlButton1.Name = "nsControlButton1";
            this.nsControlButton1.Size = new System.Drawing.Size(18, 20);
            this.nsControlButton1.TabIndex = 12;
            this.nsControlButton1.Text = "nsControlButton1";
            // 
            // nsGroupBox1
            // 
            this.nsGroupBox1.Controls.Add(this.Prd);
            this.nsGroupBox1.Controls.Add(this.btnSimpan);
            this.nsGroupBox1.Controls.Add(this.txtCabang);
            this.nsGroupBox1.Controls.Add(this.txtIpAspera);
            this.nsGroupBox1.Controls.Add(this.txtInterval);
            this.nsGroupBox1.Controls.Add(this.txtIpFTPDC);
            this.nsGroupBox1.Controls.Add(this.txtOraDC);
            this.nsGroupBox1.Controls.Add(this.txtFolderTemp);
            this.nsGroupBox1.Controls.Add(this.nsButton2);
            this.nsGroupBox1.Controls.Add(this.nsButton1);
            this.nsGroupBox1.Controls.Add(this.nsLabel8);
            this.nsGroupBox1.Controls.Add(this.txtFolderHarian);
            this.nsGroupBox1.Controls.Add(this.nsLabel4);
            this.nsGroupBox1.Controls.Add(this.nsLabel2);
            this.nsGroupBox1.Controls.Add(this.nsLabel1);
            this.nsGroupBox1.Controls.Add(this.nsLabel7);
            this.nsGroupBox1.Controls.Add(this.nsLabel6);
            this.nsGroupBox1.Controls.Add(this.nsLabel5);
            this.nsGroupBox1.Controls.Add(this.nsLabel3);
            this.nsGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nsGroupBox1.DrawSeperator = false;
            this.nsGroupBox1.ForeColor = System.Drawing.Color.White;
            this.nsGroupBox1.Location = new System.Drawing.Point(10, 40);
            this.nsGroupBox1.Name = "nsGroupBox1";
            this.nsGroupBox1.Padding = new System.Windows.Forms.Padding(10, 40, 10, 10);
            this.nsGroupBox1.Size = new System.Drawing.Size(432, 369);
            this.nsGroupBox1.SubTitle = "Details";
            this.nsGroupBox1.TabIndex = 0;
            this.nsGroupBox1.Text = "nsGroupBox1";
            this.nsGroupBox1.Title = "Setup";
            this.nsGroupBox1.Click += new System.EventHandler(this.nsGroupBox1_Click);
            // 
            // Prd
            // 
            this.Prd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Prd.Location = new System.Drawing.Point(17, 159);
            this.Prd.Name = "Prd";
            this.Prd.Size = new System.Drawing.Size(99, 20);
            this.Prd.TabIndex = 12;
            // 
            // btnSimpan
            // 
            this.btnSimpan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSimpan.Location = new System.Drawing.Point(17, 313);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(402, 43);
            this.btnSimpan.TabIndex = 11;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // txtCabang
            // 
            this.txtCabang.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCabang.Location = new System.Drawing.Point(263, 155);
            this.txtCabang.MaxLength = 32767;
            this.txtCabang.Multiline = false;
            this.txtCabang.Name = "txtCabang";
            this.txtCabang.ReadOnly = false;
            this.txtCabang.Size = new System.Drawing.Size(156, 23);
            this.txtCabang.TabIndex = 10;
            this.txtCabang.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCabang.UseSystemPasswordChar = false;
            // 
            // txtIpAspera
            // 
            this.txtIpAspera.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtIpAspera.Location = new System.Drawing.Point(263, 110);
            this.txtIpAspera.MaxLength = 32767;
            this.txtIpAspera.Multiline = false;
            this.txtIpAspera.Name = "txtIpAspera";
            this.txtIpAspera.ReadOnly = false;
            this.txtIpAspera.Size = new System.Drawing.Size(156, 23);
            this.txtIpAspera.TabIndex = 9;
            this.txtIpAspera.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtIpAspera.UseSystemPasswordChar = false;
            // 
            // txtInterval
            // 
            this.txtInterval.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.txtInterval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtInterval.ForeColor = System.Drawing.Color.White;
            this.txtInterval.Location = new System.Drawing.Point(263, 62);
            this.txtInterval.Name = "txtInterval";
            this.txtInterval.Size = new System.Drawing.Size(80, 20);
            this.txtInterval.TabIndex = 8;
            // 
            // txtIpFTPDC
            // 
            this.txtIpFTPDC.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtIpFTPDC.Location = new System.Drawing.Point(17, 273);
            this.txtIpFTPDC.MaxLength = 32767;
            this.txtIpFTPDC.Multiline = true;
            this.txtIpFTPDC.Name = "txtIpFTPDC";
            this.txtIpFTPDC.ReadOnly = false;
            this.txtIpFTPDC.Size = new System.Drawing.Size(402, 25);
            this.txtIpFTPDC.TabIndex = 7;
            this.txtIpFTPDC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtIpFTPDC.UseSystemPasswordChar = false;
            this.txtIpFTPDC.TextChanged += new System.EventHandler(this.txtOraDC_TextChanged);
            // 
            // txtOraDC
            // 
            this.txtOraDC.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtOraDC.Location = new System.Drawing.Point(17, 203);
            this.txtOraDC.MaxLength = 32767;
            this.txtOraDC.Multiline = true;
            this.txtOraDC.Name = "txtOraDC";
            this.txtOraDC.ReadOnly = false;
            this.txtOraDC.Size = new System.Drawing.Size(402, 49);
            this.txtOraDC.TabIndex = 7;
            this.txtOraDC.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtOraDC.UseSystemPasswordChar = false;
            this.txtOraDC.TextChanged += new System.EventHandler(this.txtOraDC_TextChanged);
            // 
            // txtFolderTemp
            // 
            this.txtFolderTemp.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtFolderTemp.Location = new System.Drawing.Point(13, 110);
            this.txtFolderTemp.MaxLength = 32767;
            this.txtFolderTemp.Multiline = false;
            this.txtFolderTemp.Name = "txtFolderTemp";
            this.txtFolderTemp.ReadOnly = false;
            this.txtFolderTemp.Size = new System.Drawing.Size(182, 23);
            this.txtFolderTemp.TabIndex = 5;
            this.txtFolderTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtFolderTemp.UseSystemPasswordChar = false;
            // 
            // nsButton2
            // 
            this.nsButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nsButton2.Location = new System.Drawing.Point(201, 110);
            this.nsButton2.Name = "nsButton2";
            this.nsButton2.Size = new System.Drawing.Size(26, 23);
            this.nsButton2.TabIndex = 4;
            this.nsButton2.Text = "...";
            this.nsButton2.Click += new System.EventHandler(this.nsButton2_Click);
            // 
            // nsButton1
            // 
            this.nsButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nsButton1.Location = new System.Drawing.Point(201, 62);
            this.nsButton1.Name = "nsButton1";
            this.nsButton1.Size = new System.Drawing.Size(26, 23);
            this.nsButton1.TabIndex = 4;
            this.nsButton1.Text = "...";
            this.nsButton1.Click += new System.EventHandler(this.nsButton1_Click);
            // 
            // nsLabel8
            // 
            this.nsLabel8.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel8.Location = new System.Drawing.Point(17, 254);
            this.nsLabel8.Name = "nsLabel8";
            this.nsLabel8.Size = new System.Drawing.Size(143, 13);
            this.nsLabel8.TabIndex = 2;
            this.nsLabel8.Text = "nsLabel1";
            this.nsLabel8.Value1 = "Ip Ftp DC:";
            this.nsLabel8.Value2 = "";
            // 
            // txtFolderHarian
            // 
            this.txtFolderHarian.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtFolderHarian.Location = new System.Drawing.Point(13, 62);
            this.txtFolderHarian.MaxLength = 32767;
            this.txtFolderHarian.Multiline = false;
            this.txtFolderHarian.Name = "txtFolderHarian";
            this.txtFolderHarian.ReadOnly = false;
            this.txtFolderHarian.Size = new System.Drawing.Size(182, 23);
            this.txtFolderHarian.TabIndex = 3;
            this.txtFolderHarian.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtFolderHarian.UseSystemPasswordChar = false;
            // 
            // nsLabel4
            // 
            this.nsLabel4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel4.Location = new System.Drawing.Point(17, 184);
            this.nsLabel4.Name = "nsLabel4";
            this.nsLabel4.Size = new System.Drawing.Size(143, 13);
            this.nsLabel4.TabIndex = 2;
            this.nsLabel4.Text = "nsLabel1";
            this.nsLabel4.Value1 = "Koneksi Oracle DC :";
            this.nsLabel4.Value2 = "";
            // 
            // nsLabel2
            // 
            this.nsLabel2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel2.Location = new System.Drawing.Point(13, 139);
            this.nsLabel2.Name = "nsLabel2";
            this.nsLabel2.Size = new System.Drawing.Size(143, 13);
            this.nsLabel2.TabIndex = 2;
            this.nsLabel2.Text = "nsLabel1";
            this.nsLabel2.Value1 = "Periode :";
            this.nsLabel2.Value2 = "";
            // 
            // nsLabel1
            // 
            this.nsLabel1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel1.Location = new System.Drawing.Point(13, 91);
            this.nsLabel1.Name = "nsLabel1";
            this.nsLabel1.Size = new System.Drawing.Size(143, 13);
            this.nsLabel1.TabIndex = 2;
            this.nsLabel1.Text = "nsLabel1";
            this.nsLabel1.Value1 = "Folder Data Temporary :";
            this.nsLabel1.Value2 = "";
            // 
            // nsLabel7
            // 
            this.nsLabel7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel7.Location = new System.Drawing.Point(263, 91);
            this.nsLabel7.Name = "nsLabel7";
            this.nsLabel7.Size = new System.Drawing.Size(143, 13);
            this.nsLabel7.TabIndex = 2;
            this.nsLabel7.Text = "nsLabel1";
            this.nsLabel7.Value1 = "Ip Aspera :";
            this.nsLabel7.Value2 = "";
            // 
            // nsLabel6
            // 
            this.nsLabel6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel6.Location = new System.Drawing.Point(263, 43);
            this.nsLabel6.Name = "nsLabel6";
            this.nsLabel6.Size = new System.Drawing.Size(143, 13);
            this.nsLabel6.TabIndex = 2;
            this.nsLabel6.Text = "nsLabel1";
            this.nsLabel6.Value1 = "Interval Proses :";
            this.nsLabel6.Value2 = "";
            // 
            // nsLabel5
            // 
            this.nsLabel5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel5.Location = new System.Drawing.Point(263, 139);
            this.nsLabel5.Name = "nsLabel5";
            this.nsLabel5.Size = new System.Drawing.Size(143, 13);
            this.nsLabel5.TabIndex = 2;
            this.nsLabel5.Text = "nsLabel1";
            this.nsLabel5.Value1 = "Cabang :";
            this.nsLabel5.Value2 = "";
            // 
            // nsLabel3
            // 
            this.nsLabel3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel3.Location = new System.Drawing.Point(13, 43);
            this.nsLabel3.Name = "nsLabel3";
            this.nsLabel3.Size = new System.Drawing.Size(143, 13);
            this.nsLabel3.TabIndex = 2;
            this.nsLabel3.Text = "nsLabel1";
            this.nsLabel3.Value1 = "Folder Data Harian :";
            this.nsLabel3.Value2 = "";
            // 
            // frmSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 419);
            this.Controls.Add(this.nsTheme1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSetup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmSetup";
            this.nsTheme1.ResumeLayout(false);
            this.nsGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtInterval)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private NSTheme nsTheme1;
        private NSGroupBox nsGroupBox1;
        private NSLabel nsLabel3;
        private NSTextBox txtFolderHarian;
        private NSButton nsButton1;
        private NSTextBox txtFolderTemp;
        private NSLabel nsLabel1;
        private NSButton nsButton2;
        private NSTextBox txtOraDC;
        private NSLabel nsLabel4;
        private NSLabel nsLabel2;
        private NSLabel nsLabel5;
        private NSTextBox txtCabang;
        private NSTextBox txtIpAspera;
        private System.Windows.Forms.NumericUpDown txtInterval;
        private NSLabel nsLabel6;
        private NSButton btnSimpan;
        private NSControlButton nsControlButton1;
        private NSLabel nsLabel7;
        private NSTextBox txtIpFTPDC;
        private NSLabel nsLabel8;
        private System.Windows.Forms.DateTimePicker Prd;
    }
}