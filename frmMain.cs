﻿using EIS.Net.Repository;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Tamir.SharpSsh;

namespace EIS.Net
{
    public partial class frmMain : Form
    {
        EISRepository Fungsi = new EISRepository();
        private Scp sshCp;
        private bool IsProses = false;
        public frmMain()
        {
            InitializeComponent();
            this.Icon = EIS.Net.Properties.Resources.IcoEIS;
            nsTheme1.Text = Fungsi.Versi;
            timer1.Start();
            nsGroupBox2.SubTitle = "Copyright © 2016 - Iwa Sugriwa";
        }

        private void btnSetup_Click(object sender, EventArgs e)
        {
            frmSetup f = new frmSetup();
            Load_menu(f);
        }

        private void Load_menu(Form myForm)
        {
            foreach (Control p in panel1.Controls)
            {
                if(p.Name == myForm.Name)
                {
                    Log(myForm.Name + " SUDAH DI BUKA");
                    MessageBox.Show("FORM SUDAH DI BUKA","",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    return;
                }
            }
            myForm.TopLevel = false;
            myForm.AutoScroll = true;
            panel1.Controls.Add(myForm);
            myForm.FormBorderStyle = FormBorderStyle.None;
            myForm.Show();
        }

        private void Log(string Pesan)
        {
            if (txtLog.Text.Length > 2000)
            {
                txtLog.Text = "";
            }
            if (txtLog.Text.Length == 0)
                txtLog.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + Pesan;
            else
                txtLog.Text = txtLog.Text + "\r\n" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + Pesan;
        }

        private void btnProsesAll_Click(object sender, EventArgs e)
        {
            frmProsesAll f = new frmProsesAll();
            Load_menu(f);
        }

        private void bgDownHarian_DoWork(object sender, DoWorkEventArgs e)
        {
            IsProses = true;
            /*
            Fungsi.SQL = "SELECT * FROM ABSENHR" + Fungsi.Periode + " WHERE STATUS != '1' AND BUKA <= TANGGAL;";
            using (DataTable dt = Fungsi.GetDataTable(Fungsi.SQL))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    //Log("DOWNLOAD " + dr["FILEHR"].ToString());
                    DownloadHarian(dr["FILEHR"].ToString());
                }
            }
            */
            Fungsi.ProsesPBvsNPB();
            //DownloadHrn();
        }

        private void Proses()
        {

            if (!IsProses)
            {
                Log("MULAI PROSES SINKRON NPB DC");
                bgDownHarian.RunWorkerAsync();
            }
            else
            {
                Log("MASIH PROSES SINKRON NPB DC");
            }

        }

        private void DownloadHarian(string FileHr)
        {
            try
            {
                sshCp = new Scp(Fungsi.Aspera, "ftptoko");
                sshCp.Password = "xftptokox";
                sshCp.Connect();
                sshCp.Get(@"/home/ftp/" + Fungsi.Cabang.ToLower() + "/" + Fungsi.Cabang.ToUpper() + "/DATA/" + FileHr, Fungsi.FolderHarian);
            }
            catch (Exception err)
            {
                //MessageBox.Show(err.Message + Fungsi.Aspera);
                Fungsi.Log("DOWNLOAD FILE HR", FileHr, err.Message);
            }
        }

        private void DownloadHrn()
        {
            var SQL = "SELECT * FROM ABSENHR" + Fungsi.Periode + " WHERE STATUS != '1' AND BUKA <= TANGGAL;";
            try
            {
                DataTable HR = new DataTable();
                using (MySqlConnection mConnection = new MySqlConnection(Fungsi.ServerMySQL))
                {
                    if (mConnection.State != ConnectionState.Open)
                    {
                        mConnection.Open();
                    }
                    MySqlCommand cmd = new MySqlCommand(SQL, mConnection);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    da.Fill(HR);
                    foreach (DataRow dr in HR.Rows)
                    {
                        //Log("DOWNLOAD " + dr["FILEHR"].ToString());
                        DownloadHarian(dr["FILEHR"].ToString());
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!IsProses)
            {
                Log("MULAI PROSES SINKRON NPB DC");
                bgDownHarian.RunWorkerAsync();
            }
            else
            {
                Log("MASIH PROSES SINKRON NPB DC");
            }
        }

        private void bgDownHarian_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            IsProses = false;
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show(this, "APAKAH PROGRAM MAU DI TUTUP", Fungsi.Versi, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnSetupMySQL_Click(object sender, EventArgs e)
        {
            frmServer f = new frmServer();
            Load_menu(f);
        }

        private void btnImportTkx_Click(object sender, EventArgs e)
        {
            frmTKX f = new frmTKX();
            Load_menu(f);
        }

        private void btnBRD_Click(object sender, EventArgs e)
        {
            frmBRD f = new frmBRD();
            Load_menu(f);
        }

        private void nsControlButton1_Click(object sender, EventArgs e)
        {

        }
    }
}
