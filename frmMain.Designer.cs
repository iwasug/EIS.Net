﻿namespace EIS.Net
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.nsTheme1 = new NSTheme();
            this.nsGroupBox3 = new NSGroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.nsGroupBox2 = new NSGroupBox();
            this.nsGroupBox1 = new NSGroupBox();
            this.txtLog = new NSTextBox();
            this.nsLabel2 = new NSLabel();
            this.nsSeperator1 = new NSSeperator();
            this.nsTabControl1 = new NSTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnSetupMySQL = new NSButton();
            this.btnSetup = new NSButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnProsesAll = new NSButton();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnBRD = new NSButton();
            this.btnImportTkx = new NSButton();
            this.nsControlButton1 = new NSControlButton();
            this.bgDownHarian = new System.ComponentModel.BackgroundWorker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.nsTheme1.SuspendLayout();
            this.nsGroupBox3.SuspendLayout();
            this.nsGroupBox1.SuspendLayout();
            this.nsTabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // nsTheme1
            // 
            this.nsTheme1.AccentOffset = 0;
            this.nsTheme1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.nsTheme1.BorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.nsTheme1.Colors = new Bloom[0];
            this.nsTheme1.Controls.Add(this.nsGroupBox3);
            this.nsTheme1.Controls.Add(this.nsGroupBox2);
            this.nsTheme1.Controls.Add(this.nsGroupBox1);
            this.nsTheme1.Controls.Add(this.nsControlButton1);
            this.nsTheme1.Customization = "";
            this.nsTheme1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nsTheme1.Font = new System.Drawing.Font("Verdana", 8F);
            this.nsTheme1.Image = null;
            this.nsTheme1.Location = new System.Drawing.Point(0, 0);
            this.nsTheme1.Movable = true;
            this.nsTheme1.Name = "nsTheme1";
            this.nsTheme1.NoRounding = false;
            this.nsTheme1.Padding = new System.Windows.Forms.Padding(10, 40, 10, 10);
            this.nsTheme1.Sizable = true;
            this.nsTheme1.Size = new System.Drawing.Size(677, 441);
            this.nsTheme1.SmartBounds = true;
            this.nsTheme1.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.nsTheme1.TabIndex = 0;
            this.nsTheme1.Text = "nsTheme1";
            this.nsTheme1.TransparencyKey = System.Drawing.Color.Empty;
            this.nsTheme1.Transparent = false;
            // 
            // nsGroupBox3
            // 
            this.nsGroupBox3.Controls.Add(this.panel1);
            this.nsGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nsGroupBox3.DrawSeperator = false;
            this.nsGroupBox3.ForeColor = System.Drawing.Color.White;
            this.nsGroupBox3.Location = new System.Drawing.Point(246, 40);
            this.nsGroupBox3.Name = "nsGroupBox3";
            this.nsGroupBox3.Padding = new System.Windows.Forms.Padding(10, 40, 10, 10);
            this.nsGroupBox3.Size = new System.Drawing.Size(421, 370);
            this.nsGroupBox3.SubTitle = "Details";
            this.nsGroupBox3.TabIndex = 4;
            this.nsGroupBox3.Text = "nsGroupBox3";
            this.nsGroupBox3.Title = "Main";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(10, 40);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(401, 320);
            this.panel1.TabIndex = 0;
            // 
            // nsGroupBox2
            // 
            this.nsGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nsGroupBox2.DrawSeperator = false;
            this.nsGroupBox2.ForeColor = System.Drawing.Color.White;
            this.nsGroupBox2.Location = new System.Drawing.Point(246, 410);
            this.nsGroupBox2.Name = "nsGroupBox2";
            this.nsGroupBox2.Padding = new System.Windows.Forms.Padding(10, 40, 10, 10);
            this.nsGroupBox2.Size = new System.Drawing.Size(421, 21);
            this.nsGroupBox2.SubTitle = "------";
            this.nsGroupBox2.TabIndex = 3;
            this.nsGroupBox2.Text = "nsGroupBox2";
            this.nsGroupBox2.Title = "";
            // 
            // nsGroupBox1
            // 
            this.nsGroupBox1.Controls.Add(this.txtLog);
            this.nsGroupBox1.Controls.Add(this.nsLabel2);
            this.nsGroupBox1.Controls.Add(this.nsSeperator1);
            this.nsGroupBox1.Controls.Add(this.nsTabControl1);
            this.nsGroupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.nsGroupBox1.DrawSeperator = false;
            this.nsGroupBox1.ForeColor = System.Drawing.Color.White;
            this.nsGroupBox1.Location = new System.Drawing.Point(10, 40);
            this.nsGroupBox1.Name = "nsGroupBox1";
            this.nsGroupBox1.Padding = new System.Windows.Forms.Padding(10, 40, 10, 10);
            this.nsGroupBox1.Size = new System.Drawing.Size(236, 391);
            this.nsGroupBox1.SubTitle = "Details";
            this.nsGroupBox1.TabIndex = 2;
            this.nsGroupBox1.Text = "nsGroupBox1";
            this.nsGroupBox1.Title = "Menu";
            // 
            // txtLog
            // 
            this.txtLog.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLog.Location = new System.Drawing.Point(10, 315);
            this.txtLog.MaxLength = 32767;
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = false;
            this.txtLog.Size = new System.Drawing.Size(216, 66);
            this.txtLog.TabIndex = 3;
            this.txtLog.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtLog.UseSystemPasswordChar = false;
            // 
            // nsLabel2
            // 
            this.nsLabel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.nsLabel2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel2.Location = new System.Drawing.Point(10, 302);
            this.nsLabel2.Name = "nsLabel2";
            this.nsLabel2.Size = new System.Drawing.Size(216, 13);
            this.nsLabel2.TabIndex = 2;
            this.nsLabel2.Text = "nsLabel1";
            this.nsLabel2.Value1 = "Log Proses";
            this.nsLabel2.Value2 = "";
            // 
            // nsSeperator1
            // 
            this.nsSeperator1.Dock = System.Windows.Forms.DockStyle.Top;
            this.nsSeperator1.Location = new System.Drawing.Point(10, 288);
            this.nsSeperator1.Name = "nsSeperator1";
            this.nsSeperator1.Size = new System.Drawing.Size(216, 14);
            this.nsSeperator1.TabIndex = 1;
            this.nsSeperator1.Text = "nsSeperator1";
            // 
            // nsTabControl1
            // 
            this.nsTabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.nsTabControl1.Controls.Add(this.tabPage1);
            this.nsTabControl1.Controls.Add(this.tabPage2);
            this.nsTabControl1.Controls.Add(this.tabPage3);
            this.nsTabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.nsTabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.nsTabControl1.ItemSize = new System.Drawing.Size(28, 115);
            this.nsTabControl1.Location = new System.Drawing.Point(10, 40);
            this.nsTabControl1.Multiline = true;
            this.nsTabControl1.Name = "nsTabControl1";
            this.nsTabControl1.SelectedIndex = 0;
            this.nsTabControl1.Size = new System.Drawing.Size(216, 248);
            this.nsTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.nsTabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.tabPage1.Controls.Add(this.btnSetupMySQL);
            this.tabPage1.Controls.Add(this.btnSetup);
            this.tabPage1.Location = new System.Drawing.Point(119, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(93, 240);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Setup";
            // 
            // btnSetupMySQL
            // 
            this.btnSetupMySQL.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSetupMySQL.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSetupMySQL.Location = new System.Drawing.Point(3, 58);
            this.btnSetupMySQL.Name = "btnSetupMySQL";
            this.btnSetupMySQL.Size = new System.Drawing.Size(87, 55);
            this.btnSetupMySQL.TabIndex = 1;
            this.btnSetupMySQL.Text = "Setup MySQL";
            this.btnSetupMySQL.Click += new System.EventHandler(this.btnSetupMySQL_Click);
            // 
            // btnSetup
            // 
            this.btnSetup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSetup.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSetup.Location = new System.Drawing.Point(3, 3);
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.Size = new System.Drawing.Size(87, 55);
            this.btnSetup.TabIndex = 0;
            this.btnSetup.Text = "Setup";
            this.btnSetup.Click += new System.EventHandler(this.btnSetup_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.tabPage2.Controls.Add(this.btnProsesAll);
            this.tabPage2.Location = new System.Drawing.Point(119, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(93, 240);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Transaksi";
            // 
            // btnProsesAll
            // 
            this.btnProsesAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProsesAll.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProsesAll.Location = new System.Drawing.Point(3, 3);
            this.btnProsesAll.Name = "btnProsesAll";
            this.btnProsesAll.Size = new System.Drawing.Size(87, 55);
            this.btnProsesAll.TabIndex = 2;
            this.btnProsesAll.Text = "Proses All Transaksi";
            this.btnProsesAll.Click += new System.EventHandler(this.btnProsesAll_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.tabPage3.Controls.Add(this.btnBRD);
            this.tabPage3.Controls.Add(this.btnImportTkx);
            this.tabPage3.Location = new System.Drawing.Point(119, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(93, 240);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Utility";
            // 
            // btnBRD
            // 
            this.btnBRD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBRD.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBRD.Location = new System.Drawing.Point(3, 58);
            this.btnBRD.Name = "btnBRD";
            this.btnBRD.Size = new System.Drawing.Size(87, 55);
            this.btnBRD.TabIndex = 3;
            this.btnBRD.Text = "Proses BRD";
            this.btnBRD.Click += new System.EventHandler(this.btnBRD_Click);
            // 
            // btnImportTkx
            // 
            this.btnImportTkx.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnImportTkx.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnImportTkx.Location = new System.Drawing.Point(3, 3);
            this.btnImportTkx.Name = "btnImportTkx";
            this.btnImportTkx.Size = new System.Drawing.Size(87, 55);
            this.btnImportTkx.TabIndex = 2;
            this.btnImportTkx.Text = "Import TKX CSV";
            this.btnImportTkx.Click += new System.EventHandler(this.btnImportTkx_Click);
            // 
            // nsControlButton1
            // 
            this.nsControlButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nsControlButton1.ControlButton = NSControlButton.Button.Close;
            this.nsControlButton1.Location = new System.Drawing.Point(653, 5);
            this.nsControlButton1.Margin = new System.Windows.Forms.Padding(0);
            this.nsControlButton1.MaximumSize = new System.Drawing.Size(18, 20);
            this.nsControlButton1.MinimumSize = new System.Drawing.Size(18, 20);
            this.nsControlButton1.Name = "nsControlButton1";
            this.nsControlButton1.Size = new System.Drawing.Size(18, 20);
            this.nsControlButton1.TabIndex = 1;
            this.nsControlButton1.Text = "nsControlButton1";
            this.nsControlButton1.Click += new System.EventHandler(this.nsControlButton1_Click);
            // 
            // bgDownHarian
            // 
            this.bgDownHarian.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgDownHarian_DoWork);
            this.bgDownHarian.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgDownHarian_RunWorkerCompleted);
            // 
            // timer1
            // 
            this.timer1.Interval = 3600000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 441);
            this.Controls.Add(this.nsTheme1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMain";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.nsTheme1.ResumeLayout(false);
            this.nsGroupBox3.ResumeLayout(false);
            this.nsGroupBox1.ResumeLayout(false);
            this.nsTabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private NSTheme nsTheme1;
        private NSTabControl nsTabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private NSButton btnSetup;
        private NSButton btnSetupMySQL;
        private System.Windows.Forms.TabPage tabPage3;
        private NSButton btnProsesAll;
        private NSControlButton nsControlButton1;
        private NSGroupBox nsGroupBox1;
        private NSSeperator nsSeperator1;
        private NSLabel nsLabel2;
        private NSTextBox txtLog;
        private NSGroupBox nsGroupBox2;
        private NSGroupBox nsGroupBox3;
        private System.Windows.Forms.Panel panel1;
        private System.ComponentModel.BackgroundWorker bgDownHarian;
        private System.Windows.Forms.Timer timer1;
        private NSButton btnImportTkx;
        private NSButton btnBRD;
    }
}