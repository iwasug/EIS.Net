﻿using EIS.Net.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EIS.Net
{
    public partial class frmServer : Form
    {
        EISRepository Fungsi = new EISRepository();
        public frmServer()
        {
            InitializeComponent();
            txtServer.Text = Fungsi.BacaReg("Server");
            txtPort.Text = Fungsi.BacaReg("Port");
            txtUser.Text = Fungsi.BacaReg("User");
            txtPassword.Text = Fungsi.Decrypt(Fungsi.BacaReg("Password"), "!WaSu612IwA");
            txtDatabase.Text = Fungsi.BacaReg("Database");
            Fungsi.FolderHarian = Fungsi.BacaReg("FolderHarian");
            Fungsi.FolderTemporary = Fungsi.BacaReg("FolderTemporary");
            if (Fungsi.BacaReg("Interval") == null)
            {
                Fungsi.TulisReg("Interval", "10");
            }
            else
            {
                Fungsi.Interval = int.Parse(Fungsi.BacaReg("Interval"));
            }
            if (Fungsi.BacaReg("Periode") == null)
            {
                Fungsi.TulisReg("Periode", DateTime.Now.ToString("MM-yyyy"));
            }
            else
            {
                Fungsi.Periode = Fungsi.BacaReg("Periode").Replace("-", "");
            }
        }

        private void btnCekKoneksi_Click(object sender, EventArgs e)
        {
            if (Fungsi.CheckConnection(txtServer.Text, txtPort.Text, txtUser.Text, txtPassword.Text, txtDatabase.Text))
            {
                Fungsi.Server = txtServer.Text;
                Fungsi.Port = txtPort.Text;
                Fungsi.Password = txtPassword.Text;
                Fungsi.User = txtUser.Text;
                Fungsi.Database = txtDatabase.Text;
                Fungsi.CreateStruktur(Fungsi.Periode);
                if (!Fungsi.isMdi)
                {
                    frmProsesAll Utama = new frmProsesAll();
                    Utama.Show();
                }
                else
                {
                    //MetroMessageBox.Show(this, "Cek Server Berhasil");
                    Fungsi.Pesan(this, "CEK SERVER BERHASIL", "INF");
                }
            }
            else
            {
                Fungsi.Pesan(this, "CEK SERVER GAGAL", "ERR");
            }
        }
    }
}
