﻿using EIS.Net.Repository;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EIS.Net
{
    public partial class frmProsesAll : Form
    {
        EISRepository Fungsi = new EISRepository();
        private DataTable ListProses;
        public frmProsesAll()
        {
            InitializeComponent();
            DateTime Dt_ = DateTime.Parse(Fungsi.BacaReg("Periode"));
            var firstDayOfMonth = new DateTime(Dt_.Year, Dt_.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            prdAwal.MinDate = firstDayOfMonth;
            prdAwal.MaxDate = lastDayOfMonth;
            prdAkhir.MinDate = firstDayOfMonth;
            prdAkhir.MaxDate = lastDayOfMonth;
            prdAwal.Value = firstDayOfMonth;
            ListProses = new DataTable();
            ListProses.Columns.Add("FILE", typeof(string));
            ListProses.Columns.Add("KOMA", typeof(string));
            Otomatis.Checked = true;
        }

        private void frmProsesAll_Load(object sender, EventArgs e)
        {
            foreach (DataRow df in Fungsi.ListKonversi.Rows)
            {
                ListViewItem lvi = new ListViewItem(new string[] { df[0].ToString() });
                lvi.Checked = true;
                listFile.Items.Add(lvi);
            }
            /*
            foreach (ListViewItem item in listFile.Items)
            {
                item.Selected = true;
            }
            */
            checklistFile.Checked = true;
            timer1.Start();
        }

        private void tmrProses_Tick(object sender, EventArgs e)
        {
            curent = curent - 1;
            txtInterval.Text = curent.ToString();
            if (curent == 0)
            {
                //Proses();
                //prosesAsync();
                Proses();
            }
        }

        int curent;
        private void mulai()
        {
            curent = 60 * Fungsi.Interval;
            tmrProses.Interval = 1000;
            tmrProses.Start();
        }

        private string _lbStatusToko;
        private string lbStatusToko
        {
            get
            {
                return this._lbStatusToko;
            }
            set
            {
                this._lbStatusToko = value;
                this.UpdatelbToko();
            }
        }
        private void UpdatelbToko()
        {
            bool invokeRequired = this.InvokeRequired;
            if (invokeRequired)
            {
                this.Invoke(new MethodInvoker(this.UpdatelbToko));
            }
            else
            {
                gProgress.Title = this.lbStatusToko;
            }
        }

        private string _lbStatus;
        private string lbStatus
        {
            get
            {
                return this._lbStatus;
            }
            set
            {
                this._lbStatus = value;
                this.UpdatelbStatus();
            }
        }
        private void UpdatelbStatus()
        {
            bool invokeRequired = this.InvokeRequired;
            if (invokeRequired)
            {
                this.Invoke(new MethodInvoker(this.UpdatelbStatus));
            }
            else
            {
                lblStatus.Text = this.lbStatus;
                lblStatus.Refresh();
            }
        }

        private string _lbStatusFile;
        private string lbStatusFile
        {
            get
            {
                return this._lbStatusFile;
            }
            set
            {
                this._lbStatusFile = value;
                this.UpdatelbFile();
            }
        }
        private void UpdatelbFile()
        {
            bool invokeRequired = this.InvokeRequired;
            if (invokeRequired)
            {
                this.Invoke(new MethodInvoker(this.UpdatelbFile));
            }
            else
            {
                gProgress.SubTitle = this.lbStatusFile;
            }
        }

        private string _Log;
        private string Log
        {
            set
            {
                _Log = value;
                this.UpdLog();
            }
            get { return _Log; }
        }

        private void UpdLog()
        {
            bool invokeRequired = this.InvokeRequired;
            if (invokeRequired)
            {
                this.Invoke(new MethodInvoker(this.UpdLog));
            }
            else
            {
                string[] itm = Log.Split('|');
                var item = new ListViewItem(new[] { itm[0], itm[1], itm[2] });
                listLog.Items.Add(item);
                listLog.TopItem = listLog.Items.Cast<ListViewItem>().LastOrDefault();
                /*
                TreeNode treeNode = new TreeNode(UpdLog);
                treeView1.Nodes.Add(treeNode);
                treeNode.EnsureVisible();
                */

            }
        }

        private void BuatAbsen(string Periode)
        {
            lbStatus = "Buat absen File......";
            MySqlConnection mCon = new MySqlConnection(Fungsi.ServerMySQL);
            //DataTable dt = Fungsi.GetDataTable("SELECT * FROM MTOKO" + Periode + " ORDER BY TOKO_CODE;");
            DataTable dt = new DataTable();
            var SQL = "";
            try
            {
                mCon.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM MTOKO" + Periode + " ORDER BY TOKO_CODE;", mCon);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(dt);
                Fungsi.ListToko = dt;
                pgBar1.Value = 0;
                pgBar1.Maximum = dt.Rows.Count;
                foreach (DataRow dr in dt.Rows)
                {
                    pgBar1.Value += 1;
                    lbStatusToko = dr["NAMA_TOKO"].ToString();
                    //UpdTree = dr["NAMA_TOKO"].ToString() + " --> OK";
                    //Log = dr["TOKO_CODE"].ToString() + "|BUAT ABSEN|OK";
                    //Fungsi.Pesan(Fungsi.ListKonversi.Rows.Count.ToString(),"INF");
                    foreach (DataRow fl in Fungsi.ListKonversi.Rows)
                    {
                        //UpdTree = dr["NAMA_TOKO"].ToString() + " --> " + fl["FILE"].ToString();
                        //Log = dr["TOKO_CODE"].ToString() + "|" + fl["FILE"].ToString() + "| OK";
                        lbStatusFile = "Proses File : " + fl["FILE"].ToString();
                        Application.DoEvents();
                        SQL = "INSERT IGNORE ABSENFILE" + Periode + "(SHOP,NAMATK,FILE) ";
                        SQL += "VALUES(@SHOP,@NAMATK,@FILE) ON DUPLICATE KEY UPDATE NAMATK=@NAMATK;";
                        cmd = new MySqlCommand(SQL, mCon);
                        cmd.Parameters.AddWithValue("@SHOP", dr["TOKO_CODE"].ToString());
                        cmd.Parameters.AddWithValue("@NAMATK", dr["NAMA_TOKO"].ToString());
                        cmd.Parameters.AddWithValue("@FILE", fl["FILE"].ToString());
                        cmd.ExecuteNonQuery();
                    }
                }
                lbStatus = "Buat absen File beres";
            }
            catch (Exception err)
            {
                //Fungsi.Log(err.ToString());
                Fungsi.Log("BUAT ABSEN", "", err.Message);
            }

        }

        private void BuatAbsenHr(string SHOP, string NAMA, string BUKA, string TANGGAL, string FILEHR, string KOTA)
        {
            var SQL = "";
            MySqlConnection mCon = new MySqlConnection(Fungsi.ServerMySQL);
            try
            {
                if (mCon.State != ConnectionState.Open)
                {
                    mCon.Open();
                }
                SQL = "INSERT INTO ABSENHR" + Fungsi.Periode + "(SHOP, NAMA, BUKA, TANGGAL, FILEHR, KOTA) ";
                SQL += "VALUES('" + SHOP + "', '" + NAMA + "', '" + BUKA + "', '" + TANGGAL + "', '" + FILEHR + "', '" + KOTA + "') ON DUPLICATE KEY UPDATE NAMA='" + NAMA + "',KOTA='" + KOTA + "';";
                //Fungsi.SQL += "VALUES(@SHOP, @NAMA, @BUKA, @TANGGAL, @FILEHR, @KOTA) ON DUPLICATE KEY UPDATE NAMA=@NAMA;";
                MySqlCommand cmd = new MySqlCommand(SQL, mCon);
                /*
                Fungsi.cmd.Parameters.AddWithValue("@SHOP", SHOP);
                Fungsi.cmd.Parameters.AddWithValue("@NAMA", NAMA);
                Fungsi.cmd.Parameters.AddWithValue("@BUKA", BUKA);
                Fungsi.cmd.Parameters.AddWithValue("@KOTA", TANGGAL);
                Fungsi.cmd.Parameters.AddWithValue("@TANGGAL", TANGGAL);
                */
                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                Fungsi.Log("BUAT ABSEN HARIAN", "", err.Message + Fungsi.SQL);
            }
            finally
            {
                mCon.Close();
            }
        }

        private void UpdateAbsen(string SHOP, string TANGGAL)
        {
            var SQL = "";
            MySqlConnection mCon = new MySqlConnection(Fungsi.ServerMySQL);
            try
            {
                if (mCon.State != ConnectionState.Open)
                {
                    mCon.Open();
                }

                SQL = "UPDATE ABSENHR" + Fungsi.Periode + " SET STATUS ='1' WHERE SHOP='" + SHOP + "' AND TANGGAL='" + TANGGAL + "';";
                MySqlCommand cmd = new MySqlCommand(SQL, mCon);
                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                Fungsi.Log("UPDATE ABSEN HARIAN", "", err.Message);
            }
            finally
            {
                mCon.Close();
            }
        }

        private void Proses()
        {
            //treeView1.Nodes.Clear();
            //treeView2.Nodes.Clear();
            listFile.Enabled = false;
            prdAwal.Enabled = false;
            prdAkhir.Enabled = false;
            listLog.Items.Clear();
            tmrProses.Stop();
            Fungsi.Stopwatch = DateTime.Now - DateTime.Now;
            btnProses.Enabled = false;
            btnStop.Enabled = true;
            BuatAbsen(Fungsi.Periode);
            //treeView1.Nodes.Clear();
            pgBar1.Maximum = 100;
            Fungsi.CreateStruktur(Fungsi.Periode);
            if (Otomatis.Checked == true)
            {
                var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                if (DateTime.Now.Day != 1)
                {
                    prdAkhir.Value = DateTime.Now.AddDays(-1);
                    prdAwal.Value = firstDayOfMonth;
                }
            }
            ListProses.Rows.Clear();
            foreach (ListViewItem itm in listFile.CheckedItems)
            {
                ListProses.Rows.Add(itm.SubItems[0].Text, Fungsi.GetKoma(itm.SubItems[0].Text));
            }
            bgWork1.RunWorkerAsync();
        }

        private void Stop()
        {
            DialogResult result = MessageBox.Show(this, "Apa Proses Mau di Stop", Fungsi.Versi, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                if (bgWork1.IsBusy)
                {
                    bgWork1.CancelAsync();
                }
            }
        }

        private void ClearFolder(string FolderName)
        {
            DirectoryInfo dir = new DirectoryInfo(FolderName);
            try
            {
                foreach (FileInfo fi in dir.GetFiles())
                {
                    fi.IsReadOnly = false;
                    fi.Delete();
                }

                foreach (DirectoryInfo di in dir.GetDirectories())
                {
                    ClearFolder(di.FullName);
                    di.Delete();
                }
            }
            catch (Exception)
            {

            }
        }

        private void checklistFile_CheckedChanged(object sender)
        {
            if (checklistFile.Checked)
            {
                foreach (ListViewItem itm in listFile.Items)
                {
                    //ListProses.Rows.Add(itm.SubItems[0].Text, Fungsi.GetKoma(itm.SubItems[0].Text));
                    itm.Checked = true;
                }
                checklistFile.Text = "UnCheck All List File";
            }
            else
            {
                foreach (ListViewItem itm in listFile.Items)
                {
                    //ListProses.Rows.Add(itm.SubItems[0].Text, Fungsi.GetKoma(itm.SubItems[0].Text));
                    itm.Checked = false;
                }
                checklistFile.Text = "Check All List File";
            }
        }

        private void Otomatis_CheckedChanged(object sender)
        {
            if (Otomatis.Checked == true)
            {
                mulai();
            }
            else
            {
                tmrProses.Stop();
                txtInterval.Text = "0";
            }
        }

        public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = thru.Date; day.Date >= from.Date; day = day.AddDays(-1))
                yield return day;
        }

        private void bgWork1_DoWork(object sender, DoWorkEventArgs e)
        {
            DateTime d1 = prdAwal.Value;
            DateTime d2 = prdAkhir.Value;
            Fungsi.SQL = "SELECT * FROM ABSENFILE" + Fungsi.Periode + ";";
            using (DataTable dt = Fungsi.GetDataTable(Fungsi.SQL))
            {
                Fungsi.ListCekAbsen = dt;
                //Fungsi.Pesan(dt.Rows.Count.ToString(), "ERR");
            }
            foreach (DateTime Tanggal in EachDay(d1, d2))
            {
                lbStatusFile = "UPDATE BRD & STDC " + Tanggal.ToString("yyyy-MM-dd");
                Fungsi.ProsesBRD(Tanggal);
            }
            DataTable PRS = Fungsi.ListKonversi;
            foreach (DataRow FL in ListProses.Rows)
            {
                string FILE1 = FL[0].ToString();
                string KOMA = FL[1].ToString();
                foreach (DateTime Tanggal in EachDay(d1, d2))
                {
                    using (DataTable dt = Fungsi.ListToko)
                    {
                        double x = dt.Rows.Count;
                        int a;
                        double c;
                        double b = 0;
                        foreach (DataRow dr in dt.Rows)
                        {
                            b += 1;
                            c = (b / x);
                            c = c * 100;
                            a = Convert.ToInt32(c);
                            bgWork1.ReportProgress(a);
                            lbStatusToko = dr["NAMA_TOKO"].ToString() + " - " + Tanggal.ToString("yyyy - MM - dd");
                            string KDTK = dr["TOKO_CODE"].ToString();
                            string AWAL = "";
                            if (KDTK.Substring(0, 1) == "T")
                            {
                                AWAL = "HR";
                            }
                            else if (KDTK.Substring(0, 1) == "F")
                            {
                                AWAL = "FR";
                            }
                            else if (KDTK.Substring(0, 1) == "R")
                            {
                                AWAL = "CR";
                            }
                            string FILEHRN = AWAL + Tanggal.ToString("yyMMdd") + "." + KDTK.Substring(1, 3);
                            string FILE2 = FILE1 + Tanggal.ToString("MMdd") + KDTK.Substring(0, 1) + "." + KDTK.Substring(1, 3);
                            BuatAbsenHr(dr["TOKO_CODE"].ToString(), dr["NAMA_TOKO"].ToString(), DateTime.Parse(dr["TGL_BUKA"].ToString()).ToString("yyyy-MM-dd"), Tanggal.ToString("yyyyMMdd"), FILEHRN, dr["KOTA"].ToString());
                            if (Fungsi.CekAbsenV2(dr["TOKO_CODE"].ToString(), FILE1, Tanggal))
                            {
                                if (File.Exists(Fungsi.FolderHarian + "\\" + FILEHRN))
                                {
                                    /* PROSES FILE LQS PROSES QUERY SALES 2016-03-01 */
                                    if (FILE1 == "LQS")
                                    {
                                        FILE2 = FILE1 + dr["TOKO_CODE"].ToString() + Tanggal.ToString("yyMMdd") + ".CSV";
                                    }
                                    if (FILE1 == "CL" || FILE1 == "CM" || FILE1 == "TL")
                                    {
                                        FILE2 = FILE1 + Tanggal.ToString("yyyy").Substring(3, 1) + Tanggal.ToString("MMdd") + KDTK.Substring(0, 1) + "." + KDTK.Substring(1, 3);
                                    }

                                    if (FILE1 == "TTD")
                                    {
                                        FILE2 = FILE1 + Tanggal.ToString("ddMMyy") + KDTK.Substring(0, 1) + "." + KDTK.Substring(1, 3);
                                        //MessageBox.Show(FILE2);
                                    }
                                    if (FILE1 == "PBSL")
                                    {
                                        //PBSLG027T9B0170204
                                        FILE2 = FILE1 + Fungsi.Cabang + KDTK + Tanggal.ToString("yyyy").Substring(2, 2) + Tanggal.ToString("MMdd") + ".CSV";
                                    }
                                    //MessageBox.Show(FILE2);
                                    if (Fungsi.UnzipFile(Fungsi.FolderHarian + "\\" + FILEHRN, FILE2, Fungsi.FolderTemporary))
                                    {
                                        //MessageBox.Show(FILE2);

                                        //lbStatusFile = "Proses Tanggal : " + Tanggal;
                                        //lbStatus = "Proses --> " + Tanggal.ToShortDateString();
                                        if (File.Exists((Fungsi.FolderTemporary + "\\" + FILE2)))
                                        {
                                            if (Fungsi.Konversi_CSV(FILE1, Fungsi.FolderTemporary + "\\" + FILE2, KOMA,Tanggal.ToString("MMyyyy")))
                                            {
                                                //Fungsi.Pesan("KON OK","INF");
                                                lbStatusFile = "Proses File : " + FILEHRN + " ---> " + FILE2;
                                                if (Fungsi.ProsesFile(FILE1, KDTK, Tanggal))
                                                {
                                                    //UpdTree = FILE2 + " --> Sukses di Proses";
                                                    Log = KDTK + "|" + FILE2 + "|SUKSES DI PROSES";
                                                    try
                                                    {
                                                        File.Delete(Fungsi.FolderTemporary + "\\" + FILE2);
                                                    }
                                                    catch (Exception)
                                                    {

                                                    }
                                                    if (FILE1 == "DT")
                                                    {
                                                        UpdateAbsen(dr["TOKO_CODE"].ToString(), Tanggal.ToString("yyyyMMdd"));
                                                    }
                                                }
                                                else
                                                {
                                                    //UpdTree2 = FILE2 + " --> Gagal Proses File";
                                                    Log = KDTK + "|" + FILE2 + "|GAGAL PROSES";
                                                }
                                            }
                                            else
                                            {
                                                //UpdTree2 = FILE2 + " --> Gagal Konversi ke MySQL";
                                                Log = KDTK + "|" + FILE2 + "|GAGAL KONVERSI KE MYSQL";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //MessageBox.Show(FILE2);
                                        if (FILE1 == "DT")
                                        {
                                            Log = KDTK + "|" + FILE2 + "|GAGAL UNZIPL";
                                        }

                                    }
                                }
                            }
                            else
                            {
                                lbStatusFile = "Proses File : " + FILE2 + " ---> Sudah di Proses";
                            }
                            if (bgWork1.CancellationPending)
                            {
                                e.Cancel = true;
                                bgWork1.ReportProgress(0);
                                return;
                            }
                        }
                    }
                }
                bgWork1.ReportProgress(100);
            }
        }

        private void bgWork1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pgBar1.Value = e.ProgressPercentage;
            lblStatus.Text = "Processing......" + pgBar1.Value.ToString() + "%";
            lblStatus.Refresh();
        }

        private void bgWork1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                lblStatus.Text = "Proses di Stop.....";
                lbStatusFile = String.Format("{0} days, {1} hours, {2} minutes, {3} seconds", Fungsi.Stopwatch.Days, Fungsi.Stopwatch.Hours, Fungsi.Stopwatch.Minutes, Fungsi.Stopwatch.Seconds);
            }
            else if (e.Error != null)
            {
                lblStatus.Text = "Error....";
                lbStatusFile = String.Format("{0} days, {1} hours, {2} minutes, {3} seconds", Fungsi.Stopwatch.Days, Fungsi.Stopwatch.Hours, Fungsi.Stopwatch.Minutes, Fungsi.Stopwatch.Seconds);
            }
            else
            {
                lblStatus.Text = "Proses Selesai...";
                lbStatusFile = String.Format("{0} days, {1} hours, {2} minutes, {3} seconds", Fungsi.Stopwatch.Days, Fungsi.Stopwatch.Hours, Fungsi.Stopwatch.Minutes, Fungsi.Stopwatch.Seconds);
            }
            ClearFolder(Fungsi.FolderTemporary);
            btnProses.Enabled = true;
            btnStop.Enabled = false;
            listFile.Enabled = true;
            prdAwal.Enabled = true;
            prdAkhir.Enabled = true;
            if (Otomatis.Checked == true)
            {
                mulai();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            gSetting.SubTitle = DateTime.Now.ToString("HH:mm:ss");
            if(DateTime.Now.ToString("HH:mm:ss") == "01:00:00" || DateTime.Now.ToString("HH:mm:ss") == "02:00:00" || DateTime.Now.ToString("HH:mm:ss") == "12:00:00" || DateTime.Now.ToString("HH:mm:ss") == "05:00:00" || DateTime.Now.ToString("HH:mm:ss") == "22:00:00")
            {
                if(!Otomatis.Checked)
                {
                    if(!bgWork1.IsBusy)
                    {
                        Otomatis.Checked = true;
                    }
                }
            }
        }

        private void btnProses_Click(object sender, EventArgs e)
        {
            Proses();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            Stop();
        }

        private void setupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSetup f = new frmSetup();
            f.ShowDialog();
        }

        private void btnSetup_Click(object sender, EventArgs e)
        {
            MenuSetup.Show(Cursor.Position);
        }

        private void SetupMySQL_Click(object sender, EventArgs e)
        {
            frmServer f = new frmServer();
            f.ShowDialog();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {

        }

        private void nsControlButton1_Click(object sender, EventArgs e)
        {
        }

        private void frmProsesAll_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show(this, "APAKAH PROGRAM MAU DI TUTUP", Fungsi.Versi, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}
