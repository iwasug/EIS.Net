﻿namespace EIS.Net
{
    partial class frmProsesAll
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tmrProses = new System.Windows.Forms.Timer(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.bgWork1 = new System.ComponentModel.BackgroundWorker();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.nsTheme1 = new NSTheme();
            this.nsControlButton1 = new NSControlButton();
            this.gSetting = new NSGroupBox();
            this.btnSetup = new NSButton();
            this.prdAkhir = new System.Windows.Forms.DateTimePicker();
            this.prdAwal = new System.Windows.Forms.DateTimePicker();
            this.listLog = new System.Windows.Forms.ListView();
            this.TOKO = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.FILE = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.STATUS = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listFile = new System.Windows.Forms.ListView();
            this.checklistFile = new NSCheckBox();
            this.Otomatis = new NSCheckBox();
            this.txtInterval = new NSTextBox();
            this.btnStop = new NSButton();
            this.btnProses = new NSButton();
            this.nsLabel3 = new NSLabel();
            this.nsLabel1 = new NSLabel();
            this.gProgress = new NSGroupBox();
            this.pgBar1 = new NSProgressBar();
            this.lblStatus = new System.Windows.Forms.Label();
            this.MenuSetup = new NSContextMenu();
            this.SetupMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.SetupMySQL = new System.Windows.Forms.ToolStripMenuItem();
            this.importTKXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nsTheme1.SuspendLayout();
            this.gSetting.SuspendLayout();
            this.gProgress.SuspendLayout();
            this.MenuSetup.SuspendLayout();
            this.SuspendLayout();
            // 
            // tmrProses
            // 
            this.tmrProses.Tick += new System.EventHandler(this.tmrProses_Tick);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // bgWork1
            // 
            this.bgWork1.WorkerReportsProgress = true;
            this.bgWork1.WorkerSupportsCancellation = true;
            this.bgWork1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWork1_DoWork);
            this.bgWork1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWork1_ProgressChanged);
            this.bgWork1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWork1_RunWorkerCompleted);
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // nsTheme1
            // 
            this.nsTheme1.AccentOffset = 0;
            this.nsTheme1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.nsTheme1.BorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.nsTheme1.Colors = new Bloom[0];
            this.nsTheme1.Controls.Add(this.nsControlButton1);
            this.nsTheme1.Controls.Add(this.gSetting);
            this.nsTheme1.Controls.Add(this.gProgress);
            this.nsTheme1.Customization = "";
            this.nsTheme1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nsTheme1.Font = new System.Drawing.Font("Verdana", 8F);
            this.nsTheme1.Image = null;
            this.nsTheme1.Location = new System.Drawing.Point(0, 0);
            this.nsTheme1.Movable = true;
            this.nsTheme1.Name = "nsTheme1";
            this.nsTheme1.NoRounding = false;
            this.nsTheme1.Padding = new System.Windows.Forms.Padding(10, 40, 10, 10);
            this.nsTheme1.Sizable = true;
            this.nsTheme1.Size = new System.Drawing.Size(652, 378);
            this.nsTheme1.SmartBounds = true;
            this.nsTheme1.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.nsTheme1.TabIndex = 0;
            this.nsTheme1.Text = "Pooling Data Harian Toko";
            this.nsTheme1.TransparencyKey = System.Drawing.Color.Empty;
            this.nsTheme1.Transparent = false;
            // 
            // nsControlButton1
            // 
            this.nsControlButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nsControlButton1.ControlButton = NSControlButton.Button.Close;
            this.nsControlButton1.Location = new System.Drawing.Point(627, 4);
            this.nsControlButton1.Margin = new System.Windows.Forms.Padding(0);
            this.nsControlButton1.MaximumSize = new System.Drawing.Size(18, 20);
            this.nsControlButton1.MinimumSize = new System.Drawing.Size(18, 20);
            this.nsControlButton1.Name = "nsControlButton1";
            this.nsControlButton1.Size = new System.Drawing.Size(18, 20);
            this.nsControlButton1.TabIndex = 13;
            this.nsControlButton1.Text = "nsControlButton1";
            this.nsControlButton1.Click += new System.EventHandler(this.nsControlButton1_Click);
            // 
            // gSetting
            // 
            this.gSetting.Controls.Add(this.btnSetup);
            this.gSetting.Controls.Add(this.prdAkhir);
            this.gSetting.Controls.Add(this.prdAwal);
            this.gSetting.Controls.Add(this.listLog);
            this.gSetting.Controls.Add(this.listFile);
            this.gSetting.Controls.Add(this.checklistFile);
            this.gSetting.Controls.Add(this.Otomatis);
            this.gSetting.Controls.Add(this.txtInterval);
            this.gSetting.Controls.Add(this.btnStop);
            this.gSetting.Controls.Add(this.btnProses);
            this.gSetting.Controls.Add(this.nsLabel3);
            this.gSetting.Controls.Add(this.nsLabel1);
            this.gSetting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gSetting.DrawSeperator = false;
            this.gSetting.ForeColor = System.Drawing.Color.White;
            this.gSetting.Location = new System.Drawing.Point(10, 40);
            this.gSetting.Name = "gSetting";
            this.gSetting.Padding = new System.Windows.Forms.Padding(10, 40, 10, 10);
            this.gSetting.Size = new System.Drawing.Size(632, 241);
            this.gSetting.SubTitle = "Details";
            this.gSetting.TabIndex = 1;
            this.gSetting.Text = "nsGroupBox1";
            this.gSetting.Title = "Setting";
            // 
            // btnSetup
            // 
            this.btnSetup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSetup.Location = new System.Drawing.Point(528, 148);
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.Size = new System.Drawing.Size(94, 26);
            this.btnSetup.TabIndex = 14;
            this.btnSetup.Text = "Setup";
            this.btnSetup.Click += new System.EventHandler(this.btnSetup_Click);
            // 
            // prdAkhir
            // 
            this.prdAkhir.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.prdAkhir.Location = new System.Drawing.Point(130, 63);
            this.prdAkhir.Name = "prdAkhir";
            this.prdAkhir.Size = new System.Drawing.Size(95, 20);
            this.prdAkhir.TabIndex = 13;
            // 
            // prdAwal
            // 
            this.prdAwal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.prdAwal.Location = new System.Drawing.Point(14, 63);
            this.prdAwal.Name = "prdAwal";
            this.prdAwal.Size = new System.Drawing.Size(95, 20);
            this.prdAwal.TabIndex = 13;
            // 
            // listLog
            // 
            this.listLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.listLog.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.TOKO,
            this.FILE,
            this.STATUS});
            this.listLog.ForeColor = System.Drawing.Color.White;
            this.listLog.Location = new System.Drawing.Point(13, 90);
            this.listLog.Name = "listLog";
            this.listLog.Size = new System.Drawing.Size(359, 135);
            this.listLog.TabIndex = 12;
            this.listLog.UseCompatibleStateImageBehavior = false;
            this.listLog.View = System.Windows.Forms.View.Details;
            // 
            // TOKO
            // 
            this.TOKO.Text = "TOKO";
            // 
            // FILE
            // 
            this.FILE.Text = "FILE";
            this.FILE.Width = 100;
            // 
            // STATUS
            // 
            this.STATUS.Text = "STATUS";
            this.STATUS.Width = 200;
            // 
            // listFile
            // 
            this.listFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.listFile.CheckBoxes = true;
            this.listFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listFile.ForeColor = System.Drawing.Color.White;
            this.listFile.Location = new System.Drawing.Point(378, 43);
            this.listFile.Name = "listFile";
            this.listFile.Size = new System.Drawing.Size(144, 182);
            this.listFile.TabIndex = 11;
            this.listFile.UseCompatibleStateImageBehavior = false;
            this.listFile.View = System.Windows.Forms.View.List;
            // 
            // checklistFile
            // 
            this.checklistFile.Checked = false;
            this.checklistFile.Location = new System.Drawing.Point(247, 64);
            this.checklistFile.Name = "checklistFile";
            this.checklistFile.Size = new System.Drawing.Size(136, 23);
            this.checklistFile.TabIndex = 9;
            this.checklistFile.Text = "Check All List File";
            this.checklistFile.CheckedChanged += new NSCheckBox.CheckedChangedEventHandler(this.checklistFile_CheckedChanged);
            // 
            // Otomatis
            // 
            this.Otomatis.Checked = false;
            this.Otomatis.Location = new System.Drawing.Point(247, 43);
            this.Otomatis.Name = "Otomatis";
            this.Otomatis.Size = new System.Drawing.Size(136, 23);
            this.Otomatis.TabIndex = 9;
            this.Otomatis.Text = "Otomatis";
            this.Otomatis.CheckedChanged += new NSCheckBox.CheckedChangedEventHandler(this.Otomatis_CheckedChanged);
            // 
            // txtInterval
            // 
            this.txtInterval.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtInterval.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInterval.Location = new System.Drawing.Point(528, 180);
            this.txtInterval.MaxLength = 32767;
            this.txtInterval.Multiline = true;
            this.txtInterval.Name = "txtInterval";
            this.txtInterval.ReadOnly = false;
            this.txtInterval.Size = new System.Drawing.Size(94, 45);
            this.txtInterval.TabIndex = 8;
            this.txtInterval.Text = "000";
            this.txtInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtInterval.UseSystemPasswordChar = false;
            // 
            // btnStop
            // 
            this.btnStop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStop.Location = new System.Drawing.Point(528, 103);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(94, 39);
            this.btnStop.TabIndex = 7;
            this.btnStop.Text = "Stop";
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnProses
            // 
            this.btnProses.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProses.Location = new System.Drawing.Point(528, 43);
            this.btnProses.Name = "btnProses";
            this.btnProses.Size = new System.Drawing.Size(94, 56);
            this.btnProses.TabIndex = 7;
            this.btnProses.Text = "Proses";
            this.btnProses.Click += new System.EventHandler(this.btnProses_Click);
            // 
            // nsLabel3
            // 
            this.nsLabel3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel3.Location = new System.Drawing.Point(130, 43);
            this.nsLabel3.Name = "nsLabel3";
            this.nsLabel3.Size = new System.Drawing.Size(96, 13);
            this.nsLabel3.TabIndex = 3;
            this.nsLabel3.Text = "nsLabel1";
            this.nsLabel3.Value1 = "Periode Akhir :";
            this.nsLabel3.Value2 = "";
            // 
            // nsLabel1
            // 
            this.nsLabel1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel1.Location = new System.Drawing.Point(13, 43);
            this.nsLabel1.Name = "nsLabel1";
            this.nsLabel1.Size = new System.Drawing.Size(96, 13);
            this.nsLabel1.TabIndex = 3;
            this.nsLabel1.Text = "nsLabel1";
            this.nsLabel1.Value1 = "Periode Awal :";
            this.nsLabel1.Value2 = "";
            // 
            // gProgress
            // 
            this.gProgress.Controls.Add(this.pgBar1);
            this.gProgress.Controls.Add(this.lblStatus);
            this.gProgress.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gProgress.DrawSeperator = false;
            this.gProgress.ForeColor = System.Drawing.Color.White;
            this.gProgress.Location = new System.Drawing.Point(10, 281);
            this.gProgress.Name = "gProgress";
            this.gProgress.Padding = new System.Windows.Forms.Padding(10, 40, 10, 10);
            this.gProgress.Size = new System.Drawing.Size(632, 87);
            this.gProgress.SubTitle = "Details";
            this.gProgress.TabIndex = 0;
            this.gProgress.Text = "nsGroupBox1";
            this.gProgress.Title = "Progress";
            // 
            // pgBar1
            // 
            this.pgBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgBar1.Location = new System.Drawing.Point(10, 40);
            this.pgBar1.Maximum = 100;
            this.pgBar1.Minimum = 0;
            this.pgBar1.Name = "pgBar1";
            this.pgBar1.Size = new System.Drawing.Size(612, 24);
            this.pgBar1.TabIndex = 11;
            this.pgBar1.Text = "nsProgressBar1";
            this.pgBar1.Value = 0;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblStatus.Location = new System.Drawing.Point(10, 64);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(56, 13);
            this.lblStatus.TabIndex = 10;
            this.lblStatus.Text = "lblStatus";
            // 
            // MenuSetup
            // 
            this.MenuSetup.ForeColor = System.Drawing.Color.White;
            this.MenuSetup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SetupMenu,
            this.SetupMySQL,
            this.importTKXToolStripMenuItem});
            this.MenuSetup.Name = "nsContextMenu1";
            this.MenuSetup.Size = new System.Drawing.Size(139, 70);
            // 
            // SetupMenu
            // 
            this.SetupMenu.Name = "SetupMenu";
            this.SetupMenu.Size = new System.Drawing.Size(138, 22);
            this.SetupMenu.Text = "Setup";
            this.SetupMenu.Click += new System.EventHandler(this.setupToolStripMenuItem_Click);
            // 
            // SetupMySQL
            // 
            this.SetupMySQL.Enabled = false;
            this.SetupMySQL.Name = "SetupMySQL";
            this.SetupMySQL.Size = new System.Drawing.Size(138, 22);
            this.SetupMySQL.Text = "Setup MySQL";
            this.SetupMySQL.Click += new System.EventHandler(this.SetupMySQL_Click);
            // 
            // importTKXToolStripMenuItem
            // 
            this.importTKXToolStripMenuItem.Enabled = false;
            this.importTKXToolStripMenuItem.Name = "importTKXToolStripMenuItem";
            this.importTKXToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.importTKXToolStripMenuItem.Text = "Import TKX";
            // 
            // frmProsesAll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 378);
            this.Controls.Add(this.nsTheme1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmProsesAll";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmProsesAll";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmProsesAll_FormClosing);
            this.Load += new System.EventHandler(this.frmProsesAll_Load);
            this.nsTheme1.ResumeLayout(false);
            this.gSetting.ResumeLayout(false);
            this.gProgress.ResumeLayout(false);
            this.gProgress.PerformLayout();
            this.MenuSetup.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private NSTheme nsTheme1;
        private NSGroupBox gProgress;
        private NSGroupBox gSetting;
        private NSTextBox txtInterval;
        private NSButton btnStop;
        private NSButton btnProses;
        private NSLabel nsLabel3;
        private NSLabel nsLabel1;
        private NSCheckBox checklistFile;
        private NSCheckBox Otomatis;
        private System.Windows.Forms.Timer tmrProses;
        private System.Windows.Forms.Timer timer1;
        private System.ComponentModel.BackgroundWorker bgWork1;
        private System.Windows.Forms.ListView listFile;
        private System.Windows.Forms.ListView listLog;
        private System.Windows.Forms.ColumnHeader TOKO;
        private System.Windows.Forms.ColumnHeader FILE;
        private System.Windows.Forms.ColumnHeader STATUS;
        private System.Windows.Forms.Label lblStatus;
        private NSProgressBar pgBar1;
        private NSControlButton nsControlButton1;
        private System.Windows.Forms.DateTimePicker prdAkhir;
        private System.Windows.Forms.DateTimePicker prdAwal;
        private NSContextMenu MenuSetup;
        private System.Windows.Forms.ToolStripMenuItem SetupMenu;
        private System.Windows.Forms.ToolStripMenuItem SetupMySQL;
        private NSButton btnSetup;
        private System.Windows.Forms.ToolStripMenuItem importTKXToolStripMenuItem;
        private System.Windows.Forms.Timer timer2;
    }
}