﻿using EIS.Net.Repository;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EIS.Net
{
    public partial class frmBRD : Form
    {
        EISRepository Fungsi = new EISRepository();
        DateTime Tgl = new DateTime();
        public frmBRD()
        {
            InitializeComponent();
        }

        private void btnProses_Click(object sender, EventArgs e)
        {
            Tgl = Tanggal.Value;
            btnProses.Text = "Prosesing.......";
            bgWork.RunWorkerAsync();
        }

        private void bgWork_DoWork(object sender, DoWorkEventArgs e)
        {
            Fungsi.CreateStruktur(Tgl.ToString("MMyyyy"));
            //Fungsi.ProsesBRD(Tgl);
            string TG = "TG" + int.Parse(Tgl.ToString("dd")).ToString();
            string TGL = int.Parse(Tgl.ToString("dd")).ToString();
            string PRS_FILE = "BRD" + Tgl.ToString("yyyyMMdd");
            string PRD = Tgl.ToString("MMyyyy");
            string SQL = "";
            bool s = Fungsi.Konversi_CSV("MSTXHG", Fungsi.FolderTemporary + "\\MSTXHG" + Tgl.ToString("MM") + ".CSV", ",", PRD);
            if (s)
            {
                string SUP_TMP = "MSTXHG" + PRD + "_TEMP";
                SQL = "TRUNCATE MSTXHG" + PRD + ";";
                SQL += "INSERT INTO MSTXHG" + PRD + "(TOKO,LOKASI,DOCNO,TANGGAL,PRDCD,QTY,PRICE,GROSS,RTYPE) SELECT TOKO,LOKASI,DOCNO,STR_TO_DATE(TANGGAL1,'%m/%d/%Y'),PRDCD,QTY,PRICE,GROSS,RTYPE FROM " + SUP_TMP + ";";
                //SQL += "ON DUPLICATE KEY UPDATE SNAMA=" + SUP_TMP + ".SNAMA,KOTA=" + SUP_TMP + ".SKOTA; ";
                //SQL += "INSERT INTO TRANSDC" + Periode + "(SHOP,PRDCD,NPB" + TGL + ") SELECT TOKO,PLU,QTY FROM " + ST_TMP + " WHERE RTYPE='NPB TOKO' ";
                //SQL += "ON DUPLICATE KEY UPDATE NPB" + TGL + "=BRD" + Periode + "_temp.QTY;";
                //Log("", "", SQL);
                //SQL += "UPDATE TRANSDC" + Periode + " A,STMASTDC" + PRD + " B SET A.STDC" + TGL + "=B.QTY" + TGL + " WHERE A.PRDCD=B.PRDCD;";
                //SQL += "UPDATE PBSL" + PRD + " A, STMASTDC" + PRD + " B SET A.QTY_STDC=B.QTY" + TGL + " WHERE A.PRDCD=B.PRDCD;";
                //SQL += "UPDATE PBSL" + PRD + " A, STOCKOUT" + PRD + " B SET A.STAUT=B.STAUT" + TGL + " WHERE A.PRDCD=B.PRDCD AND A.TOKO=B.SHOP;";
                SQL += "UPDATE ABSENFILEDC" + PRD + " SET TG" + TGL + "='1' WHERE `FILE`='MSTXHG';";
                using (MySqlConnection mConnection = Fungsi.db)
                {
                    using (MySqlTransaction trans = mConnection.BeginTransaction())
                    {
                        using (MySqlCommand myCmd = new MySqlCommand(SQL, mConnection, trans))
                        {
                            myCmd.CommandTimeout = 0;
                            myCmd.CommandType = CommandType.Text;
                            myCmd.ExecuteNonQuery();
                            //trans.Commit();
                        }
                    }
                }
            }

            else
            {
                MessageBox.Show("GGL");
            }
        }

        private void bgWork_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnProses.Text = "Proses";
            MessageBox.Show("BERES");
        }

        private void nsButton1_Click(object sender, EventArgs e)
        {

        }
    }
}
