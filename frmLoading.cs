﻿using EIS.Net.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EIS.Net
{
    public partial class frmLoading : Form
    {
        EISRepository Fungsi = new EISRepository();
        public frmLoading()
        {
            InitializeComponent();
            this.Icon = EIS.Net.Properties.Resources.IcoEIS;
            //EIS.NET
            nsTheme1.Text = "";
            lblVersi.Value1 = Fungsi.Versi.Substring(0,7);
            lblVersi.Value2 = " " + Fungsi.Versi.Substring(7);
            Fungsi.FolderHarian = Fungsi.BacaReg("FolderHarian");
            Fungsi.FolderTemporary = Fungsi.BacaReg("FolderTemporary");
            Fungsi.Aspera = Fungsi.BacaReg("Aspera");
            Fungsi.Cabang = Fungsi.BacaReg("Cabang");
            Fungsi.FtpDC = Fungsi.BacaReg("IpFTPDC");
            //Fungsi.OracleDC = "Data Source = (DESCRIPTION =  (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.38.1)(PORT = 1521)) ) (CONNECT_DATA = (SERVICE_NAME = dbg027) ));User Id=dcview;password=dcview;";
            Fungsi.OracleDC = Fungsi.BacaReg("OraDC");
            if (Fungsi.BacaReg("Interval") == null)
            {
                Fungsi.TulisReg("Interval", "10");
            }
            else
            {
                Fungsi.Interval = int.Parse(Fungsi.BacaReg("Interval"));
            }
            if (Fungsi.BacaReg("Periode") == null)
            {
                Fungsi.TulisReg("Periode", DateTime.Now.ToString("MM-yyyy"));
            }
            else
            {
                Fungsi.Periode = Fungsi.BacaReg("Periode").Replace("-", "");
            }

            if (Fungsi.Interval == 0)
            {
                Fungsi.Interval = 10;
            }
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            pgBar.Value += 2;
            if (pgBar.Value == 100)
            {
                if (!Fungsi.CheckConnection(Fungsi.BacaReg("Server"), Fungsi.BacaReg("Port"), Fungsi.BacaReg("User"), Fungsi.Decrypt(Fungsi.BacaReg("Password"), "!WaSu612IwA"), Fungsi.BacaReg("Database")))
                {
                    frmServer v = new frmServer();
                    Fungsi.isMdi = false;
                    v.Show();
                    this.Hide();
                }
                else
                {
                    Fungsi.Server = Fungsi.BacaReg("Server");
                    Fungsi.Port = Fungsi.BacaReg("Port");
                    Fungsi.Password = Fungsi.BacaReg("User");
                    Fungsi.User = Fungsi.Decrypt(Fungsi.BacaReg("Password"), "!WaSu612IwA");
                    Fungsi.Database = Fungsi.BacaReg("Database");
                    Fungsi.KillSleep();
                    frmProsesAll v = new frmProsesAll();
                    v.Show();
                    this.Hide();
                }
                timer1.Stop();
            }
        }
    }
}
