﻿using EIS.Net.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EIS.Net
{
    public partial class frmSetup : Form
    {
        EISRepository Fungsi = new EISRepository();

        public frmSetup()
        {
            InitializeComponent();
            txtFolderHarian.Text = Fungsi.FolderHarian;
            txtFolderTemp.Text = Fungsi.FolderTemporary;
            txtIpAspera.Text = Fungsi.Aspera;
            txtCabang.Text = Fungsi.Cabang;
            txtOraDC.Text = Fungsi.OracleDC;
            txtIpFTPDC.Text = Fungsi.FtpDC;
            Prd.Format = DateTimePickerFormat.Custom;
            Prd.CustomFormat = "MM-yyyy";
            Prd.Value = DateTime.Parse(Fungsi.BacaReg("Periode"));
            txtInterval.Text = Fungsi.Interval.ToString();
        }

        private void nsButton1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.ShowDialog();
            txtFolderHarian.Text = fd.SelectedPath;
            Fungsi.TulisReg("FolderHarian", fd.SelectedPath);
            Fungsi.FolderHarian = fd.SelectedPath;
        }

        private void nsButton2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.ShowDialog();
            txtFolderTemp.Text = fd.SelectedPath;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            Fungsi.TulisReg("FolderTemporary", txtFolderTemp.Text);
            Fungsi.FolderTemporary = txtFolderTemp.Text;
            Fungsi.TulisReg("FolderHarian", txtFolderHarian.Text);
            Fungsi.FolderHarian = txtFolderHarian.Text;
            Fungsi.TulisReg("Periode", Prd.Value.ToString("MM-yyyy"));
            Fungsi.Periode = Fungsi.BacaReg("Periode").Replace("-", "");
            Fungsi.TulisReg("Interval", int.Parse(txtInterval.Text));
            Fungsi.Interval = int.Parse(txtInterval.Text);
            Fungsi.TulisReg("Aspera", txtIpAspera.Text);
            Fungsi.Aspera = txtIpAspera.Text.Trim();
            Fungsi.TulisReg("Cabang", txtCabang.Text);
            Fungsi.Cabang = txtCabang.Text;
            Fungsi.OracleDC = txtOraDC.Text.Trim();
            Fungsi.TulisReg("OraDC", txtOraDC.Text);
            Fungsi.FtpDC = txtIpFTPDC.Text.Trim();
            Fungsi.TulisReg("IpFTPDC", txtIpFTPDC.Text);
            Fungsi.Pesan(this, "DATA BERHASIL DI SIMPAN", "INF");
        }

        private void txtOraDC_TextChanged(object sender, EventArgs e)
        {

        }

        private void nsGroupBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
