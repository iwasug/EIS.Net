﻿using Ionic.Zip;
using Microsoft.Win32;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using System.Data.OracleClient;
using System.Net;
using EnterpriseDT.Net.Ftp;

namespace EIS.Net.Repository
{
    class EISRepository
    {

        #region Setting
        //public MySqlConnection db;
        public MySqlCommand cmd;
        public string SQL;
        public string Versi
        {
           get { return Application.ProductName.ToUpper() + " v." + Assembly.GetEntryAssembly().GetName().Version.ToString(); }
        }
        private static string _server;
        private static string _servermysql;
        private static string _port;
        private static string _user;
        private static string _password;
        private static string _database;
        private static string _folderhrn;
        private static string _foldertemporary;
        private static string _periode;
        private static string _aspera;
        private static string _cabang;
        private static string _oracledc;
        private static string _ftpdc;
        private static MySqlConnection _db;
        private static DateTime _stopwatch;
        private List<string> ListSkipFile = new List<string>(new string[] { "QS" });
        public TimeSpan Stopwatch
        {
            set
            {
                _stopwatch = DateTime.Now;
            }
            get { return DateTime.Now - _stopwatch; }
        }

        public DataTable ListKonversi
        {
            set { _ListKonversi = value; }
            get { return _ListKonversi; }
        }
        public DataTable ListCekAbsen
        {
            set { _ListCekAbsen = value; }
            get { return _ListCekAbsen; }
        }
        public DataTable ListCekAbsenDC
        {
            set { _ListCekAbsenDC = value; }
            get { return _ListCekAbsenDC; }
        }
        public DataTable ListToko
        {
            set { _ListToko = value; }
            get { return _ListToko; }
        }
        private static DataTable _ListKonversi;
        private static DataTable _ListCekAbsen;
        private static DataTable _ListCekAbsenDC;
        private static DataTable _ListToko;
        public MySqlConnection db
        {
            set { _db = value; }
            get { return _db; }
        }
        public string Server
        {
            set { _server = value; }
            get { return _server; }
        }
        public string Port
        {
            set { _port = value; }
            get { return _port; }
        }
        public string User
        {
            set { _user = value; }
            get { return _user; }
        }
        public string Password
        {
            set { _password = value; }
            get { return _password; }
        }
        public string Database
        {
            set { _database = value; }
            get { return _database; }
        }
        public string FolderHarian
        {
            set { _folderhrn = value; }
            get { return _folderhrn; }
        }
        public string FolderTemporary
        {
            set { _foldertemporary = value; }
            get { return _foldertemporary; }
        }
        public string Periode
        {
            set { _periode = value; }
            get { return _periode; }
        }

        public string Aspera
        {
            set { _aspera = value; }
            get { return _aspera; }
        }

        public string Cabang
        {
            set { _cabang = value; }
            get { return _cabang; }
        }

        public string OracleDC
        {
            set { _oracledc = value; }
            get { return _oracledc; }
        }

        public string FtpDC
        {
            set { _ftpdc = value; }
            get { return _ftpdc; }
        }

        private static int _interval;
        public int Interval
        {
            set { _interval = value; }
            get { return _interval; }
        }

        public string ServerMySQL
        {
            set { _servermysql = value; }
            get { return _servermysql; }
        }

        RegistryKey baseRegistryKey = Registry.CurrentUser;
        private string subKey = "SOFTWARE\\" + Application.ProductName.ToUpper();
        public bool TulisReg(string KeyName, object Value)
        {
            try
            {
                RegistryKey rk = baseRegistryKey;
                RegistryKey sk1 = rk.CreateSubKey(subKey);
                sk1.SetValue(KeyName, Value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public string BacaReg(string KeyName)
        {
            RegistryKey rk = baseRegistryKey;
            RegistryKey sk1 = rk.OpenSubKey(subKey);
            if (sk1 == null)
            {
                return null;
            }
            else
            {
                try
                {
                    return (string)sk1.GetValue(KeyName);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public bool CheckConnection(string Server, string Port, string User, string Password, string Database)
        {
            try
            {
                ServerMySQL = String.Format("server={0};Port={1};user id={2};password={3};database={4};Convert Zero Datetime=True", Server, Port, User, Password, Database);
                using (MySqlConnection DB = new MySqlConnection(ServerMySQL))
                {
                    db = DB;
                }
                db.Open();
                TulisReg("Server", Server);
                TulisReg("Port", Port);
                TulisReg("User", User);
                TulisReg("Password", Encrypt(Password, "!WaSu612IwA"));
                TulisReg("Database", Database);
                DataTable dt = new DataTable();
                dt.Columns.Add("FILE", typeof(string));
                dt.Columns.Add("KOMA", typeof(string));
                dt.Rows.Add("DT", "|");
                dt.Rows.Add("CRDH", "|");
                dt.Rows.Add("SLP", ",");
                dt.Rows.Add("PKM", ",");
                dt.Rows.Add("WT", "|");
                dt.Rows.Add("ST", "|");
                dt.Rows.Add("NTB", "|");
                dt.Rows.Add("LQS", "|");
                //dt.Rows.Add("CL", ",");
                //dt.Rows.Add("CM", ",");
                //dt.Rows.Add("CA", ",");
                //dt.Rows.Add("LDR", ",");
                dt.Rows.Add("TL", ",");
                dt.Rows.Add("CIN", ",");
                dt.Rows.Add("DBT", ",");
                dt.Rows.Add("PBSL", "|");
                //dt.Rows.Add("TTD", "|");
                ListKonversi = dt;
                return true;
            }
            catch (Exception err)
            {
                //Log(err.ToString());
                Log("CEK KONSKSI", "", err.Message);
                return false;
            }
        }

        public void Pesan(Form frm, string msg, string tipe)
        {
            if (tipe.ToUpper() == "INF")
            {
                //MessageBox.Show(msg, Versi, MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(frm, msg,Versi, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (tipe.ToUpper() == "ERR")
            {
                //MessageBox.Show(msg, Versi, MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show(frm, msg, Versi, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        public bool UnzipFile(string filezip, string file, string folder)
        {
            try
            {
                using (ZipFile zip = ZipFile.Read(filezip))
                {
                    ZipEntry e = zip[file];
                    e.Extract(folder, ExtractExistingFileAction.OverwriteSilently);
                }
                return true;
            }
            catch (Exception err)
            {
                //Log(err.Message);
                if (err.Message != "Object reference not set to an instance of an object.")
                {
                    Log("UNZIP FILE " + file, filezip, err.Message);
                }
                return false;
            }
        }
        private static bool _isMdi;
        public bool isMdi
        {
            set { _isMdi = value; }
            get { return _isMdi; }
        }
        public bool GetSkipFile(string FileCsv)
        {
            if (ListSkipFile.Contains(FileCsv))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DataTable GetDataTable(string sql)
        {
            DataTable dt = new DataTable();
            try
            {
                using (MySqlConnection db1 = db)
                {
                    if (db1.State != ConnectionState.Open)
                    {
                        db1.Open();
                    }
                    cmd = new MySqlCommand(sql, db1);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    da.Fill(dt);
                }
            }
            catch (Exception err)
            {
                Log("GET DATATABLE", "", err.Message + sql);
            }
            finally
            {
                db.Close();
                db.Dispose();
            }
            return dt;
        }


        public void KillSleep()
        {
            try
            {
                SQL = "SHOW PROCESSLIST;";
                using (DataTable dt = GetDataTable(SQL))
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        int iPID = Convert.ToInt32(dr["Id"].ToString());
                        string strState = dr["Command"].ToString();
                        int iTime = Convert.ToInt32(dr["Time"].ToString());
                        string sql = "kill " + iPID;
                        if (strState == "Sleep" && iTime >= 100)
                        {
                            if (db.State != ConnectionState.Open)
                            {
                                db.Open();
                            }
                            cmd = new MySqlCommand(sql, db);
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
               
            }
            catch (Exception err)
            {
                Log("KILL SLEEP", "", err.ToString());
            }
            finally
            {
                db.Close();
                db.Dispose();
            }
        }

        public DataSet GetDataSet(string sql)
        {
            DataSet dt = new DataSet();
            try
            {
                if (db.State != ConnectionState.Open)
                {
                    db.Open();
                }
                cmd = new MySqlCommand(sql, db);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception err)
            {
                Log("GET DATASET", "", err.Message);
            }
            return dt;
        }


        public void Log(string proses, string file_hr, string Message)
        {
            StreamWriter sw = null;
            //Pesan(Message, "ERR");
            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\trace.txt"))
            {
                //Pesan(Message, "ERR");
            }
            string log = AppDomain.CurrentDomain.BaseDirectory + "\\Iwa.Log.txt";
            try
            {
                if (System.IO.File.Exists(log))
                {
                    long length = new System.IO.FileInfo(log).Length;
                    if (length >= 10485760)
                    {
                        System.IO.File.Delete(log);
                    }
                }
                sw = new StreamWriter(log, true);
                sw.WriteLine(DateTime.Now.ToString() + "////" + proses + "////" + Message);
                sw.Flush();
                sw.Close();
                if (db.State != ConnectionState.Open)
                {
                    db.Open();
                }
                SQL = "INSERT INTO TRACELOG_EIS(TANGGAL, PROSES, FILE_HR, LOG_ERR) VALUES(NOW(), @PROSES, @FILE_HR, @LOG_ERR)";
                cmd = new MySqlCommand(SQL, db);
                cmd.Parameters.AddWithValue("@PROSES", proses.ToUpper());
                cmd.Parameters.AddWithValue("@FILE_HR", file_hr.ToUpper());
                cmd.Parameters.AddWithValue("@LOG_ERR", Message);
                cmd.ExecuteNonQuery();
            }
            catch
            {

            }
        }

        public string Decrypt(string textToDecrypt, string key)
        {
            string pass = "";
            try
            {
                RijndaelManaged rijndaelManaged = new RijndaelManaged();
                rijndaelManaged.Mode = CipherMode.CBC;
                rijndaelManaged.Padding = PaddingMode.PKCS7;
                rijndaelManaged.KeySize = 128;
                rijndaelManaged.BlockSize = 128;
                byte[] inputBuffer = Convert.FromBase64String(textToDecrypt);
                byte[] bytes = Encoding.UTF8.GetBytes(key);
                byte[] numArray = new byte[16];
                int length = bytes.Length;
                if (length > numArray.Length)
                    length = numArray.Length;
                Array.Copy((Array)bytes, (Array)numArray, length);
                rijndaelManaged.Key = numArray;
                rijndaelManaged.IV = numArray;
                pass = Encoding.UTF8.GetString(rijndaelManaged.CreateDecryptor().TransformFinalBlock(inputBuffer, 0, inputBuffer.Length));
            }
            catch (Exception)
            {
                pass = "";
            }
            return pass;
        }

        public string Encrypt(string textToEncrypt, string key)
        {
            string pass = "";
            try
            {
                RijndaelManaged rijndaelManaged = new RijndaelManaged();
                rijndaelManaged.Mode = CipherMode.CBC;
                rijndaelManaged.Padding = PaddingMode.PKCS7;
                rijndaelManaged.KeySize = 128;
                rijndaelManaged.BlockSize = 128;
                byte[] bytes1 = Encoding.UTF8.GetBytes(key);
                byte[] numArray = new byte[16];
                int length = bytes1.Length;
                if (length > numArray.Length)
                    length = numArray.Length;
                Array.Copy((Array)bytes1, (Array)numArray, length);
                rijndaelManaged.Key = numArray;
                rijndaelManaged.IV = numArray;
                ICryptoTransform encryptor = rijndaelManaged.CreateEncryptor();
                byte[] bytes2 = Encoding.UTF8.GetBytes(textToEncrypt);
                pass = Convert.ToBase64String(encryptor.TransformFinalBlock(bytes2, 0, bytes2.Length));
            }
            catch (Exception)
            {
                pass = "";
            }
            return pass;
        }
        #region Fungsi Database
        public bool Konversi_CSV(string NamaFile, string LokasiFile, string Koma,string PRD)
        {
            string FL = NamaFile + PRD;
            try
            {
                if (db.State != ConnectionState.Open)
                {
                    db.Open();
                }
                StringBuilder rd = new StringBuilder();
                string file_kon = LokasiFile.Replace(@"\", @"\\");
                if (NamaFile == "QS")
                {
                    return true;
                }
                if (NamaFile == "PBSL")
                {
                    var csv = new StringBuilder();
                    var csvRows = File.ReadAllLines(LokasiFile);
                    foreach (string line in csvRows)
                    {
                        csv.Append(idm_Decrypt(line, "idm123"));
                        csv.Append("\r\n");
                    }
                    if (File.Exists(LokasiFile))
                    {
                        File.Delete(LokasiFile);
                    }
                    File.WriteAllText(LokasiFile, csv.ToString());
                }
                using (StreamReader reader = new StreamReader(LokasiFile))
                {
                    string HeaderCSV = reader.ReadLine().Replace(Koma, "`" + Koma + "`").Replace(Koma, " VARCHAR(100),").Replace("\"", "");
                    //Log("TES","TES",HeaderCSV);
                    rd.Append("DROP TABLE IF EXISTS " + FL + "_TEMP;");
                    rd.Append("CREATE TABLE " + FL + "_TEMP (`");
                    rd.Append(HeaderCSV + "` VARCHAR(100))ENGINE=MyISAM;").AppendLine();
                    if (NamaFile == "TKX" || NamaFile == "CM" || NamaFile == "CL" || NamaFile == "LDR" || NamaFile == "STDC" || NamaFile == "PRODMASTDC" || NamaFile == "SUPMAST" || NamaFile == "MSTXHG")
                    {
                        rd.Append("LOAD DATA LOCAL INFILE '" + file_kon + "' INTO TABLE " + FL + "_TEMP FIELDS TERMINATED BY '" + Koma + "' ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;");
                    }
                    else
                    {
                        rd.Append("LOAD DATA LOCAL INFILE '" + file_kon + "' INTO TABLE " + FL + "_TEMP FIELDS TERMINATED BY '" + Koma + "' ENCLOSED BY '''' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;");
                    }
                    SQL = rd.ToString();
                    //Log("SQL", "SS", SQL);
                    cmd = new MySqlCommand(SQL, db);
                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception err)
            {
                //Log(err.Message);
                Log("KONVERSI CSV", NamaFile, err.Message);
                return false;
            }

            /*
            try
            {
                using (MySqlConnection mConnection = db)
                {
                    if (mConnection.State != ConnectionState.Open)
                    {
                        mConnection.Open();
                    }
                    StringBuilder rd = new StringBuilder();
                    string file_kon = LokasiFile.Replace(@"\", @"\\");
                    using (StreamReader reader = new StreamReader(LokasiFile))
                    {
                        string HeaderCSV = reader.ReadLine().Replace(Koma, "`" + Koma + "`").Replace(Koma, " VARCHAR(100),");
                        rd.Append("DROP TABLE IF EXISTS " + NamaFile + "_TEMP;");
                        rd.Append("CREATE TABLE " + NamaFile + "_TEMP (`");
                        rd.Append(HeaderCSV + "` VARCHAR(100))ENGINE=MyISAM;").AppendLine();
                        if (NamaFile == "TKX")
                        {
                            rd.Append("LOAD DATA LOCAL INFILE '" + file_kon + "' INTO TABLE " + NamaFile + "_TEMP FIELDS TERMINATED BY '" + Koma + "' ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;");
                        }
                        else
                        {
                            rd.Append("LOAD DATA LOCAL INFILE '" + file_kon + "' INTO TABLE " + NamaFile + "_TEMP FIELDS TERMINATED BY '" + Koma + "' ENCLOSED BY '''' LINES TERMINATED BY '\r\n' IGNORE 1 LINES;");
                        }
                        SQL = rd.ToString();
                        using (MySqlTransaction trans = mConnection.BeginTransaction())
                        {
                            using (MySqlCommand myCmd = new MySqlCommand(SQL, mConnection, trans))
                            {
                                myCmd.CommandType = CommandType.Text;
                                myCmd.ExecuteNonQuery();
                                trans.Commit();
                            }
                        }
                    }
                    return true;
                }
            }
            catch (Exception err)
            {
                Log(err.Message);
                return false;
            }
            */
        }

        public string idm_Decrypt(string textToDecrypt, string key)
        {
            string pass = "";
            try
            {
                RijndaelManaged rijndaelManaged = new RijndaelManaged();
                rijndaelManaged.Mode = CipherMode.CBC;
                rijndaelManaged.Padding = PaddingMode.PKCS7;
                rijndaelManaged.KeySize = 128;
                rijndaelManaged.BlockSize = 128;
                byte[] inputBuffer = Convert.FromBase64String(textToDecrypt);
                byte[] bytes = Encoding.UTF8.GetBytes(key);
                byte[] numArray = new byte[16];
                int length = bytes.Length;
                if (length > numArray.Length)
                    length = numArray.Length;
                Array.Copy((Array)bytes, (Array)numArray, length);
                rijndaelManaged.Key = numArray;
                rijndaelManaged.IV = numArray;
                pass = Encoding.UTF8.GetString(rijndaelManaged.CreateDecryptor().TransformFinalBlock(inputBuffer, 0, inputBuffer.Length));
            }
            catch (Exception)
            {
                pass = "";
            }
            return pass;

        }



        public void BuatAbsen(string Toko, string Nama, string File)
        {
            try
            {
                if (db.State != ConnectionState.Open)
                {
                    db.Open();
                }
                SQL = "INSERT IGNORE ABSENFILE" + Periode + "(SHOP,NAMATK,FILE) ";
                SQL += "VALUES(@SHOP,@NAMATK,@FILE) ON DUPLICATE KEY UPDATE NAMATK=@NAMATK;";
                cmd = new MySqlCommand(SQL, db);
                cmd.Parameters.AddWithValue("@SHOP", Toko);
                cmd.Parameters.AddWithValue("@NAMATK", Nama);
                cmd.Parameters.AddWithValue("@FILE", File);
                cmd.ExecuteNonQuery();
            }
            catch (Exception err)
            {
                //Log(err.ToString());
                Log("BUAT ABSEN", "", err.Message);
            }
        }

        public string GetKoma(string file)
        {
            var query = (from p in ListKonversi.AsEnumerable()
                         where p.Field<string>("FILE") == file
                         select new
                         {
                             C = p.Field<string>("KOMA"),
                         }).Single();
            return query.C;
        }

        public bool CekAbsenV2(string Toko, string File, DateTime Tanggal)
        {
            bool result = false;
            string TG = "TG" + int.Parse(Tanggal.ToString("dd")).ToString();
            if (ListCekAbsen.Rows.Count != 0)
            {
                var query = (from p in ListCekAbsen.AsEnumerable()
                             where p.Field<string>("SHOP") == Toko && p.Field<string>("FILE") == File
                             select new
                             {
                                 C = p.Field<string>(TG),
                                 //age = p.Field<int>("age")
                             }).Single();
                //Pesan(query.C, "INF");
                if (query.C == "1")
                {
                    result = false;
                }
                else if (query.C == "0")
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }

        public bool CekAbsenDC(string File, DateTime Tanggal)
        {
            bool result = false;
            string TG = "TG" + int.Parse(Tanggal.ToString("dd")).ToString();
            if (ListCekAbsenDC.Rows.Count != 0)
            {
                var query = (from p in ListCekAbsenDC.AsEnumerable()
                             where p.Field<string>("FILE") == File
                             select new
                             {
                                 C = p.Field<string>(TG),
                                 //age = p.Field<int>("age")
                             }).Single();
                //Pesan(query.C, "INF");
                if (query.C == "1")
                {
                    result = false;
                }
                else if (query.C == "0")
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }

        public string GetToko(string KDTK, string COLOM)
        {
            var query = (from p in ListToko.AsEnumerable()
                         where p.Field<string>("TOKO_CODE") == KDTK
                         select new
                         {
                             C = p.Field<string>(COLOM),
                             //age = p.Field<int>("age")
                         }).Single();
            //Pesan(query.C, "INF");
            return query.C;
        }

        public bool CekAbsen(string Toko, string File, DateTime Tanggal)
        {
            bool result = false;
            try
            {
                if (db.State != ConnectionState.Open)
                {
                    db.Open();
                }
                string TG = "TG" + int.Parse(Tanggal.ToString("dd")).ToString();
                SQL = "SELECT " + TG + " FROM ABSENFILE" + Periode + " WHERE SHOP='" + Toko + "' AND FILE='" + File + "';";
                cmd = new MySqlCommand(SQL, db);
                if (cmd.ExecuteScalar().ToString() == "1")
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception err)
            {
                //Log(err.ToString());
                Log("CEK ABSEN", "", err.Message);
            }
            return result;
        }

        public bool CreateStruktur(string PERIODE)
        {
            try
            {
                //string ENGINE = "INNODB";
                string ENGINE = "MYISAM";
                /*
                if (db.State != ConnectionState.Open)
                {
                    db.Open();
                }
                */
                #region Create Table Absen File
                SQL = "CREATE TABLE IF NOT EXISTS `ABSENFILE" + PERIODE + "` (";
                SQL += "`SHOP` VARCHAR(4) NOT NULL DEFAULT '',";
                SQL += "`NAMATK` VARCHAR(60) DEFAULT '',";
                SQL += "`TG1` CHAR(1) DEFAULT '',";
                SQL += "`TG2` CHAR(1) DEFAULT '',";
                SQL += "`TG3` CHAR(1) DEFAULT '',";
                SQL += "`TG4` CHAR(1) DEFAULT '',";
                SQL += "`TG5` CHAR(1) DEFAULT '',";
                SQL += "`TG6` CHAR(1) DEFAULT '',";
                SQL += "`TG7` CHAR(1) DEFAULT '',";
                SQL += "`TG8` CHAR(1) DEFAULT '',";
                SQL += "`TG9` CHAR(1) DEFAULT '',";
                SQL += "`TG10` CHAR(1) DEFAULT '',";
                SQL += "`TG11` CHAR(1) DEFAULT '',";
                SQL += "`TG12` CHAR(1) DEFAULT '',";
                SQL += "`TG13` CHAR(1) DEFAULT '',";
                SQL += "`TG14` CHAR(1) DEFAULT '',";
                SQL += "`TG15` CHAR(1) DEFAULT '',";
                SQL += "`TG16` CHAR(1) DEFAULT '',";
                SQL += "`TG17` CHAR(1) DEFAULT '',";
                SQL += "`TG18` CHAR(1) DEFAULT '',";
                SQL += "`TG19` CHAR(1) DEFAULT '',";
                SQL += "`TG20` CHAR(1) DEFAULT '',";
                SQL += "`TG21` CHAR(1) DEFAULT '',";
                SQL += "`TG22` CHAR(1) DEFAULT '',";
                SQL += "`TG23` CHAR(1) DEFAULT '',";
                SQL += "`TG24` CHAR(1) DEFAULT '',";
                SQL += "`TG25` CHAR(1) DEFAULT '',";
                SQL += "`TG26` CHAR(1) DEFAULT '',";
                SQL += "`TG27` CHAR(1) DEFAULT '',";
                SQL += "`TG28` CHAR(1) DEFAULT '',";
                SQL += "`TG29` CHAR(1) DEFAULT '',";
                SQL += "`TG30` CHAR(1) DEFAULT '',";
                SQL += "`TG31` CHAR(1) DEFAULT '',";
                SQL += "`FILE` VARCHAR(45) DEFAULT NULL,";
                SQL += "`TOTAL` DOUBLE NOT NULL DEFAULT '0',";
                SQL += "PRIMARY KEY (`SHOP`,`FILE`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                SQL += "CREATE TABLE IF NOT EXISTS ABSENFILEDC" + Periode + " LIKE ABSENFILE" + Periode + ";";
                #endregion
                #region Create Table Rekap Sales
                SQL += "CREATE TABLE IF NOT EXISTS `REKAPSLS" + PERIODE + "` (";
                SQL += "`SHOP` VARCHAR(4) NOT NULL,";
                SQL += "`NAMA` VARCHAR(50) DEFAULT NULL,";
                SQL += "`TANGGAL` DATE NOT NULL DEFAULT '0000-00-00',";
                SQL += "`SALESGROSS` DOUBLE DEFAULT '0',";
                SQL += "`SALESNET` DOUBLE DEFAULT '0',";
                SQL += "`SALESHPP` DOUBLE DEFAULT '0',";
                SQL += "`STRUK` INT(11) DEFAULT '0',";
                SQL += "`UPD_DATE` DATE DEFAULT '0000-00-00',";
                SQL += "PRIMARY KEY(`SHOP`,`TANGGAL`)";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";
                #endregion
                #region Create Table Penjualan
                SQL += "CREATE TABLE IF NOT EXISTS `PENJUALAN" + PERIODE + "` (";
                SQL += "`SHOP` VARCHAR(4) NOT NULL DEFAULT '',";
                SQL += "`TANGGAL` DATE NOT NULL DEFAULT '0000-00-00',";
                SQL += "`RTYPE` CHAR(1) NOT NULL DEFAULT '',";
                SQL += "`DIV` CHAR(2) NOT NULL DEFAULT '',";
                SQL += "`CAT_COD` VARCHAR(5) DEFAULT '',";
                SQL += "`PRDCD` VARCHAR(8) NOT NULL DEFAULT '',";
                SQL += "`SLS_QTY` DOUBLE DEFAULT '0',";
                SQL += "`SLS_RP` DOUBLE DEFAULT '0',";
                SQL += "`SLS_DISC` DOUBLE DEFAULT '0',";
                SQL += "`SLS_HPP` DOUBLE DEFAULT '0',";
                SQL += "`SLS_BRS` DOUBLE DEFAULT '0',";
                SQL += "`STRUK` DOUBLE DEFAULT '0',";
                SQL += "`STK_QTY` DOUBLE DEFAULT '0',";
                SQL += "`STK_HPP` DOUBLE DEFAULT '0',";
                SQL += "`PPN` DOUBLE DEFAULT '0',";
                SQL += "`STRUK_TOKO` DOUBLE DEFAULT '0',";
                SQL += "`SALES_TYPE` CHAR(1) DEFAULT '0',";
                SQL += "`PKM_MIN` DOUBLE DEFAULT '0',";
                SQL += "`PKM_MAX` DOUBLE DEFAULT '0',";
                SQL += "`N_PLUS` DOUBLE DEFAULT '0',";
                SQL += "`N_KALI` DOUBLE DEFAULT '0',";
                SQL += "`MINOR` DOUBLE DEFAULT '0',";
                SQL += "`BKP` TINYINT(1) DEFAULT '0',";
                SQL += "`SUB_BKP` CHAR(1) DEFAULT '',";
                SQL += "PRIMARY KEY(`SHOP`,`TANGGAL`,`RTYPE`,`DIV`,`PRDCD`),";
                SQL += "KEY `KUNCIIDX2` (`TANGGAL`),";
                SQL += "KEY `KUNCIIDX3` (`RTYPE`),";
                SQL += "KEY `KUNCIIDX4` (`DIV`),";
                SQL += "KEY `KUNCIIDX5` (`PRDCD`)";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";
                #endregion
                #region Create Table MTOKO
                SQL += "CREATE TABLE IF NOT EXISTS  `MTOKO" + PERIODE + "` (";
                SQL += "`TOKO_CODE` VARCHAR(4) NOT NULL DEFAULT '',";
                SQL += "`KD_ORGAN1` VARCHAR(15) DEFAULT '',";
                SQL += "`NM_ORGAN1` VARCHAR(50) DEFAULT '',";
                SQL += "`KD_ORGAN2` VARCHAR(15) DEFAULT '',";
                SQL += "`NM_ORGAN2` VARCHAR(50) DEFAULT '',";
                SQL += "`KD_ORGAN3` VARCHAR(15) DEFAULT '',";
                SQL += "`NM_ORGAN3` VARCHAR(50) DEFAULT '',";
                SQL += "`KD_ORGAN4` VARCHAR(15) DEFAULT '',";
                SQL += "`NM_ORGAN4` VARCHAR(50) DEFAULT '',";
                SQL += "`KD_ORGAN5` VARCHAR(15) DEFAULT '',";
                SQL += "`NM_ORGAN5` VARCHAR(50) DEFAULT '',";
                SQL += "`TYPE_TOKO` CHAR(1) DEFAULT '',";
                SQL += "`NAMA_TOKO` VARCHAR(60) DEFAULT '',";
                SQL += "`ALAMAT` VARCHAR(100) DEFAULT NULL,";
                SQL += "`KOTA` VARCHAR(30) DEFAULT '',";
                SQL += "`JENIS` CHAR(1) DEFAULT '0',";
                SQL += "`TGL_TUTUP` DATETIME DEFAULT NULL,";
                SQL += "`TGL_BUKA` DATETIME DEFAULT NULL,";
                SQL += "`STATUS` CHAR(3) DEFAULT NULL,";
                SQL += "`KODE_CID` VARCHAR(3) DEFAULT NULL,";
                SQL += "`BUDGET` DOUBLE DEFAULT NULL,";
                SQL += "`TIDAK_HITUNG_PB` VARCHAR(1) DEFAULT '',";
                SQL += "PRIMARY KEY(`TOKO_CODE`)";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";
                #endregion
                #region Create Table WT
                SQL += "CREATE TABLE IF NOT EXISTS  `WTRAN" + PERIODE + "`(";
                SQL += "`RCID` CHAR(1) DEFAULT '',";
                SQL += "`RTYPE` CHAR(1) NOT NULL DEFAULT '',";
                SQL += "`DOCNO` INT(11) NOT NULL DEFAULT '0',";
                SQL += "`DIV` CHAR(2) DEFAULT '',";
                SQL += "`PRDCD` VARCHAR(8) NOT NULL DEFAULT '',";
                SQL += "`QTY` DOUBLE DEFAULT '0',";
                SQL += "`PRICE` DOUBLE DEFAULT '0',";
                SQL += "`GROSS` DOUBLE DEFAULT '0',";
                SQL += "`PPN` DOUBLE DEFAULT '0',";
                SQL += "`DOCNO2` DOUBLE DEFAULT '0',";
                SQL += "`ISTYPE` CHAR(2) DEFAULT '',";
                SQL += "`INVNO` VARCHAR(60) DEFAULT '0',";
                SQL += "`TOKO` VARCHAR(4) DEFAULT '',";
                SQL += "`KETERANGAN` CHAR(50) DEFAULT '',";
                SQL += "`TTYPE` CHAR(50) DEFAULT '',";
                SQL += "`TGL1` DATE NOT NULL DEFAULT '0000-00-00',";
                SQL += "`TGL2` DATE DEFAULT '0000-00-00',";
                SQL += "`DOCNO3` DOUBLE DEFAULT '0',";
                SQL += "`SHOP` VARCHAR(4) NOT NULL DEFAULT '',";
                SQL += "`FILE_SUMBER` VARCHAR(20) DEFAULT '',";
                SQL += "`GROSS_JUAL` DOUBLE DEFAULT '0',";
                SQL += "`PRICE_JUAL` DOUBLE DEFAULT '0',";
                SQL += "PRIMARY KEY(`SHOP`,`TGL1`,`RTYPE`,`DOCNO`,`PRDCD`),";
                SQL += "KEY `KUNCIIDX` (`DOCNO`),";
                SQL += "KEY `KUNCIIDX2` (`TGL1`),";
                SQL += "KEY `KUNCIIDX3` (`SHOP`),";
                SQL += "KEY `KUNCIIDX4` (`RTYPE`),";
                SQL += "KEY `KUNCIIDX5` (`PRDCD`),";
                SQL += "KEY `KUNCIIDX6` (`DIV`)";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";
                #endregion
                #region Create Table PKM
                SQL += "CREATE TABLE IF NOT EXISTS  `PKM" + PERIODE + "` (";
                SQL += "`TOKO` VARCHAR(4) NOT NULL,";
                SQL += "`NAMA` VARCHAR(50) NOT NULL,";
                SQL += "`TANGGAL` DATE NOT NULL DEFAULT '0000-00-00',";
                SQL += "`TUA` VARCHAR(8) DEFAULT NULL,";
                SQL += "`MODE` VARCHAR(2) DEFAULT NULL,";
                SQL += "`TIPE_TOKO` VARCHAR(2) DEFAULT NULL,";
                SQL += "`TIPE_HARGA` VARCHAR(2) DEFAULT NULL,";
                SQL += "`GUDANG_KIRIM` VARCHAR(4) DEFAULT NULL,";
                SQL += "`TGL_PKM` DATE DEFAULT '0000-00-00',";
                SQL += "`TB_RASIO` DOUBLE(12, 0) DEFAULT NULL,";
                SQL += "`FIXED_TB` DOUBLE(12, 0) DEFAULT NULL,";
                SQL += "`TBL_DIV` DOUBLE(12, 0) DEFAULT NULL,";
                SQL += "`TBL_SS` DOUBLE(12, 0) DEFAULT NULL,";
                SQL += "PRIMARY KEY  (`TOKO`,`NAMA`,`TANGGAL`)";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";

                SQL += "CREATE TABLE IF NOT EXISTS  `CEKPRD" + PERIODE + "` (";
                SQL += "`TOKO` VARCHAR(4) NOT NULL DEFAULT '',";
                SQL += "`NAMA` VARCHAR(50) DEFAULT '',";
                SQL += "`TANGGAL` DATE DEFAULT '0000-00-00',";
                SQL += "`PROD` VARCHAR(60) DEFAULT '',";
                SQL += "`PROD_TEL` VARCHAR(60) DEFAULT '',";
                SQL += "`TYPE_HARGA` CHAR(2) DEFAULT '',";
                SQL += "`TYPE_RAK` CHAR(2) DEFAULT '',";
                SQL += "`KAT_PROD` VARCHAR(60) DEFAULT '',";
                SQL += "`T_HRGKHS` CHAR(2) DEFAULT '',";
                SQL += "`THK_M` DATE DEFAULT '0000-00-00',";
                SQL += "`THK_A` DATE DEFAULT '0000-00-00',";
                SQL += "`NPWP` VARCHAR(25) DEFAULT '',";
                SQL += "`NPWPT` VARCHAR(25) DEFAULT '',";
                SQL += "`PERIODE` INT(6) DEFAULT '0',";
                SQL += "`SALES` VARCHAR(40) DEFAULT '',";
                SQL += "`SALES1` VARCHAR(40) DEFAULT '',";
                SQL += "`SALES4` VARCHAR(40) DEFAULT '',";
                SQL += "`SALES3` VARCHAR(40) DEFAULT '',";
                SQL += "`SALES2` VARCHAR(40) DEFAULT '',";
                SQL += "`STOCK` VARCHAR(40) DEFAULT '',";
                SQL += "`STOCK2` VARCHAR(40) DEFAULT '',";
                SQL += "`TRANSPRD` VARCHAR(40) DEFAULT '',";
                SQL += "`LPPTK` VARCHAR(40) DEFAULT '',";
                SQL += "`RRAK` VARCHAR(40) DEFAULT '',";
                SQL += "`UPHR` VARCHAR(40) DEFAULT '',";
                SQL += "`TUA` CHAR(8) DEFAULT '',";
                SQL += "`MODE` CHAR(2) DEFAULT '',";
                SQL += "`TGL_PKM` DATE DEFAULT '0000-00-00',";
                SQL += "`TB_RASIO` INT(8) DEFAULT '0',";
                SQL += "`FIXED_TB` INT(8) DEFAULT '0',";
                SQL += "`TBL_DIV` INT(8) DEFAULT '0',";
                SQL += "`TBL_SS` INT(8) DEFAULT '0',";
                SQL += "`JDL_BUAH` VARCHAR(8) DEFAULT '',";
                SQL += "`SALDO_AKHIR` DOUBLE DEFAULT '0',";
                SQL += "`RP_SLD_AKH` DOUBLE DEFAULT '0',";
                SQL += "`SALDO_AWAL` DOUBLE DEFAULT '0',";
                SQL += "`RP_SLD_AWL` DOUBLE DEFAULT '0',";
                SQL += "`SELISIHQ` DOUBLE DEFAULT '0',";
                SQL += "`SELISIHR` DOUBLE DEFAULT '0',";
                SQL += "`QTY_PKM` DOUBLE DEFAULT '0',";
                SQL += "`RP_PKM` DOUBLE DEFAULT '0',";
                SQL += "`LISTFILE` TEXT,";
                SQL += "`LAINLAIN` VARCHAR(250) DEFAULT '',";
                SQL += "PRIMARY KEY(`TOKO`)";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";
                #endregion
                #region Create Table SLP
                SQL += "CREATE TABLE IF NOT EXISTS  `SLP" + PERIODE + "` (";
                SQL += "`KDTK` varchar(4) NOT NULL,";
                SQL += "`NAMA` varchar(100) NOT NULL,";
                SQL += "`TGL_DATA` varchar(15) NOT NULL,";
                SQL += "`KODE` varchar(2) NOT NULL,";
                SQL += "`SUBKODE` varchar(5) NOT NULL,";
                SQL += "`DESC` varchar(30) NOT NULL default '',";
                SQL += "`RUPIAH` decimal(12, 2) NOT NULL default '0.00',";
                SQL += "`TANGGAL` date default NULL,";
                SQL += "`TP_111` decimal(5, 0) NOT NULL default '0',";
                SQL += "`TP_112` decimal(5, 0) NOT NULL default '0',";
                SQL += "`TP_121` decimal(5, 0) NOT NULL default '0',";
                SQL += "`TP_122` decimal(5, 0) NOT NULL default '0',";
                SQL += "`TP_131` decimal(5, 0) NOT NULL default '0',";
                SQL += "`TP_132` decimal(5, 0) NOT NULL default '0',";
                SQL += "`TP_141` decimal(5, 0) NOT NULL default '0',";
                SQL += "`TP_142` decimal(5, 0) NOT NULL default '0',";
                SQL += "`TP_211` decimal(5, 0) NOT NULL default '0',";
                SQL += "`TP_212` decimal(5, 0) NOT NULL default '0',";
                SQL += "`TP_221` decimal(5, 0) NOT NULL default '0',";
                SQL += "`TP_222` decimal(5, 0) NOT NULL default '0',";
                SQL += "`TP_231` decimal(5, 0) NOT NULL default '0',";
                SQL += "`TP_232` decimal(5, 0) NOT NULL default '0',";
                SQL += "`TP_241` decimal(5, 0) NOT NULL default '0',";
                SQL += "`TP_242` decimal(5, 0) NOT NULL default '0',";
                SQL += "`PRICE` decimal(11, 3) NOT NULL default '0.000',";
                SQL += "`QTY` decimal(9, 0) NOT NULL default '0',";
                SQL += "`GROSS` decimal(12, 0) NOT NULL default '0',";
                SQL += "`PPN` decimal(14, 6) NOT NULL default '0.000000',";
                SQL += "`NO_KSM` varchar(50) NOT NULL default '',";
                SQL += "`DOCNO` varchar(20) NOT NULL default '',";
                SQL += "`FTILLID` varchar(36) NOT NULL default '',";
                SQL += "`PLU` varchar(8) NOT NULL default '',";
                SQL += "`NO_HP` varchar(20) NOT NULL default '',";
                SQL += "`NO_BPB` decimal(7, 0) NOT NULL default '0',";
                SQL += "`TGL_BPB` date default NULL,";
                SQL += "`RP_BPB` decimal(12, 0) NOT NULL default '0',";
                SQL += "`FBASECODE` varchar(18) NOT NULL default '',";
                SQL += "`FSTOREID` varchar(4) NOT NULL default '',";
                SQL += "`FCASHIER` varchar(20) NOT NULL default '',";
                SQL += "`FTRANSID` varchar(5) NOT NULL default '',";
                SQL += "`FTIME` varchar(8) NOT NULL default '',";
                SQL += "`KIRIM_WU` decimal(13, 2) NOT NULL default '0.00',";
                SQL += "`FEE_WU` decimal(13, 2) NOT NULL default '0.00',";
                SQL += "`AMBIL_WU` decimal(13, 2) NOT NULL default '0.00',";
                SQL += "`MTCN` varchar(40) NOT NULL default '',";
                SQL += "`No_TRACE` varchar(10) NOT NULL default '',";
                SQL += "`No_Appro` varchar(10) NOT NULL default '',";
                SQL += "`No_Batch` varchar(10) NOT NULL default '',";
                SQL += "`Nm_KA` varchar(25) NOT NULL default '',";
                SQL += "PRIMARY KEY  (`KDTK`,`NAMA`,`TGL_DATA`,`KODE`,`SUBKODE`)";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = latin1;";
                #endregion
                #region Create Table SLP
                SQL += "CREATE TABLE IF NOT EXISTS `SLP` (";
                SQL += "`TOKO` VARCHAR(4) NOT NULL DEFAULT '',";
                SQL += "`NAMA` VARCHAR(50) DEFAULT '',";
                SQL += "`TANGGAL` DATE NOT NULL DEFAULT '0000-00-00',";
                SQL += "`SALES` DOUBLE DEFAULT '0',";
                SQL += "`SLSHPP` DOUBLE DEFAULT '0',";
                SQL += "`PPN` DOUBLE DEFAULT '0',";
                SQL += "`COUNTER1` DOUBLE DEFAULT '0',";
                SQL += "`COUNTER2` DOUBLE DEFAULT '0',";
                SQL += "`PPN_CTR2` DOUBLE DEFAULT '0',";
                SQL += "`CASH` DOUBLE DEFAULT '0',";
                SQL += "`DBT_BCA` DOUBLE DEFAULT '0',";
                SQL += "`TN_BCA` DOUBLE DEFAULT '0',";
                SQL += "`KPN_IDM` DOUBLE DEFAULT '0',";
                SQL += "`KPN_NIDM` DOUBLE DEFAULT '0',";
                SQL += "`DISCOUNT` DOUBLE DEFAULT '0',";
                SQL += "`VARIANCE` DOUBLE DEFAULT '0',";
                SQL += "`JML_STRK` DOUBLE DEFAULT '0',";
                SQL += "`BUKA` TIME DEFAULT '00:00:00',";
                SQL += "`TUTUP` TIME DEFAULT '00:00:00',";
                SQL += "`LOG_KIRIM` DATE DEFAULT '0000-00-00',";
                SQL += "`TGL_KIRIM` DATE DEFAULT '0000-00-00',";
                SQL += "`JAM_KIRIM` TIME DEFAULT '00:00:00',";
                SQL += "`STK_OUT` DOUBLE DEFAULT '0',";
                SQL += "`BRG_HLG` DOUBLE DEFAULT '0',";
                SQL += "`BRG_RSK` DOUBLE DEFAULT '0',";
                SQL += "`TGLRRAK` DATE DEFAULT '0000-00-00',";
                SQL += "`TELEPON` DOUBLE DEFAULT '0',";
                SQL += "`LISTRIK` DOUBLE DEFAULT '0',";
                SQL += "`KTG_KCL` DOUBLE DEFAULT '0',";
                SQL += "`KTG_SDG` DOUBLE DEFAULT '0',";
                SQL += "`KTG_BSR` DOUBLE DEFAULT '0',";
                SQL += "`SLS14601` DOUBLE DEFAULT '0',";
                SQL += "`PPN14601` DOUBLE DEFAULT '0',";
                SQL += "`KREDIT` DOUBLE DEFAULT '0',";
                SQL += "`PENGGANTIAN` DOUBLE DEFAULT '0',";
                SQL += "`PPN_BBS` DOUBLE DEFAULT '0',";
                SQL += "PRIMARY KEY  (`TOKO`,`TANGGAL`),";
                SQL += "KEY `KUNCIIDX1` (`TOKO`),";
                SQL += "KEY `KUNCIIDX2` (`TANGGAL`)";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";
                #endregion
                #region Create Table Stmast
                SQL += "CREATE TABLE IF NOT EXISTS  `STMAST" + PERIODE + "` (";
                SQL += "`SHOP` CHAR(4) NOT NULL,";
                SQL += "`PRDCD` VARCHAR(8) NOT NULL DEFAULT '',";
                SQL += "`CAT_COD` CHAR(6) DEFAULT '',";
                SQL += "`BEGBAL` DOUBLE DEFAULT '0',";
                SQL += "`SPD` DOUBLE DEFAULT '0',";
                SQL += "`QTY1` DOUBLE DEFAULT '0',";
                SQL += "`MAX1` DOUBLE DEFAULT '0',";
                SQL += "`STS1` INT(1) DEFAULT '0',";
                SQL += "`PKMG1` DOUBLE DEFAULT '0',";
                SQL += "`STAUT1` INT(1) DEFAULT '0',";
                SQL += "`QTY2` DOUBLE DEFAULT '0',";
                SQL += "`MAX2` DOUBLE DEFAULT '0',";
                SQL += "`STS2` INT(1) DEFAULT '0',";
                SQL += "`PKMG2` DOUBLE DEFAULT '0',";
                SQL += "`STAUT2` INT(1) DEFAULT '0',";
                SQL += "`QTY3` DOUBLE DEFAULT '0',";
                SQL += "`MAX3` DOUBLE DEFAULT '0',";
                SQL += "`STS3` INT(1) DEFAULT '0',";
                SQL += "`PKMG3` DOUBLE DEFAULT '0',";
                SQL += "`STAUT3` INT(1) DEFAULT '0',";
                SQL += "`QTY4` DOUBLE DEFAULT '0',";
                SQL += "`MAX4` DOUBLE DEFAULT '0',";
                SQL += "`STS4` INT(1) DEFAULT '0',";
                SQL += "`PKMG4` DOUBLE DEFAULT '0',";
                SQL += "`STAUT4` INT(1) DEFAULT '0',";
                SQL += "`QTY5` DOUBLE DEFAULT '0',";
                SQL += "`MAX5` DOUBLE DEFAULT '0',";
                SQL += "`STS5` INT(1) DEFAULT '0',";
                SQL += "`PKMG5` DOUBLE DEFAULT '0',";
                SQL += "`STAUT5` INT(1) DEFAULT '0',";
                SQL += "`QTY6` DOUBLE DEFAULT '0',";
                SQL += "`MAX6` DOUBLE DEFAULT '0',";
                SQL += "`STS6` INT(1) DEFAULT '0',";
                SQL += "`PKMG6` DOUBLE DEFAULT '0',";
                SQL += "`STAUT6` INT(1) DEFAULT '0',";
                SQL += "`QTY7` DOUBLE DEFAULT '0',";
                SQL += "`MAX7` DOUBLE DEFAULT '0',";
                SQL += "`STS7` INT(1) DEFAULT '0',";
                SQL += "`PKMG7` DOUBLE DEFAULT '0',";
                SQL += "`STAUT7` INT(1) DEFAULT '0',";
                SQL += "`QTY8` DOUBLE DEFAULT '0',";
                SQL += "`MAX8` DOUBLE DEFAULT '0',";
                SQL += "`STS8` INT(1) DEFAULT '0',";
                SQL += "`PKMG8` DOUBLE DEFAULT '0',";
                SQL += "`STAUT8` INT(1) DEFAULT '0',";
                SQL += "`QTY9` DOUBLE DEFAULT '0',";
                SQL += "`MAX9` DOUBLE DEFAULT '0',";
                SQL += "`STS9` INT(1) DEFAULT '0',";
                SQL += "`PKMG9` DOUBLE DEFAULT '0',";
                SQL += "`STAUT9` INT(1) DEFAULT '0',";
                SQL += "`QTY10` DOUBLE DEFAULT '0',";
                SQL += "`MAX10` DOUBLE DEFAULT '0',";
                SQL += "`STS10` INT(1) DEFAULT '0',";
                SQL += "`PKMG10` DOUBLE DEFAULT '0',";
                SQL += "`STAUT10` INT(1) DEFAULT '0',";
                SQL += "`QTY11` DOUBLE DEFAULT '0',";
                SQL += "`MAX11` DOUBLE DEFAULT '0',";
                SQL += "`STS11` INT(1) DEFAULT '0',";
                SQL += "`PKMG11` DOUBLE DEFAULT '0',";
                SQL += "`STAUT11` INT(1) DEFAULT '0',";
                SQL += "`QTY12` DOUBLE DEFAULT '0',";
                SQL += "`MAX12` DOUBLE DEFAULT '0',";
                SQL += "`STS12` INT(1) DEFAULT '0',";
                SQL += "`PKMG12` DOUBLE DEFAULT '0',";
                SQL += "`STAUT12` INT(1) DEFAULT '0',";
                SQL += "`QTY13` DOUBLE DEFAULT '0',";
                SQL += "`MAX13` DOUBLE DEFAULT '0',";
                SQL += "`STS13` INT(1) DEFAULT '0',";
                SQL += "`PKMG13` DOUBLE DEFAULT '0',";
                SQL += "`STAUT13` INT(1) DEFAULT '0',";
                SQL += "`QTY14` DOUBLE DEFAULT '0',";
                SQL += "`MAX14` DOUBLE DEFAULT '0',";
                SQL += "`STS14` INT(1) DEFAULT '0',";
                SQL += "`PKMG14` DOUBLE DEFAULT '0',";
                SQL += "`STAUT14` INT(1) DEFAULT '0',";
                SQL += "`QTY15` DOUBLE DEFAULT '0',";
                SQL += "`MAX15` DOUBLE DEFAULT '0',";
                SQL += "`STS15` INT(1) DEFAULT '0',";
                SQL += "`PKMG15` DOUBLE DEFAULT '0',";
                SQL += "`STAUT15` INT(1) DEFAULT '0',";
                SQL += "`QTY16` DOUBLE DEFAULT '0',";
                SQL += "`MAX16` DOUBLE DEFAULT '0',";
                SQL += "`STS16` INT(1) DEFAULT '0',";
                SQL += "`PKMG16` DOUBLE DEFAULT '0',";
                SQL += "`STAUT16` INT(1) DEFAULT '0',";
                SQL += "`QTY17` DOUBLE DEFAULT '0',";
                SQL += "`MAX17` DOUBLE DEFAULT '0',";
                SQL += "`STS17` INT(1) DEFAULT '0',";
                SQL += "`PKMG17` DOUBLE DEFAULT '0',";
                SQL += "`STAUT17` INT(1) DEFAULT '0',";
                SQL += "`QTY18` DOUBLE DEFAULT '0',";
                SQL += "`MAX18` DOUBLE DEFAULT '0',";
                SQL += "`STS18` INT(1) DEFAULT '0',";
                SQL += "`PKMG18` DOUBLE DEFAULT '0',";
                SQL += "`STAUT18` INT(1) DEFAULT '0',";
                SQL += "`QTY19` DOUBLE DEFAULT '0',";
                SQL += "`MAX19` DOUBLE DEFAULT '0',";
                SQL += "`STS19` INT(1) DEFAULT '0',";
                SQL += "`PKMG19` DOUBLE DEFAULT '0',";
                SQL += "`STAUT19` INT(1) DEFAULT '0',";
                SQL += "`QTY20` DOUBLE DEFAULT '0',";
                SQL += "`MAX20` DOUBLE DEFAULT '0',";
                SQL += "`STS20` INT(1) DEFAULT '0',";
                SQL += "`PKMG20` DOUBLE DEFAULT '0',";
                SQL += "`STAUT20` INT(1) DEFAULT '0',";
                SQL += "`QTY21` DOUBLE DEFAULT '0',";
                SQL += "`MAX21` DOUBLE DEFAULT '0',";
                SQL += "`STS21` INT(1) DEFAULT '0',";
                SQL += "`PKMG21` DOUBLE DEFAULT '0',";
                SQL += "`STAUT21` INT(1) DEFAULT '0',";
                SQL += "`QTY22` DOUBLE DEFAULT '0',";
                SQL += "`MAX22` DOUBLE DEFAULT '0',";
                SQL += "`STS22` INT(1) DEFAULT '0',";
                SQL += "`PKMG22` DOUBLE DEFAULT '0',";
                SQL += "`STAUT22` INT(1) DEFAULT '0',";
                SQL += "`QTY23` DOUBLE DEFAULT '0',";
                SQL += "`MAX23` DOUBLE DEFAULT '0',";
                SQL += "`STS23` INT(1) DEFAULT '0',";
                SQL += "`PKMG23` DOUBLE DEFAULT '0',";
                SQL += "`STAUT23` INT(1) DEFAULT '0',";
                SQL += "`QTY24` DOUBLE DEFAULT '0',";
                SQL += "`MAX24` DOUBLE DEFAULT '0',";
                SQL += "`STS24` INT(1) DEFAULT '0',";
                SQL += "`PKMG24` DOUBLE DEFAULT '0',";
                SQL += "`STAUT24` INT(1) DEFAULT '0',";
                SQL += "`QTY25` DOUBLE DEFAULT '0',";
                SQL += "`MAX25` DOUBLE DEFAULT '0',";
                SQL += "`STS25` INT(1) DEFAULT '0',";
                SQL += "`PKMG25` DOUBLE DEFAULT '0',";
                SQL += "`STAUT25` INT(1) DEFAULT '0',";
                SQL += "`QTY26` DOUBLE DEFAULT '0',";
                SQL += "`MAX26` DOUBLE DEFAULT '0',";
                SQL += "`STS26` INT(1) DEFAULT '0',";
                SQL += "`PKMG26` DOUBLE DEFAULT '0',";
                SQL += "`STAUT26` INT(1) DEFAULT '0',";
                SQL += "`QTY27` DOUBLE DEFAULT '0',";
                SQL += "`MAX27` DOUBLE DEFAULT '0',";
                SQL += "`STS27` INT(1) DEFAULT '0',";
                SQL += "`PKMG27` DOUBLE DEFAULT '0',";
                SQL += "`STAUT27` INT(1) DEFAULT '0',";
                SQL += "`QTY28` DOUBLE DEFAULT '0',";
                SQL += "`MAX28` DOUBLE DEFAULT '0',";
                SQL += "`STS28` INT(1) DEFAULT '0',";
                SQL += "`PKMG28` DOUBLE DEFAULT '0',";
                SQL += "`STAUT28` INT(1) DEFAULT '0',";
                SQL += "`QTY29` DOUBLE DEFAULT '0',";
                SQL += "`MAX29` DOUBLE DEFAULT '0',";
                SQL += "`STS29` INT(1) DEFAULT '0',";
                SQL += "`PKMG29` DOUBLE DEFAULT '0',";
                SQL += "`STAUT29` INT(1) DEFAULT '0',";
                SQL += "`QTY30` DOUBLE DEFAULT '0',";
                SQL += "`MAX30` DOUBLE DEFAULT '0',";
                SQL += "`STS30` INT(1) DEFAULT '0',";
                SQL += "`PKMG30` DOUBLE DEFAULT '0',";
                SQL += "`STAUT30` INT(1) DEFAULT '0',";
                SQL += "`QTY31` DOUBLE DEFAULT '0',";
                SQL += "`MAX31` DOUBLE DEFAULT '0',";
                SQL += "`STS31` INT(1) DEFAULT '0',";
                SQL += "`PKMG31` DOUBLE DEFAULT '0',";
                SQL += "`STAUT31` INT(1) DEFAULT '0',";
                SQL += "`STKQTY` DOUBLE DEFAULT '0',";
                SQL += "`TGL_STK` DATE DEFAULT '0000-00-00',";
                SQL += "`PKM` DOUBLE DEFAULT '0',";
                SQL += "`PKMG` DOUBLE DEFAULT '0',";
                SQL += "`ACOST` DOUBLE DEFAULT '0',";
                SQL += "`LCOST` DOUBLE DEFAULT '0',";
                SQL += "`PLCOST` DOUBLE DEFAULT '0',";
                SQL += "`PRCOST` DOUBLE DEFAULT '0',";
                SQL += "`PRICE` DOUBLE DEFAULT '0',";
                SQL += "`TP_GDG` VARCHAR(1) DEFAULT '',";
                SQL += "`RP_ADJ_K` DOUBLE DEFAULT '0',";
                SQL += "`RP_ADJ_L` DOUBLE DEFAULT '0',";
                SQL += "`MIN_DISP` VARCHAR(10) DEFAULT NULL,";
                SQL += "`KAP_DISP` VARCHAR(10) DEFAULT NULL,";
                SQL += "PRIMARY KEY  (`SHOP`,`PRDCD`),";
                SQL += "KEY `KUNCIIDX` (`PRDCD`)";
                SQL += ") ENGINE =" + ENGINE + " DEFAULT CHARSET=LATIN1;";
                #endregion
                #region Create Table StockOut
                SQL += "CREATE TABLE IF NOT EXISTS `STOCKOUT" + PERIODE + "` (";
                SQL += "`SHOP` CHAR(4) NOT NULL DEFAULT '',";
                SQL += "`PRDCD` CHAR(8) NOT NULL,";
                SQL += "`STAUT1` INT(1) DEFAULT '0',";
                SQL += "`STAUT2` INT(1) DEFAULT '0',";
                SQL += "`STAUT3` INT(1) DEFAULT '0',";
                SQL += "`STAUT4` INT(1) DEFAULT '0',";
                SQL += "`STAUT5` INT(1) DEFAULT '0',";
                SQL += "`STAUT6` INT(1) DEFAULT '0',";
                SQL += "`STAUT7` INT(1) DEFAULT '0',";
                SQL += "`STAUT8` INT(1) DEFAULT '0',";
                SQL += "`STAUT9` INT(1) DEFAULT '0',";
                SQL += "`STAUT10` INT(1) DEFAULT '0',";
                SQL += "`STAUT11` INT(1) DEFAULT '0',";
                SQL += "`STAUT12` INT(1) DEFAULT '0',";
                SQL += "`STAUT13` INT(1) DEFAULT '0',";
                SQL += "`STAUT14` INT(1) DEFAULT '0',";
                SQL += "`STAUT15` INT(1) DEFAULT '0',";
                SQL += "`STAUT16` INT(1) DEFAULT '0',";
                SQL += "`STAUT17` INT(1) DEFAULT '0',";
                SQL += "`STAUT18` INT(1) DEFAULT '0',";
                SQL += "`STAUT19` INT(1) DEFAULT '0',";
                SQL += "`STAUT20` INT(1) DEFAULT '0',";
                SQL += "`STAUT21` INT(1) DEFAULT '0',";
                SQL += "`STAUT22` INT(1) DEFAULT '0',";
                SQL += "`STAUT23` INT(1) DEFAULT '0',";
                SQL += "`STAUT24` INT(1) DEFAULT '0',";
                SQL += "`STAUT25` INT(1) DEFAULT '0',";
                SQL += "`STAUT26` INT(1) DEFAULT '0',";
                SQL += "`STAUT27` INT(1) DEFAULT '0',";
                SQL += "`STAUT28` INT(1) DEFAULT '0',";
                SQL += "`STAUT29` INT(1) DEFAULT '0',";
                SQL += "`STAUT30` INT(1) DEFAULT '0',";
                SQL += "`STAUT31` INT(1) DEFAULT '0',";
                SQL += "`ACOST` DOUBLE DEFAULT '0',";
                SQL += "`TP_GDG` VARCHAR(1) DEFAULT '',";
                SQL += "`SPD` DOUBLE DEFAULT '0',";
                SQL += "`PRICE` DOUBLE DEFAULT '0',";
                SQL += "`TAG` VARCHAR(1) DEFAULT '',";
                SQL += "PRIMARY KEY(`SHOP`,`PRDCD`),";
                SQL += "KEY `KUNCIIDX` (`PRDCD`)";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";
                #endregion
                #region Create Table STM
                SQL += "CREATE TABLE IF NOT EXISTS `STM" + PERIODE + "` (";
                SQL += "`SHOP` CHAR(4) NOT NULL DEFAULT '',";
                SQL += "`PRDCD` VARCHAR(8) NOT NULL DEFAULT '',";
                SQL += "`CAT_COD` CHAR(5) DEFAULT NULL,";
                SQL += "`BEGBAL` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`QTY` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`MAX` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`PRICE` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`ACOST` DECIMAL(10, 2) DEFAULT '0.00',";
                SQL += "`LCOST` DECIMAL(10, 2) DEFAULT '0.00',";
                SQL += "`SPD` DECIMAL(15, 6) DEFAULT '0.000000',";
                SQL += "`AQTY` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`BQTY` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`CQTY` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`DQTY` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`EQTY` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`FQTY` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`ST01` DECIMAL(3, 0) DEFAULT '0',";
                SQL += "`ST02` DECIMAL(3, 0) DEFAULT '0',";
                SQL += "`ST03` DECIMAL(3, 0) DEFAULT '0',";
                SQL += "`ST04` DECIMAL(3, 0) DEFAULT '0',";
                SQL += "`ST05` DECIMAL(3, 0) DEFAULT '0',";
                SQL += "`ST06` DECIMAL(3, 0) DEFAULT '0',";
                SQL += "`AHJ` DECIMAL(2, 0) DEFAULT '0',";
                SQL += "`BHJ` DECIMAL(2, 0) DEFAULT '0',";
                SQL += "`CHJ` DECIMAL(2, 0) DEFAULT '0',";
                SQL += "`DHJ` DECIMAL(2, 0) DEFAULT '0',";
                SQL += "`EHJ` DECIMAL(2, 0) DEFAULT '0',";
                SQL += "`FHJ` DECIMAL(2, 0) DEFAULT '0',";
                SQL += "`ARP` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`BRP` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`CRP` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`DRP` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`ERP` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`FRP` DECIMAL(12, 0) DEFAULT '0',";
                SQL += "`RP_ADJ_K` DOUBLE DEFAULT '0',";
                SQL += "`RP_ADJ_L` DOUBLE DEFAULT '0',";
                SQL += "`ADDDATE` DATETIME DEFAULT '0000-00-00 00:00:00',";
                SQL += "PRIMARY KEY(`SHOP`,`PRDCD`)";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";
                #endregion
                #region Create SLS
                SQL += "CREATE TABLE IF NOT EXISTS `SLS" + PERIODE + "` (";
                SQL += "`TOKO` VARCHAR(4) NOT NULL,";
                SQL += "`NAMA` VARCHAR(150) NOT NULL DEFAULT '',";
                SQL += "`SLS1` DOUBLE DEFAULT '0',";
                SQL += "`STK1` DOUBLE DEFAULT '0',";
                SQL += "`MGR1` DOUBLE DEFAULT '0',";
                SQL += "`SLS2` DOUBLE DEFAULT '0',";
                SQL += "`STK2` DOUBLE DEFAULT '0',";
                SQL += "`MGR2` DOUBLE DEFAULT '0',";
                SQL += "`SLS3` DOUBLE DEFAULT '0',";
                SQL += "`STK3` DOUBLE DEFAULT '0',";
                SQL += "`MGR3` DOUBLE DEFAULT '0',";
                SQL += "`SLS4` DOUBLE DEFAULT '0',";
                SQL += "`STK4` DOUBLE DEFAULT '0',";
                SQL += "`MGR4` DOUBLE DEFAULT '0',";
                SQL += "`SLS5` DOUBLE DEFAULT '0',";
                SQL += "`STK5` DOUBLE DEFAULT '0',";
                SQL += "`MGR5` DOUBLE DEFAULT '0',";
                SQL += "`SLS6` DOUBLE DEFAULT '0',";
                SQL += "`STK6` DOUBLE DEFAULT '0',";
                SQL += "`MGR6` DOUBLE DEFAULT '0',";
                SQL += "`SLS7` DOUBLE DEFAULT '0',";
                SQL += "`STK7` DOUBLE DEFAULT '0',";
                SQL += "`MGR7` DOUBLE DEFAULT '0',";
                SQL += "`SLS8` DOUBLE DEFAULT '0',";
                SQL += "`STK8` DOUBLE DEFAULT '0',";
                SQL += "`MGR8` DOUBLE DEFAULT '0',";
                SQL += "`SLS9` DOUBLE DEFAULT '0',";
                SQL += "`STK9` DOUBLE DEFAULT '0',";
                SQL += "`MGR9` DOUBLE DEFAULT '0',";
                SQL += "`SLS10` DOUBLE DEFAULT '0',";
                SQL += "`STK10` DOUBLE DEFAULT '0',";
                SQL += "`MGR10` DOUBLE DEFAULT '0',";
                SQL += "`SLS11` DOUBLE DEFAULT '0',";
                SQL += "`STK11` DOUBLE DEFAULT '0',";
                SQL += "`MGR11` DOUBLE DEFAULT '0',";
                SQL += "`SLS12` DOUBLE DEFAULT '0',";
                SQL += "`STK12` DOUBLE DEFAULT '0',";
                SQL += "`MGR12` DOUBLE DEFAULT '0',";
                SQL += "`SLS13` DOUBLE DEFAULT '0',";
                SQL += "`STK13` DOUBLE DEFAULT '0',";
                SQL += "`MGR13` DOUBLE DEFAULT '0',";
                SQL += "`SLS14` DOUBLE DEFAULT '0',";
                SQL += "`STK14` DOUBLE DEFAULT '0',";
                SQL += "`MGR14` DOUBLE DEFAULT '0',";
                SQL += "`SLS15` DOUBLE DEFAULT '0',";
                SQL += "`STK15` DOUBLE DEFAULT '0',";
                SQL += "`MGR15` DOUBLE DEFAULT '0',";
                SQL += "`SLS16` DOUBLE DEFAULT '0',";
                SQL += "`STK16` DOUBLE DEFAULT '0',";
                SQL += "`MGR16` DOUBLE DEFAULT '0',";
                SQL += "`SLS17` DOUBLE DEFAULT '0',";
                SQL += "`STK17` DOUBLE DEFAULT '0',";
                SQL += "`MGR17` DOUBLE DEFAULT '0',";
                SQL += "`SLS18` DOUBLE DEFAULT '0',";
                SQL += "`STK18` DOUBLE DEFAULT '0',";
                SQL += "`MGR18` DOUBLE DEFAULT '0',";
                SQL += "`SLS19` DOUBLE DEFAULT '0',";
                SQL += "`STK19` DOUBLE DEFAULT '0',";
                SQL += "`MGR19` DOUBLE DEFAULT '0',";
                SQL += "`SLS20` DOUBLE DEFAULT '0',";
                SQL += "`STK20` DOUBLE DEFAULT '0',";
                SQL += "`MGR20` DOUBLE DEFAULT '0',";
                SQL += "`SLS21` DOUBLE DEFAULT '0',";
                SQL += "`STK21` DOUBLE DEFAULT '0',";
                SQL += "`MGR21` DOUBLE DEFAULT '0',";
                SQL += "`SLS22` DOUBLE DEFAULT '0',";
                SQL += "`STK22` DOUBLE DEFAULT '0',";
                SQL += "`MGR22` DOUBLE DEFAULT '0',";
                SQL += "`SLS23` DOUBLE DEFAULT '0',";
                SQL += "`STK23` DOUBLE DEFAULT '0',";
                SQL += "`MGR23` DOUBLE DEFAULT '0',";
                SQL += "`SLS24` DOUBLE DEFAULT '0',";
                SQL += "`STK24` DOUBLE DEFAULT '0',";
                SQL += "`MGR24` DOUBLE DEFAULT '0',";
                SQL += "`SLS25` DOUBLE DEFAULT '0',";
                SQL += "`STK25` DOUBLE DEFAULT '0',";
                SQL += "`MGR25` DOUBLE DEFAULT '0',";
                SQL += "`SLS26` DOUBLE DEFAULT '0',";
                SQL += "`STK26` DOUBLE DEFAULT '0',";
                SQL += "`MGR26` DOUBLE DEFAULT '0',";
                SQL += "`SLS27` DOUBLE DEFAULT '0',";
                SQL += "`STK27` DOUBLE DEFAULT '0',";
                SQL += "`MGR27` DOUBLE DEFAULT '0',";
                SQL += "`SLS28` DOUBLE DEFAULT '0',";
                SQL += "`STK28` DOUBLE DEFAULT '0',";
                SQL += "`MGR28` DOUBLE DEFAULT '0',";
                SQL += "`SLS29` DOUBLE DEFAULT '0',";
                SQL += "`STK29` DOUBLE DEFAULT '0',";
                SQL += "`MGR29` DOUBLE DEFAULT '0',";
                SQL += "`SLS30` DOUBLE DEFAULT '0',";
                SQL += "`STK30` DOUBLE DEFAULT '0',";
                SQL += "`MGR30` DOUBLE DEFAULT '0',";
                SQL += "`SLS31` DOUBLE DEFAULT '0',";
                SQL += "`STK31` DOUBLE DEFAULT '0',";
                SQL += "`MGR31` DOUBLE DEFAULT '0',";
                SQL += "PRIMARY KEY(`TOKO`),";
                SQL += "KEY `KUNCIIDX` (`TOKO`)";
                SQL += ") ENGINE = MYISAM DEFAULT CHARSET = LATIN1;";
                #endregion
                #region Create Table Custab Gross
                //SQL += "CREATE TABLE `GR_CUSTAB` (";
                SQL += "CREATE TABLE IF NOT EXISTS `CUSTAB_GROSS_" + PERIODE + "` (";
                SQL += "`TANGGAL` DATE NOT NULL DEFAULT '0000-00-00',";
                SQL += "`STRUK` INT(1) NOT NULL DEFAULT '0',";
                SQL += "`SHOP` CHAR(4) NOT NULL,";
                SQL += "`SHIFT` CHAR(1) NOT NULL,";
                SQL += "`STATION` CHAR(3) NOT NULL,";
                SQL += "`DOCNO` CHAR(9) NOT NULL,";
                SQL += "`ITEM` BIGINT(21) NOT NULL DEFAULT '0',";
                SQL += "`GROSS` DECIMAL(34, 0) DEFAULT NULL,";
                SQL += "`JAM` VARBINARY(2) DEFAULT NULL,";
                SQL += "`JENIS` VARCHAR(20) DEFAULT NULL,";
                SQL += "`CJNS` VARCHAR(3) DEFAULT NULL,";
                SQL += "`ROWID` CHAR(2) NOT NULL DEFAULT ''";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";

                SQL += "CREATE TABLE IF NOT EXISTS `CUSTAB_GROSS" + PERIODE + "` (";
                SQL += "`SHOP` CHAR(4) NOT NULL,";
                SQL += "`TANGGAL` DATE NOT NULL DEFAULT '0000-00-00',";
                SQL += "`ITEM` BIGINT(21) DEFAULT NULL,";
                SQL += "`STRUK` DECIMAL(32, 0) DEFAULT NULL,";
                SQL += "`GROSS` DECIMAL(56, 0) DEFAULT NULL,";
                SQL += "`JAM` VARBINARY(2) DEFAULT NULL,";
                SQL += "`JENIS` VARCHAR(20) DEFAULT NULL,";
                SQL += "`ROWID` CHAR(2) NOT NULL DEFAULT '',";
                SQL += "PRIMARY KEY(`SHOP`,`TANGGAL`,`ROWID`)";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";

                #endregion
                #region Create Table Custab Jenis
                SQL += "CREATE TABLE IF NOT EXISTS `JENIS_CUSTAB_" + PERIODE + "` (";
                SQL += "`JENIS` VARCHAR(5) DEFAULT NULL,";
                SQL += "`BL` VARCHAR(2) DEFAULT NULL,";
                SQL += "`THN` VARCHAR(4) DEFAULT NULL,";
                SQL += "`PERIOD1` DATE DEFAULT NULL,";
                SQL += "`PERIOD2` DATE DEFAULT NULL";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";

                SQL += "CREATE TABLE IF NOT EXISTS `CUSTAB_IT_" + PERIODE + "` (";
                SQL += "`TANGGAL` DATE NOT NULL DEFAULT '0000-00-00',";
                SQL += "`SHOP` CHAR(4) NOT NULL,";
                SQL += "`SHIFT` CHAR(1) NOT NULL,";
                SQL += "`STATION` CHAR(3) NOT NULL,";
                SQL += "`STRUK` INT(1) NOT NULL DEFAULT '0',";
                SQL += "`ITEM` BIGINT(21) NOT NULL DEFAULT '0',";
                SQL += "`GROSS` DECIMAL(34, 0) DEFAULT NULL,";
                SQL += "`JAM` VARBINARY(2) DEFAULT NULL,";
                SQL += "`JENIS` VARCHAR(20) DEFAULT NULL,";
                SQL += "`CJNS` VARCHAR(3) DEFAULT NULL,";
                SQL += "`ROWID` CHAR(2) NOT NULL DEFAULT ''";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";

                //SQL += "CREATE TABLE `CUSTAB_ITEM122015` (";
                SQL += "CREATE TABLE IF NOT EXISTS `CUSTAB_ITEM" + PERIODE + "` (";
                SQL += "`SHOP` CHAR(4) NOT NULL,";
                SQL += "`TANGGAL` DATE NOT NULL DEFAULT '0000-00-00',";
                SQL += "`ITEM` BIGINT(21) NOT NULL DEFAULT '0',";
                SQL += "`STRUK` DECIMAL(41, 0) DEFAULT NULL,";
                SQL += "`GROSS` DECIMAL(56, 0) DEFAULT NULL,";
                SQL += "`JAM` VARBINARY(2) DEFAULT NULL,";
                SQL += "`JENIS` VARCHAR(20) DEFAULT NULL,";
                SQL += "`ROWID` CHAR(2) NOT NULL DEFAULT '',";
                SQL += "PRIMARY KEY(`SHOP`,`TANGGAL`,`ROWID`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create Table Custab Jam
                SQL += "CREATE TABLE IF NOT EXISTS `CUSTAB_JAM_" + PERIODE + "` (";
                SQL += "`TANGGAL` DATE NOT NULL DEFAULT '0000-00-00',";
                SQL += "`SHOP` CHAR(4) NOT NULL,";
                SQL += "`SHIFT` CHAR(1) NOT NULL,";
                SQL += "`STATION` CHAR(3) NOT NULL,";
                SQL += "`STRUK` INT(1) NOT NULL DEFAULT '0',";
                SQL += "`ITEM` BIGINT(21) NOT NULL DEFAULT '0',";
                SQL += "`GROSS` DECIMAL(34, 0) DEFAULT NULL,";
                SQL += "`JAM` VARBINARY(2) DEFAULT NULL,";
                SQL += "`JENIS` VARCHAR(20) DEFAULT NULL,";
                SQL += "`CJNS` VARCHAR(3) DEFAULT NULL,";
                SQL += "`ROWID` CHAR(2) NOT NULL DEFAULT ''";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";


                SQL += "CREATE TABLE IF NOT EXISTS `CUSTAB_JAM" + PERIODE + "` (";
                SQL += "`SHOP` CHAR(4) NOT NULL,";
                SQL += "`TANGGAL` DATE NOT NULL DEFAULT '0000-00-00',";
                SQL += "`ITEM` BIGINT(21) NOT NULL DEFAULT '0',";
                SQL += "`STRUK` DECIMAL(41, 0) DEFAULT NULL,";
                SQL += "`GROSS` DECIMAL(56, 0) DEFAULT NULL,";
                SQL += "`JAM` VARBINARY(2) DEFAULT NULL,";
                SQL += "`JENIS` VARCHAR(20) DEFAULT NULL,";
                SQL += "`ROWID` CHAR(2) NOT NULL DEFAULT '',";
                SQL += "PRIMARY KEY(`SHOP`,`TANGGAL`,`ROWID`)";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";
                #endregion
                #region Create Table Custab PLU
                SQL += "CREATE TABLE IF NOT EXISTS `CUSTAB_PLU_" + PERIODE + "` (";
                SQL += "`SHOP` VARCHAR(4) NOT NULL,";
                SQL += "`TANGGAL` DATE NOT NULL DEFAULT '0000-00-00',";
                SQL += "`PRDCD` VARCHAR(8) NOT NULL,";
                SQL += "`QTY` DOUBLE DEFAULT NULL,";
                SQL += "`GROSS` DOUBLE DEFAULT NULL,";
                SQL += "`JAM` CHAR(2) NOT NULL,";
                SQL += "`JENIS` VARCHAR(20) DEFAULT NULL,";
                SQL += "`ROWID` CHAR(2) NOT NULL DEFAULT '',";
                SQL += "PRIMARY KEY(`SHOP`,`TANGGAL`,`PRDCD`,`JAM`,`ROWID`)";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";

                SQL += "CREATE TABLE IF NOT EXISTS `CUSTAB_PLU" + PERIODE + "` (";
                SQL += "`SHOP` VARCHAR(4) NOT NULL,";
                SQL += "`TANGGAL` DATE NOT NULL DEFAULT '0000-00-00',";
                SQL += "`PRDCD` VARCHAR(8) NOT NULL,";
                SQL += "`QTY` DOUBLE DEFAULT NULL,";
                SQL += "`GROSS` DOUBLE DEFAULT NULL,";
                SQL += "`JAM` CHAR(2) NOT NULL,";
                SQL += "`JENIS` VARCHAR(20) DEFAULT NULL,";
                SQL += "`ROWID` CHAR(2) NOT NULL DEFAULT '',";
                SQL += "PRIMARY KEY(`SHOP`,`TANGGAL`,`PRDCD`,`JAM`,`ROWID`)";
                SQL += ") ENGINE = " + ENGINE + " DEFAULT CHARSET = LATIN1;";


                #endregion
                #region Create Stuktur sales Apka & Ecomers
                SQL += "CREATE TABLE IF NOT EXISTS `REKAPSLS_APKA" + PERIODE + "` (";
                SQL += "`SHOP` VARCHAR(4) NOT NULL DEFAULT '',";
                SQL += "`NAMA` VARCHAR(50) NOT NULL DEFAULT '',";
                SQL += "`TANGGAL` DATE NOT NULL DEFAULT 0,";
                SQL += "`SALES_APKA_QTY` DOUBLE NOT NULL DEFAULT 0,";
                SQL += "`SALES_APKA_GROSS` DOUBLE NOT NULL DEFAULT 0,";
                SQL += "`SALES_APKA_NET` DOUBLE NOT NULL DEFAULT 0,";
                SQL += "`SALES_APKA_HPP` DOUBLE NOT NULL DEFAULT 0,";
                SQL += "`SALES_ECO_QTY` DOUBLE NOT NULL DEFAULT 0,";
                SQL += "`SALES_ECO_GROSS` DOUBLE NOT NULL DEFAULT 0,";
                SQL += "`SALES_ECO_NET` DOUBLE NOT NULL DEFAULT 0,";
                SQL += "`SALES_ECO_HPP` DOUBLE NOT NULL DEFAULT 0,";
                SQL += "`TGLUPD` DATETIME,";
                SQL += "PRIMARY KEY(`SHOP`, `TANGGAL`)";
                SQL += ")";
                SQL += "ENGINE = " + ENGINE + ";";
                #endregion
                #region Create Table NTB
                SQL += "CREATE TABLE IF NOT EXISTS `NTB" + PERIODE + "` (";
                SQL += "`STATION` VARCHAR(100) NOT NULL DEFAULT '',";
                SQL += "`NO_STRUK` VARCHAR(100) NOT NULL,";
                SQL += "`TOKO` VARCHAR(100) NOT NULL DEFAULT '',";
                SQL += "`TANGGAL` DATE NOT NULL DEFAULT '0000-00-00',";
                SQL += "`KD_PROMO` VARCHAR(100) NOT NULL DEFAULT '',";
                SQL += "`GROUP` VARCHAR(100) NOT NULL DEFAULT '',";
                SQL += "`NOURUT` VARCHAR(100) NOT NULL DEFAULT '',";
                SQL += "`JNS_NTAMBAH` VARCHAR(100) NOT NULL DEFAULT '',";
                SQL += "`PLU_NTAMBAH` VARCHAR(100) NOT NULL DEFAULT '',";
                SQL += "`QTY_NTAMBAH` VARCHAR(100) NOT NULL DEFAULT '',";
                SQL += "`RP_NTAMBAH` VARCHAR(100) NOT NULL DEFAULT ''";
                SQL += ")ENGINE = " + ENGINE + ";";
                #endregion
                #region Create Table Tracelog
                SQL += "CREATE TABLE IF NOT EXISTS `TRACELOG_EIS` (";
                SQL += "`ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,";
                SQL += "`TANGGAL` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',";
                SQL += "`PROSES` VARCHAR(45) NOT NULL DEFAULT '',";
                SQL += "`FILE_HR` VARCHAR(45) NOT NULL DEFAULT '',";
                SQL += "`LOG_ERR` LONGTEXT,";
                SQL += "PRIMARY KEY(`ID`)";
                SQL += ") ENGINE = " + ENGINE + " AUTO_INCREMENT = 10; ";
                #endregion
                #region Create Table Absen Harian
                SQL += "CREATE TABLE IF NOT EXISTS `ABSENHR" + PERIODE + "` (";
                SQL += "`SHOP` VARCHAR(4) NOT NULL DEFAULT '',";
                SQL += "`NAMA` VARCHAR(145) NOT NULL DEFAULT '',";
                SQL += "`BUKA` DATE NOT NULL DEFAULT 0,";
                SQL += "`TANGGAL` DATE NOT NULL DEFAULT 0,";
                SQL += "`FILEHR` VARCHAR(45) NOT NULL DEFAULT '',";
                SQL += "`KOTA` VARCHAR(45) NOT NULL DEFAULT '',";
                SQL += "`KETERANGAN` LONGTEXT,";
                SQL += "`STATUS` VARCHAR(1) NOT NULL DEFAULT '',";
                SQL += "PRIMARY KEY(`SHOP`, `TANGGAL`)) ENGINE = " + ENGINE + ";";
                #endregion
                #region Create Table Jam Operational Toko
                SQL += "CREATE TABLE IF NOT EXISTS `OPRTOKO" + PERIODE + "` (";
                SQL += "`TOKO` VARCHAR(4) DEFAULT NULL,";
                SQL += "`NAMA` VARCHAR(50) DEFAULT NULL,";
                SQL += "`BUKA1` TIME DEFAULT NULL,";
                SQL += "`TUTUP1` TIME DEFAULT NULL,";
                SQL += "`BUKA2` TIME DEFAULT NULL,";
                SQL += "`TUTUP2` TIME DEFAULT NULL,";
                SQL += "`BUKA3` TIME DEFAULT NULL,";
                SQL += "`TUTUP3` TIME DEFAULT NULL,";
                SQL += "`BUKA4` TIME DEFAULT NULL,";
                SQL += "`TUTUP4` TIME DEFAULT NULL,";
                SQL += "`BUKA5` TIME DEFAULT NULL,";
                SQL += "`TUTUP5` TIME DEFAULT NULL,";
                SQL += "`BUKA6` TIME DEFAULT NULL,";
                SQL += "`TUTUP6` TIME DEFAULT NULL,";
                SQL += "`BUKA7` TIME DEFAULT NULL,";
                SQL += "`TUTUP7` TIME DEFAULT NULL,";
                SQL += "`BUKA8` TIME DEFAULT NULL,";
                SQL += "`TUTUP8` TIME DEFAULT NULL,";
                SQL += "`BUKA9` TIME DEFAULT NULL,";
                SQL += "`TUTUP9` TIME DEFAULT NULL,";
                SQL += "`BUKA10` TIME DEFAULT NULL,";
                SQL += "`TUTUP10` TIME DEFAULT NULL,";
                SQL += "`BUKA11` TIME DEFAULT NULL,";
                SQL += "`TUTUP11` TIME DEFAULT NULL,";
                SQL += "`BUKA12` TIME DEFAULT NULL,";
                SQL += "`TUTUP12` TIME DEFAULT NULL,";
                SQL += "`BUKA13` TIME DEFAULT NULL,";
                SQL += "`TUTUP13` TIME DEFAULT NULL,";
                SQL += "`BUKA14` TIME DEFAULT NULL,";
                SQL += "`TUTUP14` TIME DEFAULT NULL,";
                SQL += "`BUKA15` TIME DEFAULT NULL,";
                SQL += "`TUTUP15` TIME DEFAULT NULL,";
                SQL += "`BUKA16` TIME DEFAULT NULL,";
                SQL += "`TUTUP16` TIME DEFAULT NULL,";
                SQL += "`BUKA17` TIME DEFAULT NULL,";
                SQL += "`TUTUP17` TIME DEFAULT NULL,";
                SQL += "`BUKA18` TIME DEFAULT NULL,";
                SQL += "`TUTUP18` TIME DEFAULT NULL,";
                SQL += "`BUKA19` TIME DEFAULT NULL,";
                SQL += "`TUTUP19` TIME DEFAULT NULL,";
                SQL += "`BUKA20` TIME DEFAULT NULL,";
                SQL += "`TUTUP20` TIME DEFAULT NULL,";
                SQL += "`BUKA21` TIME DEFAULT NULL,";
                SQL += "`TUTUP21` TIME DEFAULT NULL,";
                SQL += "`BUKA22` TIME DEFAULT NULL,";
                SQL += "`TUTUP22` TIME DEFAULT NULL,";
                SQL += "`BUKA23` TIME DEFAULT NULL,";
                SQL += "`TUTUP23` TIME DEFAULT NULL,";
                SQL += "`BUKA24` TIME DEFAULT NULL,";
                SQL += "`TUTUP24` TIME DEFAULT NULL,";
                SQL += "`BUKA25` TIME DEFAULT NULL,";
                SQL += "`TUTUP25` TIME DEFAULT NULL,";
                SQL += "`BUKA26` TIME DEFAULT NULL,";
                SQL += "`TUTUP26` TIME DEFAULT NULL,";
                SQL += "`BUKA27` TIME DEFAULT NULL,";
                SQL += "`TUTUP27` TIME DEFAULT NULL,";
                SQL += "`BUKA28` TIME DEFAULT NULL,";
                SQL += "`TUTUP28` TIME DEFAULT NULL,";
                SQL += "`BUKA29` TIME DEFAULT NULL,";
                SQL += "`TUTUP29` TIME DEFAULT NULL,";
                SQL += "`BUKA30` TIME DEFAULT NULL,";
                SQL += "`TUTUP30` TIME DEFAULT NULL,";
                SQL += "`BUKA31` TIME DEFAULT NULL,";
                SQL += "`TUTUP31` TIME DEFAULT NULL,";
                SQL += "`TGLUPD` DATE DEFAULT NULL,";
                SQL += " PRIMARY KEY (`TOKO`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create Table RKBSPV
                SQL += "CREATE TABLE IF NOT EXISTS `RKBSPV" + PERIODE + "` (";
                SQL += "`HAR_DC_CODE` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_FK_ASPV_CODE` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`HAR_FK_ASPV_NIK` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_FK_ASPV_NAME` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_FK_TANGGAL` date NOT NULL DEFAULT '0000-00-00',";
                SQL += "`HAR_BULAN` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_TAHUN` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_TOK_CODE` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`HAR_TOK_NAME` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_GROUP_CODE` varchar(100) NOT NULL,";
                SQL += "`HAR_GROUP_NAMA` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_ITEM_CODE` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`HAR_ITEM_NAMA` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_ITEMDTL_CODE` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`HAR_ITEMDTL_NAMA` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_KONDISI` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_BOBOT` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_PENYEBAB` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_FOLLOWUP` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_KETERANGAN` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_PENCAPAIAN` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_FLAG_TDKADA_BOBOT` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_JAM_MASUK` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_JAM_KELUAR` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_TGL_CHECK` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_PENGGANTI` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_PASSWORD` varchar(100) DEFAULT NULL,";
                SQL += "PRIMARY KEY(`HAR_FK_ASPV_CODE`,`HAR_FK_TANGGAL`,`HAR_TOK_CODE`,`HAR_GROUP_CODE`,`HAR_ITEM_CODE`,`HAR_ITEMDTL_CODE`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create Tabel RKBAM
                SQL += "CREATE TABLE IF NOT EXISTS `RKBAM" + PERIODE + "` (";
                SQL += "`HAR_DC_CODE` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_FK_AMGR_CODE` varchar(100) NOT NULL,";
                SQL += "`HAR_FK_AMGR_NIK` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_FK_AMGR_NAME` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_FK_TANGGAL` date NOT NULL DEFAULT '0000-00-00',";
                SQL += "`HAR_BULAN` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_TAHUN` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_TOK_CODE` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`HAR_TOK_NAME` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_GROUP_CODE` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`HAR_GROUP_NAMA` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_ITEM_CODE` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`HAR_ITEM_NAMA` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_ITEMDTL_CODE` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`HAR_ITEMDTL_NAMA` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_KONDISI` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_BOBOT` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_PENYEBAB` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_FOLLOWUP` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_KETERANGAN` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_PENCAPAIAN` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_FLAG_TDKADA_BOBOT` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_JAM_MASUK` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_JAM_KELUAR` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_TGL_CHECK` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_PENGGANTI` varchar(100) DEFAULT NULL,";
                SQL += "`HAR_PASSWORD` varchar(100) DEFAULT NULL,";
                SQL += "PRIMARY KEY(`HAR_FK_AMGR_CODE`,`HAR_FK_TANGGAL`,`HAR_TOK_CODE`,`HAR_GROUP_CODE`,`HAR_ITEM_CODE`,`HAR_ITEMDTL_CODE`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create Tabel Cancel
                SQL += "CREATE TABLE IF NOT EXISTS `CANCEL" + PERIODE + "` (";
                SQL += "`RECID` varchar(1) DEFAULT NULL,";
                SQL += "`STATION` varchar(2) NOT NULL DEFAULT '',";
                SQL += "`SHIFT` varchar(2) NOT NULL DEFAULT '',";
                SQL += "`RTYPE` varchar(1) DEFAULT NULL,";
                SQL += "`DOCNO` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`SEQNO` varchar(5) DEFAULT NULL,";
                SQL += "`DIV` varchar(5) DEFAULT NULL,";
                SQL += "`CAT_COD` varchar(10) DEFAULT NULL,";
                SQL += "`PRDCD` varchar(8) NOT NULL DEFAULT '',";
                SQL += "`QTY` varchar(15) DEFAULT NULL,";
                SQL += "`QTY1` varchar(15) DEFAULT NULL,";
                SQL += "`PRICE` varchar(100) DEFAULT NULL,";
                SQL += "`HPP` varchar(100) DEFAULT NULL,";
                SQL += "`GROSS` varchar(100) DEFAULT NULL,";
                SQL += "`DISCOUNT` varchar(100) DEFAULT NULL,";
                SQL += "`SHOP` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`TANGGAL` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`JAM` varchar(100) DEFAULT NULL,";
                SQL += "`KONS` varchar(100) DEFAULT NULL,";
                SQL += "`TTYPE` varchar(100) DEFAULT NULL,";
                SQL += "`PTAG` varchar(100) DEFAULT NULL,";
                SQL += "`PROMOSI` varchar(100) DEFAULT NULL,";
                SQL += "`PPN` varchar(100) DEFAULT NULL,";
                SQL += "`KASIR_NAME` varchar(100) DEFAULT NULL,";
                SQL += "PRIMARY KEY(`SHOP`,`TANGGAL`,`SHIFT`,`STATION`,`DOCNO`,`PRDCD`,`RECID`,`SEQNO`,`JAM`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create Tabel Laundry
                SQL += "CREATE TABLE IF NOT EXISTS `LAUNDRY" + PERIODE + "` (";
                SQL += "`NONC` VARCHAR(15) NOT NULL,";
                SQL += "`NOLAYANAN` VARCHAR(10) DEFAULT NULL,";
                SQL += "`NCDATE` DATE NOT NULL DEFAULT '0000-00-00',";
                SQL += "`TOKO` VARCHAR(100) NOT NULL DEFAULT '',";
                SQL += "`SUPCO` VARCHAR(20) DEFAULT NULL,";
                SQL += "`NAMA` VARCHAR(45) DEFAULT NULL,";
                SQL += "`ALAMAT` VARCHAR(100) DEFAULT NULL,";
                SQL += "`NOTELP` VARCHAR(45) DEFAULT NULL,";
                SQL += "`PLU` VARCHAR(8) DEFAULT NULL,";
                SQL += "`DESKRIP` VARCHAR(100) DEFAULT NULL,";
                SQL += "`QTY` VARCHAR(5) DEFAULT NULL,";
                SQL += "`HARGA` VARCHAR(100) DEFAULT NULL,";
                SQL += "`RUPIAH` VARCHAR(100) DEFAULT NULL,";
                SQL += "`TGLPENGAMBILAN` VARCHAR(100) DEFAULT NULL,";
                SQL += "`TGLPEMBAYARAN` DATE DEFAULT NULL,";
                SQL += "`NOSJ` VARCHAR(100) DEFAULT NULL,";
                SQL += "`SJDATE` VARCHAR(100) DEFAULT NULL,";
                SQL += "`NOTTB` VARCHAR(100) DEFAULT NULL,";
                SQL += "`TTBDATE` VARCHAR(100) DEFAULT NULL,";
                SQL += "`STATUS` VARCHAR(100) DEFAULT NULL,";
                SQL += "PRIMARY KEY(`NONC`,`NCDATE`,`TOKO`,`STATUS`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create Tabel Dtran
                SQL += "CREATE TABLE IF NOT EXISTS `DTRAN" + PERIODE + "` (";
                SQL += "`RECID` varchar(1) DEFAULT NULL,";
                SQL += "`STATION` varchar(2) NOT NULL DEFAULT '',";
                SQL += "`SHIFT` varchar(2) NOT NULL DEFAULT '',";
                SQL += "`RTYPE` varchar(1) NOT NULL DEFAULT '',";
                SQL += "`DOCNO` varchar(15) NOT NULL DEFAULT '',";
                SQL += "`SEQNO` varchar(10) NOT NULL DEFAULT '',";
                SQL += "`DIV` varchar(2) DEFAULT NULL,";
                SQL += "`PRDCD` varchar(8) NOT NULL,";
                SQL += "`QTY` decimal(12, 0) NOT NULL DEFAULT '0',";
                SQL += "`PRICE` decimal(12, 6) DEFAULT NULL,";
                SQL += "`GROSS` decimal(12, 0) DEFAULT NULL,";
                SQL += "`DISCOUNT` decimal(12, 6) DEFAULT NULL,";
                SQL += "`SHOP` varchar(4) NOT NULL DEFAULT '',";
                SQL += "`TANGGAL` date NOT NULL,";
                SQL += "`JAM` time DEFAULT NULL,";
                SQL += "`KONS` varchar(1) DEFAULT NULL,";
                SQL += "`TTYPE` varchar(1) NOT NULL DEFAULT '',";
                SQL += "`HPP` decimal(12, 6) DEFAULT NULL,";
                SQL += "`PROMOSI` varchar(50) DEFAULT NULL,";
                SQL += "`PPN` decimal(12, 6) DEFAULT NULL,";
                SQL += "`PTAG` varchar(1) DEFAULT NULL,";
                SQL += "`CAT_COD` varchar(5) DEFAULT NULL,";
                SQL += "`NM_VCR` varchar(50) DEFAULT NULL,";
                SQL += "`NO_VCR` varchar(50) NOT NULL DEFAULT '',";
                SQL += "`BKP` varchar(1) DEFAULT NULL,";
                SQL += "`SUB_BKP` varchar(1) DEFAULT NULL,";
                SQL += "`PLUMD` varchar(8) NOT NULL DEFAULT '',";
                SQL += "`NOPESANAN` varchar(20) DEFAULT '',";
                SQL += "`NOACC` varchar(255) DEFAULT '',";
                SQL += "PRIMARY KEY(`SHOP`,`TANGGAL`,`PRDCD`,`STATION`,`SHIFT`,`DOCNO`,`RTYPE`,`SEQNO`,`QTY`,`TTYPE`,`PLUMD`,`NO_VCR`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create table Mprod
                SQL += "CREATE TABLE IF NOT EXISTS `MPROD" + PERIODE + "` (";
                SQL += "`KODENAS` VARCHAR(10) NOT NULL DEFAULT '',";
                SQL += "`DIVS_CODE` CHAR(2) DEFAULT '',";
                SQL += "`DIVS_NAME` VARCHAR(64) DEFAULT '',";
                SQL += "`DEPT_CODE` VARCHAR(6) DEFAULT '',";
                SQL += "`DEPT_NAME` VARCHAR(78) DEFAULT '',";
                SQL += "`CATG_CODE` VARCHAR(10) DEFAULT '',";
                SQL += "`CATG_NAME` VARCHAR(132) DEFAULT '',";
                SQL += "`PRDCD` VARCHAR(8) NOT NULL DEFAULT '',";
                SQL += "`NAMA` VARCHAR(160) DEFAULT '',";
                SQL += "`MERK` VARCHAR(30) DEFAULT '',";
                SQL += "`FLAVOUR` VARCHAR(30) DEFAULT '',";
                SQL += "`PTAG` CHAR(2) DEFAULT '',";
                SQL += "`SUPCO` VARCHAR(8) DEFAULT '',";
                SQL += "`TGLBERLAKU` DATE DEFAULT '0000-00-00',";
                SQL += "PRIMARY KEY(`PRDCD`),";
                SQL += "KEY `KUNCIIDX1` (`DIVS_CODE`,`DEPT_CODE`,`CATG_CODE`,`PRDCD`),";
                SQL += "KEY `KUNCIIDX2` (`DEPT_CODE`),";
                SQL += "KEY `KUNCIIDX3` (`CATG_CODE`),";
                SQL += "KEY `KUNCIIDX4` (`MERK`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create Table Supmast
                SQL += "CREATE TABLE IF NOT EXISTS `SUPMAST" + PERIODE + "` (";
                SQL += "`SUPCO` CHAR(4) NOT NULL DEFAULT '',";
                SQL += "`SNAMA` VARCHAR(60) DEFAULT '',";
                SQL += "`ALAMAT1` VARCHAR(50) DEFAULT '',";
                SQL += "`ALAMAT2` VARCHAR(50) DEFAULT '',";
                SQL += "`KOTA` VARCHAR(40) DEFAULT '',";
                SQL += "`STATUS` CHAR(2) DEFAULT '',";
                SQL += "`TOP` INT(5) DEFAULT '0'";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create Table Prodmast
                SQL += "CREATE TABLE IF NOT EXISTS `PRODMAST" + PERIODE + "` (";
                SQL += "`RECID` CHAR(1) DEFAULT NULL,";
                SQL += "`WARE_HOUSE` VARCHAR(4) DEFAULT NULL,";
                SQL += "`CAT_COD` VARCHAR(5) DEFAULT NULL,";
                SQL += "`PRDCD` CHAR(8) NOT NULL DEFAULT '',";
                SQL += "`DESC` VARCHAR(50) DEFAULT NULL,";
                SQL += "`SINGKAT` VARCHAR(50) DEFAULT NULL,";
                SQL += "`MERK` VARCHAR(25) DEFAULT NULL,";
                SQL += "`NAMA` VARCHAR(50) DEFAULT NULL,";
                SQL += "`FLAVOUR` VARCHAR(15) DEFAULT NULL,";
                SQL += "`KEMASAN` CHAR(3) DEFAULT NULL,";
                SQL += "`SIZE` VARCHAR(10) DEFAULT NULL,";
                SQL += "`BKP` CHAR(1) DEFAULT NULL,";
                SQL += "`DESC2` VARCHAR(50) DEFAULT NULL,";
                SQL += "`FRAC` DOUBLE DEFAULT NULL,";
                SQL += "`UNIT` VARCHAR(4) DEFAULT NULL,";
                SQL += "`ACOST` DOUBLE DEFAULT NULL,";
                SQL += "`RCOST` DOUBLE DEFAULT NULL,";
                SQL += "`LCOST` DOUBLE DEFAULT NULL,";
                SQL += "`PRICE_A` DOUBLE DEFAULT NULL,";
                SQL += "`PRICE_B` DOUBLE DEFAULT NULL,";
                SQL += "`PRICE_C` DOUBLE DEFAULT NULL,";
                SQL += "`PRICE_D` DOUBLE DEFAULT NULL,";
                SQL += "`PRICE_E` DOUBLE DEFAULT NULL,";
                SQL += "`PRICE_F` DOUBLE DEFAULT NULL,";
                SQL += "`DIV` CHAR(2) DEFAULT NULL,";
                SQL += "`CTGR` CHAR(2) DEFAULT NULL,";
                SQL += "`KONS` CHAR(1) DEFAULT NULL,";
                SQL += "`SUPCO` VARCHAR(4) DEFAULT NULL,";
                SQL += "`PTAG` CHAR(1) DEFAULT NULL,";
                SQL += "`TGL_HARGA1` DATETIME DEFAULT NULL,";
                SQL += "`TGL_HARGA2` DATETIME DEFAULT NULL,";
                SQL += "`TGL_HARGA3` DATETIME DEFAULT NULL,";
                SQL += "`TGL_HARGA4` DATETIME DEFAULT NULL,";
                SQL += "`TGL_HARGA5` DATETIME DEFAULT NULL,";
                SQL += "`TGL_HARGA6` DATETIME DEFAULT NULL,";
                SQL += "`REORDER` DOUBLE DEFAULT NULL,";
                SQL += "`BRG_AKTIF` CHAR(1) DEFAULT NULL,";
                SQL += "`TGL_TAMBAH` DATE DEFAULT NULL,";
                SQL += "PRIMARY KEY(`PRDCD`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create Table MSTXHG
                SQL += "CREATE TABLE IF NOT EXISTS `MSTXHG" + PERIODE + "` (";
                SQL += "`TOKO` varchar(4) NOT NULL DEFAULT '',";
                SQL += "`LOKASI` varchar(2) NOT NULL DEFAULT '',";
                SQL += "`DOCNO` varchar(45) NOT NULL DEFAULT '',";
                SQL += "`TANGGAL` date NOT NULL DEFAULT '0000-00-00',";
                SQL += "`PRDCD` varchar(8) NOT NULL DEFAULT '',";
                SQL += "`QTY` decimal(12, 0) NOT NULL DEFAULT '0',";
                SQL += "`PRICE` decimal(12, 3) NOT NULL DEFAULT '0.000',";
                SQL += "`GROSS` decimal(12, 3) NOT NULL DEFAULT '0.000',";
                SQL += "`RTYPE` varchar(1) NOT NULL DEFAULT '',";
                SQL += "PRIMARY KEY (`TOKO`,`LOKASI`,`DOCNO`,`TANGGAL`,`PRDCD`,`RTYPE`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create Table TLOG
                SQL += "CREATE TABLE IF NOT EXISTS `TLOG" + PERIODE + "` (";
                SQL += "`FSTOREID` varchar(4) NOT NULL DEFAULT '',";
                SQL += "`NM_TOKO` varchar(100) DEFAULT NULL,";
                SQL += "`ALAMAT` varchar(100) DEFAULT NULL,";
                SQL += "`KOTA` varchar(100) DEFAULT NULL,";
                SQL += "`STATION` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`SHIFT` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`FTILLID` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`FCASHIERID` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`FTRANSID` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`FDATE` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`FTIME` varchar(100) NOT NULL DEFAULT '',";
                SQL += "`FMSERIAL` varchar(100) DEFAULT NULL,";
                SQL += "`FBASECODE` varchar(100) DEFAULT NULL,";
                SQL += "`FLIVECODE` varchar(100) DEFAULT NULL,";
                SQL += "`FSKU` varchar(8) NOT NULL DEFAULT '',";
                SQL += "`NM_SKU` varchar(100) DEFAULT NULL,";
                SQL += "`FVALUE` varchar(100) DEFAULT NULL,";
                SQL += "`FLINEQTY` varchar(100) DEFAULT NULL,";
                SQL += "`FPHONE` varchar(100) DEFAULT NULL,";
                SQL += "`FTELCO` varchar(100) DEFAULT NULL,";
                SQL += "`TGL_BPB` varchar(100) DEFAULT NULL,";
                SQL += "`SUP_BPB` varchar(100) DEFAULT NULL,";
                SQL += "`NO_BPB` varchar(100) DEFAULT NULL,";
                SQL += "`RP_BPB` varchar(100) DEFAULT NULL,";
                SQL += "`PRICE_IDM` varchar(100) DEFAULT NULL,";
                SQL += "`PPNRP_IDM` varchar(100) DEFAULT NULL,";
                SQL += "`KD_DC` varchar(100) DEFAULT NULL,";
                SQL += "PRIMARY KEY(`FSTOREID`,`STATION`,`SHIFT`,`FTILLID`,`FCASHIERID`,`FTRANSID`,`FDATE`,`FSKU`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create Struktur PBSL
                SQL += "CREATE TABLE IF NOT EXISTS `PBSL" + PERIODE + "` (";
                SQL += "`RECID` VARCHAR(1) DEFAULT '',";
                SQL += "`RTYPE` VARCHAR(1) DEFAULT '',";
                SQL += "`DOCNO` DECIMAL(7, 0) NOT NULL,";
                SQL += "`PRDCD` VARCHAR(8) NOT NULL DEFAULT '',";
                SQL += "`SINGKAT` VARCHAR(28) DEFAULT '',";
                SQL += "`DIV` VARCHAR(2) DEFAULT '',";
                SQL += "`QTY` DECIMAL(9, 0) DEFAULT NULL,";
                SQL += "`TOKO` VARCHAR(4) NOT NULL DEFAULT '',";
                SQL += "`TGL_PB` DATE NOT NULL,";
                SQL += "`PKM_AKH` DECIMAL(12, 0) DEFAULT NULL,";
                SQL += "`LENGTH` DECIMAL(7, 2) DEFAULT NULL,";
                SQL += "`WIDTH` DECIMAL(7, 2) DEFAULT NULL,";
                SQL += "`HEIGHT` DECIMAL(7, 2) DEFAULT NULL,";
                SQL += "`MINOR` DECIMAL(5, 0) DEFAULT NULL,";
                SQL += "`GUDANG` VARCHAR(5) DEFAULT '',";
                SQL += "`MAX` DECIMAL(9, 0) DEFAULT NULL,";
                SQL += "`PKM_G` DECIMAL(9, 0) DEFAULT NULL,";
                SQL += "`QTYM1` DECIMAL(9, 0) DEFAULT NULL,";
                SQL += "`SPD` DECIMAL(9, 0) DEFAULT NULL,";
                SQL += "`PRICE` DECIMAL(10, 3) DEFAULT NULL,";
                SQL += "`GROSS` DECIMAL(10, 0) DEFAULT NULL,";
                SQL += "`PTAG` VARCHAR(1) DEFAULT '',";
                SQL += "`QTY_MAN` DECIMAL(9, 0) DEFAULT NULL,";
                SQL += "`PPN` DECIMAL(8, 0) DEFAULT NULL,";
                SQL += "`KM` DECIMAL(20, 5) DEFAULT NULL,";
                SQL += "`JAMIN` VARCHAR(8) DEFAULT '',";
                SQL += "`JAMOUT` VARCHAR(8) DEFAULT '',";
                SQL += "`NO_SJ` VARCHAR(10) DEFAULT '',";
                SQL += "`QTY_NPB` DECIMAL(9, 0) DEFAULT 0,";
                SQL += "`QTY_STDC` DECIMAL(9, 0) DEFAULT 0,";
                SQL += "`STAUT` VARCHAR(1) DEFAULT '',";
                SQL += "PRIMARY KEY (`DOCNO`,`PRDCD`,`TOKO`,`TGL_PB`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create struktur BRDDC
                SQL += "CREATE TABLE IF NOT EXISTS `BRDDC" + PERIODE + "` (";
                SQL += "`TOKO` VARCHAR(4) NOT NULL DEFAULT '',";
                SQL += "`DOCNO` VARCHAR(15) NOT NULL DEFAULT '',";
                SQL += "`RTYPE` VARCHAR(15) NOT NULL DEFAULT '',";
                SQL += "`TGL_DOCNO` DATETIME DEFAULT NULL,";
                SQL += "`NO_INV` VARCHAR(15) NOT NULL,";
                SQL += "`TGL_INV` DATE NOT NULL,";
                SQL += "`PLU` VARCHAR(8) NOT NULL DEFAULT '',";
                SQL += "`QTY` DECIMAL(12, 0) DEFAULT NULL,";
                SQL += "PRIMARY KEY(`TOKO`,`DOCNO`,`NO_INV`,`TGL_INV`,`PLU`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create struktur TransDC
                /*
                SQL += "CREATE TABLE IF NOT EXISTS `TRANSDC" + PERIODE + "` (";
                SQL += "`SHOP` varchar(4) NOT NULL DEFAULT '',";
                SQL += "`PRDCD` varchar(8) NOT NULL DEFAULT '',";
                SQL += "`PB1` int(10) DEFAULT '0',";
                SQL += "`NPB1` int(10) DEFAULT '0',";
                SQL += "`STDC1` int(10) DEFAULT '0',";
                SQL += "`PB2` int(10) DEFAULT '0',";
                SQL += "`NPB2` int(10) DEFAULT '0',";
                SQL += "`STDC2` int(10) DEFAULT '0',";
                SQL += "`PB3` int(10) DEFAULT '0',";
                SQL += "`NPB3` int(10) DEFAULT '0',";
                SQL += "`STDC3` int(10) DEFAULT '0',";
                SQL += "`PB4` int(10) DEFAULT '0',";
                SQL += "`NPB4` int(10) DEFAULT '0',";
                SQL += "`STDC4` int(10) DEFAULT '0',";
                SQL += "`PB5` int(10) DEFAULT '0',";
                SQL += "`NPB5` int(10) DEFAULT '0',";
                SQL += "`STDC5` int(10) DEFAULT '0',";
                SQL += "`PB6` int(10) DEFAULT '0',";
                SQL += "`NPB6` int(10) DEFAULT '0',";
                SQL += "`STDC6` int(10) DEFAULT '0',";
                SQL += "`PB7` int(10) DEFAULT '0',";
                SQL += "`NPB7` int(10) DEFAULT '0',";
                SQL += "`STDC7` int(10) DEFAULT '0',";
                SQL += "`PB8` int(10) DEFAULT '0',";
                SQL += "`NPB8` int(10) DEFAULT '0',";
                SQL += "`STDC8` int(10) DEFAULT '0',";
                SQL += "`PB9` int(10) DEFAULT '0',";
                SQL += "`NPB9` int(10) DEFAULT '0',";
                SQL += "`STDC9` int(10) DEFAULT '0',";
                SQL += "`PB10` int(10) DEFAULT '0',";
                SQL += "`NPB10` int(10) DEFAULT '0',";
                SQL += "`STDC10` int(10) DEFAULT '0',";
                SQL += "`PB11` int(10) DEFAULT '0',";
                SQL += "`NPB11` int(10) DEFAULT '0',";
                SQL += "`STDC11` int(10) DEFAULT '0',";
                SQL += "`PB12` int(10) DEFAULT '0',";
                SQL += "`NPB12` int(10) DEFAULT '0',";
                SQL += "`STDC12` int(10) DEFAULT '0',";
                SQL += "`PB13` int(10) DEFAULT '0',";
                SQL += "`NPB13` int(10) DEFAULT '0',";
                SQL += "`STDC13` int(10) DEFAULT '0',";
                SQL += "`PB14` int(10) DEFAULT '0',";
                SQL += "`NPB14` int(10) DEFAULT '0',";
                SQL += "`STDC14` int(10) DEFAULT '0',";
                SQL += "`PB15` int(10) DEFAULT '0',";
                SQL += "`NPB15` int(10) DEFAULT '0',";
                SQL += "`STDC15` int(10) DEFAULT '0',";
                SQL += "`PB16` int(10) DEFAULT '0',";
                SQL += "`NPB16` int(10) DEFAULT '0',";
                SQL += "`STDC16` int(10) DEFAULT '0',";
                SQL += "`PB17` int(10) DEFAULT '0',";
                SQL += "`NPB17` int(10) DEFAULT '0',";
                SQL += "`STDC17` int(10) DEFAULT '0',";
                SQL += "`PB18` int(10) DEFAULT '0',";
                SQL += "`NPB18` int(10) DEFAULT '0',";
                SQL += "`STDC18` int(10) DEFAULT '0',";
                SQL += "`PB19` int(10) DEFAULT '0',";
                SQL += "`NPB19` int(10) DEFAULT '0',";
                SQL += "`STDC19` int(10) DEFAULT '0',";
                SQL += "`PB20` int(10) DEFAULT '0',";
                SQL += "`NPB20` int(10) DEFAULT '0',";
                SQL += "`STDC20` int(10) DEFAULT '0',";
                SQL += "`PB21` int(10) DEFAULT '0',";
                SQL += "`NPB21` int(10) DEFAULT '0',";
                SQL += "`STDC21` int(10) DEFAULT '0',";
                SQL += "`PB22` int(10) DEFAULT '0',";
                SQL += "`NPB22` int(10) DEFAULT '0',";
                SQL += "`STDC22` int(10) DEFAULT '0',";
                SQL += "`PB23` int(10) DEFAULT '0',";
                SQL += "`NPB23` int(10) DEFAULT '0',";
                SQL += "`STDC23` int(10) DEFAULT '0',";
                SQL += "`PB24` int(10) DEFAULT '0',";
                SQL += "`NPB24` int(10) DEFAULT '0',";
                SQL += "`STDC24` int(10) DEFAULT '0',";
                SQL += "`PB25` int(10) DEFAULT '0',";
                SQL += "`NPB25` int(10) DEFAULT '0',";
                SQL += "`STDC25` int(10) DEFAULT '0',";
                SQL += "`PB26` int(10) DEFAULT '0',";
                SQL += "`NPB26` int(10) DEFAULT '0',";
                SQL += "`STDC26` int(10) DEFAULT '0',";
                SQL += "`PB27` int(10) DEFAULT '0',";
                SQL += "`NPB27` int(10) DEFAULT '0',";
                SQL += "`STDC27` int(10) DEFAULT '0',";
                SQL += "`PB28` int(10) DEFAULT '0',";
                SQL += "`NPB28` int(10) DEFAULT '0',";
                SQL += "`STDC28` int(10) DEFAULT '0',";
                SQL += "`PB29` int(10) DEFAULT '0',";
                SQL += "`NPB29` int(10) DEFAULT '0',";
                SQL += "`STDC29` int(10) DEFAULT '0',";
                SQL += "`PB30` int(10) DEFAULT '0',";
                SQL += "`NPB30` int(10) DEFAULT '0',";
                SQL += "`STDC30` int(10) DEFAULT '0',";
                SQL += "`PB31` int(10) DEFAULT '0',";
                SQL += "`NPB31` int(10) DEFAULT '0',";
                SQL += "`STDC31` int(10) DEFAULT '0',";
                SQL += "PRIMARY KEY(`SHOP`,`PRDCD`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                */
                #endregion
                #region Create struktur HIS PB
                SQL += "CREATE TABLE IF NOT EXISTS `HISPB" + PERIODE + "` (";
                SQL += "`SHOP` varchar(4) NOT NULL DEFAULT '',";
                SQL += "`PRDCD` varchar(8) NOT NULL DEFAULT '',";
                SQL += "`SINGKAT` varchar(50) DEFAULT '',";
                SQL += "`QTY1` int(10) DEFAULT '0',";
                SQL += "`PB1` int(10) DEFAULT '0',";
                SQL += "`NPB1` int(10) DEFAULT '0',";
                SQL += "`STDC1` int(10) DEFAULT '0',";
                SQL += "`PKM1` int(10) DEFAULT '0',";
                SQL += "`MINOR1` int(10) DEFAULT '0',";
                SQL += "`QTY2` int(10) DEFAULT '0',";
                SQL += "`PB2` int(10) DEFAULT '0',";
                SQL += "`NPB2` int(10) DEFAULT '0',";
                SQL += "`STDC2` int(10) DEFAULT '0',";
                SQL += "`PKM2` int(10) DEFAULT '0',";
                SQL += "`MINOR2` int(10) DEFAULT '0',";
                SQL += "`QTY3` int(10) DEFAULT '0',";
                SQL += "`PB3` int(10) DEFAULT '0',";
                SQL += "`NPB3` int(10) DEFAULT '0',";
                SQL += "`STDC3` int(10) DEFAULT '0',";
                SQL += "`PKM3` int(10) DEFAULT '0',";
                SQL += "`MINOR3` int(10) DEFAULT '0',";
                SQL += "`QTY4` int(10) DEFAULT '0',";
                SQL += "`PB4` int(10) DEFAULT '0',";
                SQL += "`NPB4` int(10) DEFAULT '0',";
                SQL += "`STDC4` int(10) DEFAULT '0',";
                SQL += "`PKM4` int(10) DEFAULT '0',";
                SQL += "`MINOR4` int(10) DEFAULT '0',";
                SQL += "`QTY5` int(10) DEFAULT '0',";
                SQL += "`PB5` int(10) DEFAULT '0',";
                SQL += "`NPB5` int(10) DEFAULT '0',";
                SQL += "`STDC5` int(10) DEFAULT '0',";
                SQL += "`PKM5` int(10) DEFAULT '0',";
                SQL += "`MINOR5` int(10) DEFAULT '0',";
                SQL += "`QTY6` int(10) DEFAULT '0',";
                SQL += "`PB6` int(10) DEFAULT '0',";
                SQL += "`NPB6` int(10) DEFAULT '0',";
                SQL += "`STDC6` int(10) DEFAULT '0',";
                SQL += "`PKM6` int(10) DEFAULT '0',";
                SQL += "`MINOR6` int(10) DEFAULT '0',";
                SQL += "`QTY7` int(10) DEFAULT '0',";
                SQL += "`PB7` int(10) DEFAULT '0',";
                SQL += "`NPB7` int(10) DEFAULT '0',";
                SQL += "`STDC7` int(10) DEFAULT '0',";
                SQL += "`PKM7` int(10) DEFAULT '0',";
                SQL += "`MINOR7` int(10) DEFAULT '0',";
                SQL += "`QTY8` int(10) DEFAULT '0',";
                SQL += "`PB8` int(10) DEFAULT '0',";
                SQL += "`NPB8` int(10) DEFAULT '0',";
                SQL += "`STDC8` int(10) DEFAULT '0',";
                SQL += "`PKM8` int(10) DEFAULT '0',";
                SQL += "`MINOR8` int(10) DEFAULT '0',";
                SQL += "`QTY9` int(10) DEFAULT '0',";
                SQL += "`PB9` int(10) DEFAULT '0',";
                SQL += "`NPB9` int(10) DEFAULT '0',";
                SQL += "`STDC9` int(10) DEFAULT '0',";
                SQL += "`PKM9` int(10) DEFAULT '0',";
                SQL += "`MINOR9` int(10) DEFAULT '0',";
                SQL += "`QTY10` int(10) DEFAULT '0',";
                SQL += "`PB10` int(10) DEFAULT '0',";
                SQL += "`NPB10` int(10) DEFAULT '0',";
                SQL += "`STDC10` int(10) DEFAULT '0',";
                SQL += "`PKM10` int(10) DEFAULT '0',";
                SQL += "`MINOR10` int(10) DEFAULT '0',";
                SQL += "`QTY11` int(10) DEFAULT '0',";
                SQL += "`PB11` int(10) DEFAULT '0',";
                SQL += "`NPB11` int(10) DEFAULT '0',";
                SQL += "`STDC11` int(10) DEFAULT '0',";
                SQL += "`PKM11` int(10) DEFAULT '0',";
                SQL += "`MINOR11` int(10) DEFAULT '0',";
                SQL += "`QTY12` int(10) DEFAULT '0',";
                SQL += "`PB12` int(10) DEFAULT '0',";
                SQL += "`NPB12` int(10) DEFAULT '0',";
                SQL += "`STDC12` int(10) DEFAULT '0',";
                SQL += "`PKM12` int(10) DEFAULT '0',";
                SQL += "`MINOR12` int(10) DEFAULT '0',";
                SQL += "`QTY13` int(10) DEFAULT '0',";
                SQL += "`PB13` int(10) DEFAULT '0',";
                SQL += "`NPB13` int(10) DEFAULT '0',";
                SQL += "`STDC13` int(10) DEFAULT '0',";
                SQL += "`PKM13` int(10) DEFAULT '0',";
                SQL += "`MINOR13` int(10) DEFAULT '0',";
                SQL += "`QTY14` int(10) DEFAULT '0',";
                SQL += "`PB14` int(10) DEFAULT '0',";
                SQL += "`NPB14` int(10) DEFAULT '0',";
                SQL += "`STDC14` int(10) DEFAULT '0',";
                SQL += "`PKM14` int(10) DEFAULT '0',";
                SQL += "`MINOR14` int(10) DEFAULT '0',";
                SQL += "`QTY15` int(10) DEFAULT '0',";
                SQL += "`PB15` int(10) DEFAULT '0',";
                SQL += "`NPB15` int(10) DEFAULT '0',";
                SQL += "`STDC15` int(10) DEFAULT '0',";
                SQL += "`PKM15` int(10) DEFAULT '0',";
                SQL += "`MINOR15` int(10) DEFAULT '0',";
                SQL += "`QTY16` int(10) DEFAULT '0',";
                SQL += "`PB16` int(10) DEFAULT '0',";
                SQL += "`NPB16` int(10) DEFAULT '0',";
                SQL += "`STDC16` int(10) DEFAULT '0',";
                SQL += "`PKM16` int(10) DEFAULT '0',";
                SQL += "`MINOR16` int(10) DEFAULT '0',";
                SQL += "`QTY17` int(10) DEFAULT '0',";
                SQL += "`PB17` int(10) DEFAULT '0',";
                SQL += "`NPB17` int(10) DEFAULT '0',";
                SQL += "`STDC17` int(10) DEFAULT '0',";
                SQL += "`PKM17` int(10) DEFAULT '0',";
                SQL += "`MINOR17` int(10) DEFAULT '0',";
                SQL += "`QTY18` int(10) DEFAULT '0',";
                SQL += "`PB18` int(10) DEFAULT '0',";
                SQL += "`NPB18` int(10) DEFAULT '0',";
                SQL += "`STDC18` int(10) DEFAULT '0',";
                SQL += "`PKM18` int(10) DEFAULT '0',";
                SQL += "`MINOR18` int(10) DEFAULT '0',";
                SQL += "`QTY19` int(10) DEFAULT '0',";
                SQL += "`PB19` int(10) DEFAULT '0',";
                SQL += "`NPB19` int(10) DEFAULT '0',";
                SQL += "`STDC19` int(10) DEFAULT '0',";
                SQL += "`PKM19` int(10) DEFAULT '0',";
                SQL += "`MINOR19` int(10) DEFAULT '0',";
                SQL += "`QTY20` int(10) DEFAULT '0',";
                SQL += "`PB20` int(10) DEFAULT '0',";
                SQL += "`NPB20` int(10) DEFAULT '0',";
                SQL += "`STDC20` int(10) DEFAULT '0',";
                SQL += "`PKM20` int(10) DEFAULT '0',";
                SQL += "`MINOR20` int(10) DEFAULT '0',";
                SQL += "`QTY21` int(10) DEFAULT '0',";
                SQL += "`PB21` int(10) DEFAULT '0',";
                SQL += "`NPB21` int(10) DEFAULT '0',";
                SQL += "`STDC21` int(10) DEFAULT '0',";
                SQL += "`PKM21` int(10) DEFAULT '0',";
                SQL += "`MINOR21` int(10) DEFAULT '0',";
                SQL += "`QTY22` int(10) DEFAULT '0',";
                SQL += "`PB22` int(10) DEFAULT '0',";
                SQL += "`NPB22` int(10) DEFAULT '0',";
                SQL += "`STDC22` int(10) DEFAULT '0',";
                SQL += "`PKM22` int(10) DEFAULT '0',";
                SQL += "`MINOR22` int(10) DEFAULT '0',";
                SQL += "`QTY23` int(10) DEFAULT '0',";
                SQL += "`PB23` int(10) DEFAULT '0',";
                SQL += "`NPB23` int(10) DEFAULT '0',";
                SQL += "`STDC23` int(10) DEFAULT '0',";
                SQL += "`PKM23` int(10) DEFAULT '0',";
                SQL += "`MINOR23` int(10) DEFAULT '0',";
                SQL += "`QTY24` int(10) DEFAULT '0',";
                SQL += "`PB24` int(10) DEFAULT '0',";
                SQL += "`NPB24` int(10) DEFAULT '0',";
                SQL += "`STDC24` int(10) DEFAULT '0',";
                SQL += "`PKM24` int(10) DEFAULT '0',";
                SQL += "`MINOR24` int(10) DEFAULT '0',";
                SQL += "`QTY25` int(10) DEFAULT '0',";
                SQL += "`PB25` int(10) DEFAULT '0',";
                SQL += "`NPB25` int(10) DEFAULT '0',";
                SQL += "`STDC25` int(10) DEFAULT '0',";
                SQL += "`PKM25` int(10) DEFAULT '0',";
                SQL += "`MINOR25` int(10) DEFAULT '0',";
                SQL += "`QTY26` int(10) DEFAULT '0',";
                SQL += "`PB26` int(10) DEFAULT '0',";
                SQL += "`NPB26` int(10) DEFAULT '0',";
                SQL += "`STDC26` int(10) DEFAULT '0',";
                SQL += "`PKM26` int(10) DEFAULT '0',";
                SQL += "`MINOR26` int(10) DEFAULT '0',";
                SQL += "`QTY27` int(10) DEFAULT '0',";
                SQL += "`PB27` int(10) DEFAULT '0',";
                SQL += "`NPB27` int(10) DEFAULT '0',";
                SQL += "`STDC27` int(10) DEFAULT '0',";
                SQL += "`PKM27` int(10) DEFAULT '0',";
                SQL += "`MINOR27` int(10) DEFAULT '0',";
                SQL += "`QTY28` int(10) DEFAULT '0',";
                SQL += "`PB28` int(10) DEFAULT '0',";
                SQL += "`NPB28` int(10) DEFAULT '0',";
                SQL += "`STDC28` int(10) DEFAULT '0',";
                SQL += "`PKM28` int(10) DEFAULT '0',";
                SQL += "`MINOR28` int(10) DEFAULT '0',";
                SQL += "`QTY29` int(10) DEFAULT '0',";
                SQL += "`PB29` int(10) DEFAULT '0',";
                SQL += "`NPB29` int(10) DEFAULT '0',";
                SQL += "`STDC29` int(10) DEFAULT '0',";
                SQL += "`PKM29` int(10) DEFAULT '0',";
                SQL += "`MINOR29` int(10) DEFAULT '0',";
                SQL += "`QTY30` int(10) DEFAULT '0',";
                SQL += "`PB30` int(10) DEFAULT '0',";
                SQL += "`NPB30` int(10) DEFAULT '0',";
                SQL += "`STDC30` int(10) DEFAULT '0',";
                SQL += "`PKM30` int(10) DEFAULT '0',";
                SQL += "`MINOR30` int(10) DEFAULT '0',";
                SQL += "`QTY31` int(10) DEFAULT '0',";
                SQL += "`PB31` int(10) DEFAULT '0',";
                SQL += "`NPB31` int(10) DEFAULT '0',";
                SQL += "`STDC31` int(10) DEFAULT '0',";
                SQL += "`PKM31` int(10) DEFAULT '0',";
                SQL += "`MINOR31` int(10) DEFAULT '0',";
                SQL += "PRIMARY KEY (`SHOP`,`PRDCD`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Tabel Hispbdoc
                SQL += "CREATE TABLE IF NOT EXISTS `HISPBDOC" + PERIODE + "` (";
                SQL += "`SHOP` varchar(4) NOT NULL DEFAULT '',";
                SQL += "`TANGGAL` date NOT NULL,";
                SQL += "`DOCNO` varchar(8) NOT NULL DEFAULT '',";
                SQL += "`PROSES` varchar(1) NOT NULL DEFAULT '',";
                SQL += "PRIMARY KEY (`SHOP`,`TANGGAL`,`DOCNO`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create struktur STMAST DC
                SQL+= "CREATE TABLE IF NOT EXISTS `STMASTDC" + PERIODE + "` (";
                SQL += "`LOKASI` varchar(2) DEFAULT '',";
                SQL += "`DIV` varchar(2) DEFAULT '',";
                SQL += "`PRDCD` varchar(8) NOT NULL DEFAULT '',";
                SQL += "`QTY1` int(10) DEFAULT '0',";
                SQL += "`MAX1` int(10) DEFAULT '0',";
                SQL += "`QTY2` int(10) DEFAULT '0',";
                SQL += "`MAX2` int(10) DEFAULT '0',";
                SQL += "`QTY3` int(10) DEFAULT '0',";
                SQL += "`MAX3` int(10) DEFAULT '0',";
                SQL += "`QTY4` int(10) DEFAULT '0',";
                SQL += "`MAX4` int(10) DEFAULT '0',";
                SQL += "`QTY5` int(10) DEFAULT '0',";
                SQL += "`MAX5` int(10) DEFAULT '0',";
                SQL += "`QTY6` int(10) DEFAULT '0',";
                SQL += "`MAX6` int(10) DEFAULT '0',";
                SQL += "`QTY7` int(10) DEFAULT '0',";
                SQL += "`MAX7` int(10) DEFAULT '0',";
                SQL += "`QTY8` int(10) DEFAULT '0',";
                SQL += "`MAX8` int(10) DEFAULT '0',";
                SQL += "`QTY9` int(10) DEFAULT '0',";
                SQL += "`MAX9` int(10) DEFAULT '0',";
                SQL += "`QTY10` int(10) DEFAULT '0',";
                SQL += "`MAX10` int(10) DEFAULT '0',";
                SQL += "`QTY11` int(10) DEFAULT '0',";
                SQL += "`MAX11` int(10) DEFAULT '0',";
                SQL += "`QTY12` int(10) DEFAULT '0',";
                SQL += "`MAX12` int(10) DEFAULT '0',";
                SQL += "`QTY13` int(10) DEFAULT '0',";
                SQL += "`MAX13` int(10) DEFAULT '0',";
                SQL += "`QTY14` int(10) DEFAULT '0',";
                SQL += "`MAX14` int(10) DEFAULT '0',";
                SQL += "`QTY15` int(10) DEFAULT '0',";
                SQL += "`MAX15` int(10) DEFAULT '0',";
                SQL += "`QTY16` int(10) DEFAULT '0',";
                SQL += "`MAX16` int(10) DEFAULT '0',";
                SQL += "`QTY17` int(10) DEFAULT '0',";
                SQL += "`MAX17` int(10) DEFAULT '0',";
                SQL += "`QTY18` int(10) DEFAULT '0',";
                SQL += "`MAX18` int(10) DEFAULT '0',";
                SQL += "`QTY19` int(10) DEFAULT '0',";
                SQL += "`MAX19` int(10) DEFAULT '0',";
                SQL += "`QTY20` int(10) DEFAULT '0',";
                SQL += "`MAX20` int(10) DEFAULT '0',";
                SQL += "`QTY21` int(10) DEFAULT '0',";
                SQL += "`MAX21` int(10) DEFAULT '0',";
                SQL += "`QTY22` int(10) DEFAULT '0',";
                SQL += "`MAX22` int(10) DEFAULT '0',";
                SQL += "`QTY23` int(10) DEFAULT '0',";
                SQL += "`MAX23` int(10) DEFAULT '0',";
                SQL += "`QTY24` int(10) DEFAULT '0',";
                SQL += "`MAX24` int(10) DEFAULT '0',";
                SQL += "`QTY25` int(10) DEFAULT '0',";
                SQL += "`MAX25` int(10) DEFAULT '0',";
                SQL += "`QTY26` int(10) DEFAULT '0',";
                SQL += "`MAX26` int(10) DEFAULT '0',";
                SQL += "`QTY27` int(10) DEFAULT '0',";
                SQL += "`MAX27` int(10) DEFAULT '0',";
                SQL += "`QTY28` int(10) DEFAULT '0',";
                SQL += "`MAX28` int(10) DEFAULT '0',";
                SQL += "`QTY29` int(10) DEFAULT '0',";
                SQL += "`MAX29` int(10) DEFAULT '0',";
                SQL += "`QTY30` int(10) DEFAULT '0',";
                SQL += "`MAX30` int(10) DEFAULT '0',";
                SQL += "`QTY31` int(10) DEFAULT '0',";
                SQL += "`MAX31` int(10) DEFAULT '0',";
                SQL += "`LCOST` decimal(12, 3) DEFAULT '0.000',";
                SQL += "PRIMARY KEY(`PRDCD`)";
                SQL += ") ENGINE = " + ENGINE + " ;";
                #endregion
                #region Buat Tmp PRD DC
                SQL+= "CREATE TABLE IF NOT EXISTS `prodmastdc" + PERIODE + "_temp` (";
                SQL += "`RECID` varchar(100) DEFAULT NULL,";
                SQL += "`WARE_HOUSE` varchar(100) DEFAULT NULL,";
                SQL += "`CAT_COD` varchar(100) DEFAULT NULL,";
                SQL += "`PRDCD` varchar(100) DEFAULT NULL,";
                SQL += "`DESC` varchar(100) DEFAULT NULL,";
                SQL += "`SINGKAT` varchar(100) DEFAULT NULL,";
                SQL += "`MERK` varchar(100) DEFAULT NULL,";
                SQL += "`NAMA` varchar(100) DEFAULT NULL,";
                SQL += "`FLAVOUR` varchar(100) DEFAULT NULL,";
                SQL += "`KEMASAN` varchar(100) DEFAULT NULL,";
                SQL += "`SIZE` varchar(100) DEFAULT NULL,";
                SQL += "`BKP` varchar(100) DEFAULT NULL,";
                SQL += "`DESC2` varchar(100) DEFAULT NULL,";
                SQL += "`FRAC` varchar(100) DEFAULT NULL,";
                SQL += "`UNIT` varchar(100) DEFAULT NULL,";
                SQL += "`FRAC_CTN` varchar(100) DEFAULT NULL,";
                SQL += "`UNIT_CTN` varchar(100) DEFAULT NULL,";
                SQL += "`ACOST` varchar(100) DEFAULT NULL,";
                SQL += "`RCOST` varchar(100) DEFAULT NULL,";
                SQL += "`LCOST` varchar(100) DEFAULT NULL,";
                SQL += "`MARKUP1` varchar(100) DEFAULT NULL,";
                SQL += "`MARKUP2` varchar(100) DEFAULT NULL,";
                SQL += "`MARKUP3` varchar(100) DEFAULT NULL,";
                SQL += "`MARKUP4` varchar(100) DEFAULT NULL,";
                SQL += "`MARKUP5` varchar(100) DEFAULT NULL,";
                SQL += "`MARKUP6` varchar(100) DEFAULT NULL,";
                SQL += "`PRICE_A` varchar(100) DEFAULT NULL,";
                SQL += "`PRICE_B` varchar(100) DEFAULT NULL,";
                SQL += "`PRICE_C` varchar(100) DEFAULT NULL,";
                SQL += "`PRICE_D` varchar(100) DEFAULT NULL,";
                SQL += "`PRICE_E` varchar(100) DEFAULT NULL,";
                SQL += "`PRICE_F` varchar(100) DEFAULT NULL,";
                SQL += "`DIV` varchar(100) DEFAULT NULL,";
                SQL += "`PRDGRP` varchar(100) DEFAULT NULL,";
                SQL += "`CTGR` varchar(100) DEFAULT NULL,";
                SQL += "`KONS` varchar(100) DEFAULT NULL,";
                SQL += "`SUPCO` varchar(100) DEFAULT NULL,";
                SQL += "`SUPCO_1` varchar(100) DEFAULT NULL,";
                SQL += "`PTAG` varchar(100) DEFAULT NULL,";
                SQL += "`TGL_TAMBAH` varchar(100) DEFAULT NULL,";
                SQL += "`TGL_RUBAH` varchar(100) DEFAULT NULL,";
                SQL += "`TGL_HARGA1` varchar(100) DEFAULT NULL,";
                SQL += "`TGL_HARGA2` varchar(100) DEFAULT NULL,";
                SQL += "`TGL_HARGA3` varchar(100) DEFAULT NULL,";
                SQL += "`TGL_HARGA4` varchar(100) DEFAULT NULL,";
                SQL += "`TGL_HARGA5` varchar(100) DEFAULT NULL,";
                SQL += "`TGL_HARGA6` varchar(100) DEFAULT NULL,";
                SQL += "`KEL_ORD` varchar(100) DEFAULT NULL,";
                SQL += "`REORDER` varchar(100) DEFAULT NULL,";
                SQL += "`LENGTH` varchar(100) DEFAULT NULL,";
                SQL += "`WIDTH` varchar(100) DEFAULT NULL,";
                SQL += "`HEIGHT` varchar(100) DEFAULT NULL,";
                SQL += "`K_LENGTH` varchar(100) DEFAULT NULL,";
                SQL += "`K_WIDTH` varchar(100) DEFAULT NULL,";
                SQL += "`K_HEIGHT` varchar(100) DEFAULT NULL,";
                SQL += "`BERAT_SAT` varchar(100) DEFAULT NULL,";
                SQL += "`BERAT_KRT` varchar(100) DEFAULT NULL,";
                SQL += "`EXP_MONTH` varchar(100) DEFAULT NULL,";
                SQL += "`EXP_DAY` varchar(100) DEFAULT NULL,";
                SQL += "`UPDATE` varchar(100) DEFAULT NULL,";
                SQL += "`BRCP1` varchar(100) DEFAULT NULL,";
                SQL += "`BRCPST1` varchar(100) DEFAULT NULL,";
                SQL += "`BRCP2` varchar(100) DEFAULT NULL,";
                SQL += "`BRCPST2` varchar(100) DEFAULT NULL,";
                SQL += "`BRCD1` varchar(100) DEFAULT NULL,";
                SQL += "`BRCDST1` varchar(100) DEFAULT NULL,";
                SQL += "`BRCD2` varchar(100) DEFAULT NULL,";
                SQL += "`BRCDST2` varchar(100) DEFAULT NULL,";
                SQL += "`BRCK1` varchar(100) DEFAULT NULL,";
                SQL += "`BRCKST1` varchar(100) DEFAULT NULL,";
                SQL += "`BRCK2` varchar(100) DEFAULT NULL,";
                SQL += "`BRCKST2` varchar(100) DEFAULT NULL,";
                SQL += "`BRG_AKTIF` varchar(100) DEFAULT NULL,";
                SQL += "`ST_BKP` varchar(100) DEFAULT NULL,";
                SQL += "`FRAC_BOX` varchar(100) DEFAULT NULL,";
                SQL += "`UNIT_BOX` varchar(100) DEFAULT NULL,";
                SQL += "`RCVEXPB` varchar(100) DEFAULT NULL,";
                SQL += "`RTREXPB` varchar(100) DEFAULT NULL,";
                SQL += "`PLU4` varchar(100) DEFAULT NULL";
                SQL += ")ENGINE = " + ENGINE + " ;";
                #endregion
                #region Create Struktur Trend STO
                SQL += "CREATE TABLE IF NOT EXISTS `trendstockout" + PERIODE + "` (";
                SQL += "`SHOP` varchar(4) NOT NULL DEFAULT '',";
                SQL += "`NAMA` varchar(60) DEFAULT '',";
                SQL += "`TG1` int(5) DEFAULT '0',";
                SQL += "`TG2` int(5) DEFAULT '0',";
                SQL += "`TG3` int(5) DEFAULT '0',";
                SQL += "`TG4` int(5) DEFAULT '0',";
                SQL += "`TG5` int(5) DEFAULT '0',";
                SQL += "`TG6` int(5) DEFAULT '0',";
                SQL += "`TG7` int(5) DEFAULT '0',";
                SQL += "`TG8` int(5) DEFAULT '0',";
                SQL += "`TG9` int(5) DEFAULT '0',";
                SQL += "`TG10` int(5) DEFAULT '0',";
                SQL += "`TG11` int(5) DEFAULT '0',";
                SQL += "`TG12` int(5) DEFAULT '0',";
                SQL += "`TG13` int(5) DEFAULT '0',";
                SQL += "`TG14` int(5) DEFAULT '0',";
                SQL += "`TG15` int(5) DEFAULT '0',";
                SQL += "`TG16` int(5) DEFAULT '0',";
                SQL += "`TG17` int(5) DEFAULT '0',";
                SQL += "`TG18` int(5) DEFAULT '0',";
                SQL += "`TG19` int(5) DEFAULT '0',";
                SQL += "`TG20` int(5) DEFAULT '0',";
                SQL += "`TG21` int(5) DEFAULT '0',";
                SQL += "`TG22` int(5) DEFAULT '0',";
                SQL += "`TG23` int(5) DEFAULT '0',";
                SQL += "`TG24` int(5) DEFAULT '0',";
                SQL += "`TG25` int(5) DEFAULT '0',";
                SQL += "`TG26` int(5) DEFAULT '0',";
                SQL += "`TG27` int(5) DEFAULT '0',";
                SQL += "`TG28` int(5) DEFAULT '0',";
                SQL += "`TG29` int(5) DEFAULT '0',";
                SQL += "`TG30` int(5) DEFAULT '0',";
                SQL += "`TG31` int(5) DEFAULT '0',";
                SQL += "`TOTAL` double NOT NULL DEFAULT '0',";
                SQL += "PRIMARY KEY(`SHOP`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                #region Create Struktur CRDH
                SQL += "CREATE TABLE IF NOT EXISTS `CRDH" + PERIODE + "` (";
                SQL += "`RECID` varchar(1) DEFAULT NULL,";
                SQL += "`STATION` varchar(2) NOT NULL DEFAULT '',";
                SQL += "`SHIFT` varchar(2) NOT NULL DEFAULT '',";
                SQL += "`RTYPE` varchar(1) NOT NULL DEFAULT '',";
                SQL += "`DOCNO` varchar(15) NOT NULL DEFAULT '',";
                SQL += "`SEQNO` varchar(10) NOT NULL DEFAULT '',";
                SQL += "`DIV` varchar(2) DEFAULT NULL,";
                SQL += "`PRDCD` varchar(8) NOT NULL,";
                SQL += "`QTY` decimal(12, 0) NOT NULL DEFAULT '0',";
                SQL += "`PRICE` decimal(12, 6) DEFAULT NULL,";
                SQL += "`GROSS` decimal(12, 0) DEFAULT NULL,";
                SQL += "`DISCOUNT` decimal(12, 6) DEFAULT NULL,";
                SQL += "`SHOP` varchar(4) NOT NULL DEFAULT '',";
                SQL += "`TANGGAL` date NOT NULL,";
                SQL += "`JAM` time DEFAULT NULL,";
                SQL += "`KONS` varchar(1) DEFAULT NULL,";
                SQL += "`TTYPE` varchar(1) NOT NULL DEFAULT '',";
                SQL += "`HPP` decimal(12, 6) DEFAULT NULL,";
                SQL += "`PROMOSI` varchar(50) DEFAULT NULL,";
                SQL += "`PPN` decimal(12, 6) DEFAULT NULL,";
                SQL += "`PTAG` varchar(1) DEFAULT NULL,";
                SQL += "`CAT_COD` varchar(5) DEFAULT NULL,";
                SQL += "`NM_VCR` varchar(50) DEFAULT NULL,";
                SQL += "`NO_VCR` varchar(50) NOT NULL DEFAULT '',";
                SQL += "`BKP` varchar(1) DEFAULT NULL,";
                SQL += "`SUB_BKP` varchar(1) DEFAULT NULL,";
                SQL += "`KDBIN` varchar(50) NOT NULL DEFAULT '',";
                SQL += "`NOKARTU` varchar(50) DEFAULT NULL,";
                SQL += "PRIMARY KEY(`SHOP`,`TANGGAL`,`PRDCD`,`STATION`,`SHIFT`,`DOCNO`,`RTYPE`,`SEQNO`,`QTY`,`TTYPE`,`KDBIN`,`NO_VCR`)";
                SQL += ") ENGINE = " + ENGINE + "; ";
                #endregion
                #region DBT
                SQL += "CREATE TABLE IF NOT EXISTS `DBT" + PERIODE + "` (";
                SQL += "`RECID` varchar(1) DEFAULT '',";
                SQL += "`STATION` varchar(2) NOT NULL DEFAULT '',";
                SQL += "`SHIFT` varchar(1) NOT NULL DEFAULT '',";
                SQL += "`DOCNO` varchar(25) NOT NULL DEFAULT '',";
                SQL += "`TANGGAL` date NOT NULL,";
                SQL += "`TOKO` varchar(4) NOT NULL DEFAULT '',";
                SQL += "`SALESRP` decimal(12, 0) DEFAULT NULL,";
                SQL += "`KDBANK` varchar(10) DEFAULT '',";
                SQL += "`KD_BIN` varchar(10) DEFAULT '',";
                SQL += "`NMBANK` varchar(100) DEFAULT '',";
                SQL += "`NOMOR` varchar(100) DEFAULT '',";
                SQL += "`DEBIT` decimal(12, 0) DEFAULT NULL,";
                SQL += "`AMBILTN` decimal(12, 0) NOT NULL,";
                SQL += "`Type` varchar(100) DEFAULT '',";
                SQL += "`TRX_ID` varchar(100) DEFAULT '',";
                SQL += "`TRX_USER` varchar(100) DEFAULT '',";
                SQL += "`STS_KRM` varchar(100) DEFAULT '',";
                SQL += "`TP_TRAN` varchar(100) DEFAULT '',";
                SQL += "`TRANDATE` date DEFAULT NULL,";
                SQL += "`TRANTIME` varchar(100) DEFAULT '',";
                SQL += "`APPRO_CD` varchar(100) DEFAULT '',";
                SQL += "`TRC_NO` varchar(100) DEFAULT '',";
                SQL += "`TRM_ID` varchar(100) DEFAULT '',";
                SQL += "`MERCH_ID` varchar(100) DEFAULT '',";
                SQL += "`MEMBERID` varchar(100) DEFAULT '',";
                SQL += "`EXPIRYDT` varchar(100) DEFAULT '',";
                SQL += "`BATH_NO` varchar(100) DEFAULT '',";
                SQL += "`KASIR_NAME` varchar(100) DEFAULT '',";
                SQL += "`FEE` decimal(12, 6) DEFAULT NULL,";
                SQL += "`TIPE` varchar(100) DEFAULT '',";
                SQL += "PRIMARY KEY (`STATION`,`SHIFT`,`DOCNO`,`TANGGAL`,`TOKO`,`AMBILTN`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion

                #region CIN
                SQL += "CREATE TABLE IF NOT EXISTS `CIN" + PERIODE + "` (";
                SQL += "`TOKO` varchar(4) NOT NULL DEFAULT '',";
                SQL += "`SHIFT` varchar(1) NOT NULL DEFAULT '',";
                SQL += "`STATION` varchar(2) NOT NULL DEFAULT '',";
                SQL += "`DOCNO` varchar(20) NOT NULL DEFAULT '',";
                SQL += "`PRDCD` varchar(8) NOT NULL DEFAULT '',";
                SQL += "`QTY` decimal(12, 0) DEFAULT NULL,";
                SQL += "`CASHIN` decimal(12, 0) DEFAULT NULL,";
                SQL += "`FEE` decimal(12, 0) DEFAULT NULL,";
                SQL += "`NO_HP` varchar(100) DEFAULT '',";
                SQL += "`NO_TRXID` varchar(100) DEFAULT '',";
                SQL += "`TGLTRXID` date NOT NULL,";
                SQL += "`JAMTRXID` varchar(100) DEFAULT '',";
                SQL += "`KD_TRX` varchar(100) DEFAULT '',";
                SQL += "`DBT` varchar(100) DEFAULT '',";
                SQL += "`NO_DBT` varchar(100) DEFAULT '',";
                SQL += "`JENIS` varchar(100) DEFAULT '',";
                SQL += "`KET_JNS` varchar(100) DEFAULT '',";
                SQL += "`ID_PEL` varchar(100) DEFAULT '',";
                SQL += "`NO_TID` varchar(100) DEFAULT '',";
                SQL += "`NO_PAN` varchar(100) DEFAULT '',";
                SQL += "`NO_TRACE` varchar(100) DEFAULT '',";
                SQL += "`NO_APPRO` varchar(100) DEFAULT '',";
                SQL += "`NO_BATCH` varchar(100) DEFAULT '',";
                SQL += "PRIMARY KEY (`TOKO`,`SHIFT`,`STATION`,`DOCNO`,`PRDCD`,`TGLTRXID`)";
                SQL += ") ENGINE = " + ENGINE + ";";
                #endregion
                using (MySqlConnection mConnection = db)
                {
                    if (mConnection.State != ConnectionState.Open)
                    {
                        mConnection.Open();
                    }
                    using (MySqlTransaction trans = mConnection.BeginTransaction())
                    {
                        using (MySqlCommand myCmd = new MySqlCommand(SQL, mConnection, trans))
                        {
                            myCmd.CommandType = CommandType.Text;
                            myCmd.ExecuteNonQuery();
                            trans.Commit();
                        }
                    }
                }
                UpdateStuktur(PERIODE);
                return true;
            }
            catch (Exception err)
            {
                //Log(err.ToString());
                Log("CREATE STRUKTUR", "", err.Message);
                return false;
            }
        }

        #endregion

        #region UpdateStruktur

        public void UpdateStuktur(string PERIODE)
        {
            try
            {
                if (db.State != ConnectionState.Open)
                {
                    db.Open();
                }
                SQL = "SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'DTRAN" + PERIODE + "' AND TABLE_SCHEMA = '" + Database + "' AND COLUMN_NAME = 'NOACC';";
                MySqlCommand cmd = new MySqlCommand(SQL, db);
                if(cmd.ExecuteScalar().ToString() == "0")
                {
                    SQL = "ALTER TABLE `DTRAN" + PERIODE + "` ADD `NOACC` VARCHAR(255) DEFAULT '';";
                    cmd = new MySqlCommand(SQL, db);
                    cmd.ExecuteNonQuery();
                }
                SQL = "SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'SLP' AND TABLE_SCHEMA = '" + Database + "' AND COLUMN_NAME = 'STK_OUT_DRY';";
                cmd = new MySqlCommand(SQL, db);
                if (cmd.ExecuteScalar().ToString() == "0")
                {
                    SQL = "ALTER TABLE `SLP` ADD `STK_OUT_DRY` DOUBLE DEFAULT 0;";
                    cmd = new MySqlCommand(SQL, db);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {

            }
            

        }

        #endregion
        #region Read CSV To DataTable
        public DataTable GETCSVTABLE(string filename, string koma)
        {
            FileInfo file = new FileInfo(filename);
            DataTable tbl = new DataTable();
            try
            {
                if (File.Exists(file.DirectoryName + "//schema.ini"))
                {
                    File.Delete(file.DirectoryName + "//schema.ini");
                }
                using (StreamReader reader = new StreamReader(filename))
                {
                    FileStream fs = new FileStream(file.DirectoryName + "//schema.ini", FileMode.Append, FileAccess.Write);
                    StreamWriter sw = new StreamWriter(fs);
                    sw.WriteLine("[" + file.Name + "]");
                    sw.WriteLine("Format = Delimited(" + koma + ")");
                    if (file.Name.Substring(0, 2) != "TL")
                    {
                        char K = Char.Parse(koma);
                        string B = reader.ReadLine();
                        string[] A = B.Split(K);
                        //MessageBox.Show(B);
                        int D = 1;
                        foreach (string C in A)
                        {
                            sw.WriteLine("Col" + D.ToString() + "=" + C + " Char Width 100");
                            //MessageBox.Show(C);
                            D += 1;
                        }
                    }
                    else
                    {
                        char K = Char.Parse(koma);
                        string B = reader.ReadLine();
                        string[] A = B.Split(K);
                        //MessageBox.Show(B);
                        int D = 1;
                        foreach (string C in A)
                        {
                            sw.WriteLine("Col" + D.ToString() + "=" + C + " Char Width 100");
                            //MessageBox.Show(C);
                            D += 1;
                        }
                        sw.WriteLine("Col28 = A Char Width 100");
                        sw.WriteLine("Col29 = B Char Width 100");
                        sw.WriteLine("Col30 = C Char Width 100");
                        sw.WriteLine("Col31 = D Char Width 100");
                    }
                    sw.Close();
                }
                if (koma == ",")
                {
                    var fileContents = File.ReadAllText(filename);
                    fileContents = fileContents.Replace("'", "");
                    File.WriteAllText(filename, fileContents);
                }
                using (OleDbConnection con = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\"" + file.DirectoryName + "\";Extended Properties =\"Text;HDR=YES;\""))
                {
                    using (OleDbCommand cmd = new OleDbCommand(string.Format("SELECT * FROM [{0}]", file.Name), con))
                    {
                        con.Open();
                        using (OleDbDataAdapter adp = new OleDbDataAdapter(cmd))
                        {
                            adp.Fill(tbl);
                        }
                    }
                }
            }
            catch (Exception err)
            {
                //Log(err.ToString());
                Log("GET DATATABLE CSV", "", err.Message);
                //MessageBox.Show(ERR.Message);
            }
            return tbl;
        }
        #endregion
        #region Proses File

        public bool ProsesFile(string FILE, string TOKO, DateTime TANGGAL)
        {
            try
            {
                using (MySqlConnection mCon = db)
                {
                    using (MySqlCommand Cmd = mCon.CreateCommand())
                    {
                        string PRS_FILE = FILE + Periode + "_TEMP";
                        string TMP = DateTime.Now.ToString("yyyyMMddHHmmss");
                        string TG = "TG" + int.Parse(TANGGAL.ToString("dd")).ToString();
                        string TGL = int.Parse(TANGGAL.ToString("dd")).ToString();
                        string TGL1 = TANGGAL.ToString("yyyy-MM-dd");
                        switch (FILE)
                        {
                            case "DT":
                                string CATCOD = "'54004','54005','55401','55402','55403','55404','55405','55406','55407','55408','55409','55501','55502','55503','55504','55601','55701','55702','55703','55801','55802','55803','55804'";
                                SQL = "";
                                SQL += "DELETE FROM REKAPSLS_APKA" + Periode + " WHERE SHOP='" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                SQL += "DROP TABLE IF EXISTS STK" + TMP + ";";
                                SQL += "DELETE FROM REKAPSLS" + Periode + " WHERE SHOP='" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                SQL += "DELETE FROM PENJUALAN" + Periode + " WHERE SHOP='" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                SQL += "INSERT IGNORE INTO PENJUALAN" + Periode + "(SHOP,TANGGAL,RTYPE,`DIV`,CAT_COD,PRDCD,SLS_QTY,SLS_RP,SLS_DISC,SLS_HPP,SLS_BRS,STRUK,PPN) SELECT SHOP,STR_TO_DATE(TANGGAL,'%d-%m-%Y'),RTYPE,`DIV`,CAT_COD,PRDCD,SUM(QTY) AS QTY,SUM(GROSS) AS PRICE,DISCOUNT,SUM(HPP*QTY) AS HPP,SUM(GROSS-PPN) AS SALES_BERSIH,COUNT(DOCNO) AS STRUK,PPN FROM " + PRS_FILE + " GROUP BY SHOP,PRDCD,RTYPE ORDER BY PRDCD;";
                                SQL += "CREATE TEMPORARY TABLE STK" + TMP + " SELECT SHOP,TANGGAL,1 AS STRUK FROM " + PRS_FILE + " GROUP BY DOCNO,SHIFT,STATION;";
                                SQL += "INSERT IGNORE INTO REKAPSLS" + Periode + "(SHOP,NAMA,TANGGAL,SALESGROSS,SALESNET,SALESHPP,STRUK,UPD_DATE) SELECT SHOP,'" + GetToko(TOKO, "NAMA_TOKO") + "',STR_TO_DATE(TANGGAL,'%d-%m-%Y'),(SELECT SUM(IF(RTYPE='J',GROSS,0))-SUM(IF(RTYPE='D',GROSS,0)) FROM " + PRS_FILE + " WHERE CAT_COD NOT IN(" + CATCOD + ")) AS GROSS,(SELECT SUM(IF(RTYPE='J',GROSS-PPN,0))-SUM(IF(RTYPE='D',GROSS-PPN,0)) FROM " + PRS_FILE + " WHERE CAT_COD NOT IN(" + CATCOD + ")) AS NET,";
                                SQL += "(SELECT SUM(IF(RTYPE='J',HPP*QTY,0))-SUM(IF(RTYPE='D',HPP*QTY,0)) FROM " + PRS_FILE + " WHERE CAT_COD NOT IN(" + CATCOD + ")) AS HPP,(SELECT SUM(STRUK) FROM STK" + TMP + ") AS STRUK,NOW() FROM " + PRS_FILE + " GROUP BY SHOP;";
                                SQL += "DELETE FROM DTRAN" + Periode + " WHERE SHOP='" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                SQL += "INSERT IGNORE DTRAN" + Periode + " (RECID, STATION, SHIFT, RTYPE, DOCNO, SEQNO, `DIV`, PRDCD, QTY, PRICE, GROSS, DISCOUNT, SHOP, TANGGAL, JAM, KONS, TTYPE, HPP, PROMOSI, PPN, PTAG, CAT_COD, NM_VCR, NO_VCR, BKP, SUB_BKP, PLUMD, NOACC) ";
                                SQL += "SELECT RECID, STATION, SHIFT, RTYPE, DOCNO, SEQNO, `DIV`, PRDCD, QTY, PRICE, GROSS, DISCOUNT, SHOP, STR_TO_DATE(TANGGAL,'%d-%m-%Y'), JAM, KONS, TTYPE, HPP, PROMOSI, PPN, PTAG, CAT_COD, NM_VCR, NO_VCR, BKP, SUB_BKP, PLUMD,NOACC FROM " + PRS_FILE + ";";
                                #region Create Custab
                                //******************** CUSTAB GROSS ******************************
                                SQL += "DELETE FROM CUSTAB_GROSS_" + Periode + ";";
                                SQL += "INSERT IGNORE INTO CUSTAB_GROSS_" + Periode + "(TANGGAL,STRUK,SHOP,SHIFT,STATION,DOCNO,ITEM,GROSS,JAM) SELECT STR_TO_DATE(TANGGAL,'%d-%m-%Y'),1 AS STRUK,SHOP,SHIFT,STATION,DOCNO,COUNT(*) AS ITEM,SUM(IF(RTYPE='J' AND CAT_COD!='54005',GROSS-PPN,0))AS GROSS,SUBSTRING(JAM,1,2)AS JAM FROM " + PRS_FILE + " GROUP BY TANGGAL,SHOP,SHIFT,STATION,DOCNO;";
                                SQL += "UPDATE CUSTAB_GROSS_" + Periode + " SET JENIS = '<10,000', cJNS = '01', rowid = 'a' WHERE GROSS< 10000;";
                                SQL += "UPDATE CUSTAB_GROSS_" + Periode + " SET JENIS='10,000 -  20,000',cJNS='02',rowid='b' WHERE GROSS>=10000 AND GROSS<20000;";
                                SQL += "UPDATE CUSTAB_GROSS_" + Periode + " SET JENIS='20,000 -  30,000',cJNS='03',rowid='c' WHERE GROSS>=20000 AND GROSS<30000;";
                                SQL += "UPDATE CUSTAB_GROSS_" + Periode + " SET JENIS='30,000 -  40,000',cJNS='04',rowid='d' WHERE GROSS>=30000 AND GROSS<40000;";
                                SQL += "UPDATE CUSTAB_GROSS_" + Periode + " SET JENIS='40,000 -  50,000',cJNS='05',rowid='e' WHERE GROSS>=40000 AND GROSS<50000;";
                                SQL += "UPDATE CUSTAB_GROSS_" + Periode + " SET JENIS='50,000 -  60,000',cJNS='06',rowid='f' WHERE GROSS>=50000 AND GROSS<60000;";
                                SQL += "UPDATE CUSTAB_GROSS_" + Periode + " SET JENIS='60,000 -  70,000',cJNS='07',rowid='g' WHERE GROSS>=60000 AND GROSS<70000;";
                                SQL += "UPDATE CUSTAB_GROSS_" + Periode + " SET JENIS='70,000 -  80,000',cJNS='08',rowid='h' WHERE GROSS>=70000 AND GROSS<80000;";
                                SQL += "UPDATE CUSTAB_GROSS_" + Periode + " SET JENIS='80,000 -  90,000',cJNS='09',rowid='i'WHERE GROSS>=80000 AND GROSS<90000;";
                                SQL += "UPDATE CUSTAB_GROSS_" + Periode + " SET JENIS='90,000 -  100,000',cJNS='10',rowid='j'WHERE GROSS>=90000 AND GROSS<100000;";
                                SQL += "UPDATE CUSTAB_GROSS_" + Periode + " SET JENIS='100,000 -  150,000',cJNS='11',rowid='k' WHERE GROSS>=100000 AND GROSS<150000;";
                                SQL += "UPDATE CUSTAB_GROSS_" + Periode + " SET JENIS='150,000 -  200,000',cJNS='12',rowid='l'WHERE GROSS>=150000 AND GROSS<200000;";
                                SQL += "UPDATE CUSTAB_GROSS_" + Periode + " SET JENIS='200,000 -  250,000',cJNS='13',rowid='m'WHERE GROSS>=200000 AND GROSS<250000;";
                                SQL += "UPDATE CUSTAB_GROSS_" + Periode + " SET JENIS='250,000 -  300,000',cJNS='14',rowid='n'WHERE GROSS>=250000 AND GROSS<300000;";
                                SQL += "UPDATE CUSTAB_GROSS_" + Periode + " SET JENIS='>300,000',cJNS='15',rowid='o' WHERE GROSS>=300000;";
                                SQL += "DELETE FROM CUSTAB_GROSS" + Periode + " WHERE SHOP='" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                SQL += "INSERT INTO CUSTAB_GROSS" + Periode + " SELECT SHOP,TANGGAL,ITEM,SUM(STRUK) AS STRUK,SUM(GROSS)AS GROSS,SUBSTRING(JAM,1,2)AS JAM ,JENIS,ROWID FROM CUSTAB_GROSS_" + Periode + " GROUP BY JENIS,SHOP,TANGGAL,ROWID ORDER BY CJNS;";

                                //******************** CUSTAB ITEM ******************************
                                SQL += "DELETE FROM CUSTAB_IT_" + Periode + ";";
                                SQL += "INSERT INTO CUSTAB_IT_" + Periode + "(TANGGAL,SHOP,SHIFT,STATION,STRUK,ITEM,GROSS,JAM) SELECT STR_TO_DATE(TANGGAL,'%d-%m-%Y'),SHOP,SHIFT,STATION,1 AS STRUK,COUNT(PRDCD) AS ITEM,SUM(IF(RTYPE='J' AND CAT_COD!='54005',GROSS-PPN,0))AS GROSS,SUBSTRING(JAM,1,2)AS JAM FROM " + PRS_FILE + "  GROUP BY TANGGAL,SHOP,SHIFT,STATION,DOCNO;";
                                SQL += "UPDATE CUSTAB_IT_" + Periode + " SET JENIS='Item  =  1 ',cJNS='01',rowid='a' WHERE ITEM=1;";
                                SQL += "UPDATE CUSTAB_IT_" + Periode + " SET JENIS='Item 2 - 3 ',cJNS='02',rowid='b' WHERE ITEM>1 AND ITEM<4;";
                                SQL += "UPDATE CUSTAB_IT_" + Periode + " SET JENIS='Item 4 - 5 ',cJNS='03',rowid='c' WHERE ITEM>3 AND ITEM<6;";
                                SQL += "UPDATE CUSTAB_IT_" + Periode + " SET JENIS='Item 6 - 10',cJNS='04',rowid='d' WHERE ITEM>5 AND ITEM <11;";
                                SQL += "UPDATE CUSTAB_IT_" + Periode + " SET JENIS='Item 11 - 15',cJNS='05',rowid='e' WHERE ITEM >10 AND ITEM <16;";
                                SQL += "UPDATE CUSTAB_IT_" + Periode + " SET JENIS='Item  >  15',cJNS='06',rowid='f' WHERE ITEM>15;";
                                SQL += "DELETE FROM CUSTAB_ITEM" + Periode + " WHERE SHOP='" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                SQL += "INSERT INTO CUSTAB_ITEM" + Periode + " SELECT SHOP,TANGGAL,ITEM,SUM(STRUK) AS STRUK,SUM(GROSS)AS GROSS,SUBSTRING(JAM,1,2)AS JAM ,JENIS,ROWID FROM CUSTAB_IT_" + Periode + " GROUP BY JENIS,SHOP,TANGGAL,ROWID ORDER BY CJNS;";

                                //******************** CUSTAB JAM ******************************
                                SQL += "DELETE FROM CUSTAB_JAM_" + Periode + ";";
                                SQL += "INSERT INTO CUSTAB_JAM_" + Periode + "(TANGGAL,SHOP,SHIFT,STATION,STRUK,ITEM,GROSS,JAM) SELECT STR_TO_DATE(TANGGAL,'%d-%m-%Y'),SHOP,SHIFT,STATION,1 AS STRUK,COUNT(PRDCD) AS ITEM,SUM(IF(RTYPE='J' AND CAT_COD!='54005',GROSS-PPN,0))AS GROSS,SUBSTRING(JAM,1,2)AS JAM FROM " + PRS_FILE + "  GROUP BY TANGGAL,SHOP,SHIFT,STATION,DOCNO;";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='00.00 - 01.00',cJNS='01',rowid='a' WHERE JAM='00';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='01.00 - 02.00',cJNS='02',rowid='b' WHERE JAM='01';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='02.00 - 03.00',cJNS='03',rowid='c' WHERE JAM='02';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='03.00 - 04.00',cJNS='04',rowid='d' WHERE JAM='03';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='04.00 - 05.00',cJNS='05',rowid='e' WHERE JAM='04';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='05.00 - 06.00',cJNS='06',rowid='f' WHERE JAM='05';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='06.00 - 07.00',cJNS='07',rowid='g' WHERE JAM='06';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='07.00 - 08.00',cJNS='08',rowid='h' WHERE JAM='07';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='08.00 - 09.00',cJNS='09',rowid='i' WHERE JAM='08';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='09.00 - 10.00',cJNS='10',rowid='j' WHERE JAM='09';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='10.00 - 11.00',cJNS='11',rowid='k' WHERE JAM='10';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='11.00 - 12.00',cJNS='12',rowid='l' WHERE JAM='11';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='12.00 - 13.00',cJNS='13',rowid='m' WHERE JAM='12';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='13.00 - 14.00',cJNS='14',rowid='n' WHERE JAM='13';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='14.00 - 15.00',cJNS='15',rowid='o' WHERE JAM='14';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='15.00 - 16.00',cJNS='16',rowid='p' WHERE JAM='15';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='16.00 - 17.00',cJNS='17',rowid='q' WHERE JAM='16';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='17.00 - 18.00',cJNS='18',rowid='r' WHERE JAM='17';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='18.00 - 19.00',cJNS='19',rowid='s' WHERE JAM='18';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='19.00 - 20.00',cJNS='20',rowid='t' WHERE JAM='19';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='20.00 - 21.00',cJNS='21',rowid='u' WHERE JAM='20';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='21.00 - 22.00',cJNS='22',rowid='v' WHERE JAM='21';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='22.00 - 23.00',cJNS='23',rowid='w' WHERE JAM='22';";
                                SQL += "UPDATE CUSTAB_JAM_" + Periode + " SET JENIS='23.00 - 00.00',cJNS='24',rowid='x' WHERE JAM='23';";
                                SQL += "DELETE FROM CUSTAB_JAM" + Periode + " WHERE SHOP='" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                SQL += "INSERT IGNORE INTO CUSTAB_JAM" + Periode + " SELECT SHOP,TANGGAL,ITEM,SUM(STRUK) AS STRUK,SUM(GROSS)AS GROSS,SUBSTRING(JAM,1,2)AS JAM ,JENIS,ROWID FROM CUSTAB_JAM_" + Periode + " GROUP BY JENIS,SHOP,TANGGAL,ROWID ORDER BY CJNS;";

                                //******************** CUSTAB PLU ******************************
                                SQL += "DELETE FROM CUSTAB_PLU_" + Periode + ";";
                                SQL += "INSERT INTO CUSTAB_PLU_" + Periode + "(SHOP,TANGGAL,PRDCD,QTY,GROSS,JAM) SELECT SHOP,STR_TO_DATE(TANGGAL,'%d-%m-%Y'),PRDCD,SUM(QTY) QTY,SUM(GROSS) GROSS,LEFT(JAM,2) JAM FROM " + PRS_FILE + "  GROUP BY SHOP,PRDCD,LEFT(JAM,2) ORDER BY SHOP,TANGGAL,JAM;";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='00.00 - 01.00',rowid='a' WHERE JAM='00';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='01.00 - 02.00',rowid='b' WHERE JAM='01';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='02.00 - 03.00',rowid='c' WHERE JAM='02';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='03.00 - 04.00',rowid='d' WHERE JAM='03';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='04.00 - 05.00',rowid='e' WHERE JAM='04';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='05.00 - 06.00',rowid='f' WHERE JAM='05';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='06.00 - 07.00',rowid='g' WHERE JAM='06';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='07.00 - 08.00',rowid='h' WHERE JAM='07';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='08.00 - 09.00',rowid='i' WHERE JAM='08';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='09.00 - 10.00',rowid='j' WHERE JAM='09';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='10.00 - 11.00',rowid='k' WHERE JAM='10';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='11.00 - 12.00',rowid='l' WHERE JAM='11';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='12.00 - 13.00',rowid='m' WHERE JAM='12';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='13.00 - 14.00',rowid='n' WHERE JAM='13';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='14.00 - 15.00',rowid='o' WHERE JAM='14';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='15.00 - 16.00',rowid='p' WHERE JAM='15';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='16.00 - 17.00',rowid='q' WHERE JAM='16';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='17.00 - 18.00',rowid='r' WHERE JAM='17';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='18.00 - 19.00',rowid='s' WHERE JAM='18';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='19.00 - 20.00',rowid='t' WHERE JAM='19';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='20.00 - 21.00',rowid='u' WHERE JAM='20';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='21.00 - 22.00',rowid='v' WHERE JAM='21';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='22.00 - 23.00',rowid='w' WHERE JAM='22';";
                                SQL += "UPDATE CUSTAB_PLU_" + Periode + " SET JENIS='23.00 - 00.00',rowid='x' WHERE JAM='23';";
                                SQL += "DELETE FROM CUSTAB_PLU" + Periode + " WHERE SHOP='" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                SQL += "INSERT INTO CUSTAB_PLU" + Periode + " SELECT SHOP,TANGGAL,PRDCD,SUM(QTY) QTY,SUM(GROSS) GROSS,JAM,JENIS,ROWID FROM CUSTAB_PLU_" + Periode + " GROUP BY SHOP,TANGGAL,PRDCD,JENIS;";
                                //******************** END ******************************
                                #endregion
                                #region Create Sales APKA & E-comarce
                                //shop, nama, tanggal, sales_apka_qty, sales_apka_gross, sales_apka_net, sales_apka_hpp, sales_eco_qty, sales_eco_gross, sales_eco_net, sales_eco_hpp, tglupd
                                SQL += "INSERT IGNORE INTO REKAPSLS_APKA" + Periode + " SELECT SHOP,'" + GetToko(TOKO, "NAMA_TOKO") + "',STR_TO_DATE(TANGGAL,'%d-%m-%Y'),";
                                SQL += "IFNULL((SELECT SUM(IF(RTYPE='J',QTY,0)) FROM " + PRS_FILE + " WHERE CAT_COD !='54005' AND TTYPE='AP'),0) AS APKA_QTY,";
                                SQL += "IFNULL((SELECT SUM(IF(RTYPE = 'J', GROSS, 0)) - SUM(IF(RTYPE = 'D', GROSS, 0)) FROM " + PRS_FILE + " WHERE CAT_COD NOT IN(" + CATCOD + ") AND TTYPE = 'AP'),0) AS APKA_GROSS,";
                                SQL += "IFNULL((SELECT SUM(IF(RTYPE = 'J', GROSS - PPN, 0)) - SUM(IF(RTYPE = 'D', GROSS - PPN, 0)) FROM " + PRS_FILE + " WHERE CAT_COD NOT IN(" + CATCOD + ") AND TTYPE = 'AP'),0) AS APKA_NET,";
                                SQL += "IFNULL((SELECT SUM(IF(RTYPE = 'J', HPP * QTY, 0)) - SUM(IF(RTYPE = 'D', HPP * QTY, 0)) FROM " + PRS_FILE + " WHERE CAT_COD NOT IN(" + CATCOD + ") AND TTYPE = 'AP'),0) AS APKA_HPP,";
                                SQL += "IFNULL((SELECT SUM(IF(RTYPE = 'J', QTY, 0)) FROM " + PRS_FILE + " WHERE CAT_COD != '54005' AND TTYPE = 'IS'),0) AS ECO_QTY,";
                                SQL += "IFNULL((SELECT SUM(IF(RTYPE = 'J', GROSS, 0)) - SUM(IF(RTYPE = 'D', GROSS, 0)) FROM " + PRS_FILE + " WHERE CAT_COD NOT IN(" + CATCOD + ") AND TTYPE = 'IS'),0) AS ECO_GROSS,";
                                SQL += "IFNULL((SELECT SUM(IF(RTYPE = 'J', GROSS - PPN, 0)) - SUM(IF(RTYPE = 'D', GROSS - PPN, 0)) FROM " + PRS_FILE + " WHERE CAT_COD NOT IN(" + CATCOD + ") AND TTYPE = 'IS'),0) AS ECO_NET,";
                                SQL += "IFNULL((SELECT SUM(IF(RTYPE = 'J', HPP * QTY, 0)) - SUM(IF(RTYPE = 'D', HPP * QTY, 0)) FROM " + PRS_FILE + " WHERE CAT_COD NOT IN(" + CATCOD + ") AND TTYPE = 'IS'),0) AS ECO_HPP,NOW() ";
                                SQL += "FROM " + PRS_FILE + " GROUP BY SHOP;";
                                #endregion
                                break;
                            case "PKM":
                                SQL = "DELETE FROM PKM" + Periode + " WHERE TOKO='" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                SQL += "INSERT INTO PKM" + Periode + "(TOKO, NAMA, TANGGAL, TUA, MODE, TIPE_TOKO, TIPE_HARGA, GUDANG_KIRIM, TGL_PKM, TB_RASIO, FIXED_TB, TBL_DIV, TBL_SS) ";
                                SQL += "SELECT TOKO, NAMA, STR_TO_DATE(TGL,'%d-%m-%Y'), TUA, MODE, TP_TOKO, TP_HARGA, KIRIM, STR_TO_DATE(TGL_PKM,'%d-%m-%Y'), TB_RASIO, FIXED_TB, TBL_DIV, TBL_SS FROM " + PRS_FILE + ";";
                                SQL += "INSERT INTO CEKPRD" + Periode + "(TOKO, NAMA, TANGGAL, TYPE_HARGA, TYPE_RAK, TUA, MODE, TGL_PKM, TB_RASIO, FIXED_TB, TBL_DIV, TBL_SS) ";
                                SQL += "SELECT TOKO, NAMA, STR_TO_DATE(TGL,'%d-%m-%Y'),TP_HARGA,TP_TOKO,TUA,MODE,STR_TO_DATE(TGL_PKM, '%d-%m-%Y'),TB_RASIO,FIXED_TB,TBL_DIV,TBL_SS FROM " + PRS_FILE + " ";
                                SQL += "ON DUPLICATE KEY UPDATE TANGGAL = STR_TO_DATE(" + PRS_FILE + ".TGL, '%d-%m-%Y'), TYPE_HARGA = " + PRS_FILE + ".TP_HARGA, TYPE_RAK = " + PRS_FILE + ".TP_TOKO, TUA = " + PRS_FILE + ".TUA, MODE = " + PRS_FILE + ".MODE, TGL_PKM = STR_TO_DATE(" + PRS_FILE + ".TGL_PKM, '%d-%m-%Y') ";
                                SQL += ", TB_RASIO = " + PRS_FILE + ".TB_RASIO, FIXED_TB = " + PRS_FILE + ".FIXED_TB, TBL_DIV = " + PRS_FILE + ".TBL_DIV, TBL_SS = " + PRS_FILE + ".TBL_SS;";
                                break;
                            case "SLP":

                                /******************** PROSES SLP ******************************/

                                SQL = "DROP TABLE IF EXISTS SLP" + TMP + ";";
                                SQL += "DELETE FROM SLP" + Periode + " WHERE KDTK='" + TOKO + "' AND TGL_DATA='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
								SQL += "DELETE FROM SLP WHERE TOKO='" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                SQL += "INSERT IGNORE INTO SLP" + Periode + "(KDTK, NAMA, TGL_DATA, KODE, SUBKODE, `DESC`, RUPIAH, TANGGAL, TP_111, TP_112, TP_121, TP_122, TP_131, TP_132, TP_141, TP_142, TP_211, TP_212, TP_221, TP_222, TP_231, TP_232, TP_241, TP_242, PRICE, QTY, GROSS, PPN, NO_KSM, DOCNO, FTILLID, PLU, NO_HP, NO_BPB, TGL_BPB, RP_BPB, FBASECODE, FSTOREID, FCASHIER, FTRANSID, FTIME, KIRIM_WU, FEE_WU, AMBIL_WU, MTCN, NO_TRACE, NO_APPRO, NO_BATCH, NM_KA) ";
                                SQL += "SELECT '" + TOKO + "','" + GetToko(TOKO, "NAMA_TOKO") + "','" + TANGGAL.ToString("yyyy-MM-dd") + "',KODE, SUBKODE, `DESC`, RUPIAH, TANGGAL, TP_111, TP_112, TP_121, TP_122, TP_131, TP_132, TP_141, TP_142, TP_211, TP_212, TP_221, TP_222, TP_231, TP_232, TP_241, TP_242, PRICE, QTY, GROSS, PPN, NO_KSM, DOCNO, FTILLID, PLU, NO_HP, NO_BPB, TGL_BPB, RP_BPB, FBASECODE, FSTOREID, FCASHIER, FTRANSID, FTIME, KIRIM_WU, FEE_WU, AMBIL_WU, MTCN, NO_TRACE, NO_APPRO, NO_BATCH, NM_KA FROM " + PRS_FILE + ";";
                                SQL += "INSERT IGNORE SLP(TOKO,NAMA,TANGGAL) VALUES ('" + TOKO + "','" + GetToko(TOKO, "NAMA_TOKO") + "','" + TANGGAL.ToString("yyyy-MM-dd") + "');";
                                SQL += "INSERT INTO SLP(TOKO,NAMA,TANGGAL) VALUES ('" + TOKO + "','" + GetToko(TOKO, "NAMA_TOKO") + "','" + TANGGAL.ToString("yyyy-MM-dd") + "') ON DUPLICATE KEY UPDATE SALES=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='SALES'),";
                                SQL += "SLSHPP=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='SLS_HPP'),";
                                SQL += "PPN=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='PPN'),";
                                SQL += "COUNTER1=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='COUNTER1'),";
                                SQL += "COUNTER2=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='COUNTER2'),";
                                SQL += "PPN_CTR2=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='PPN_CTR2'),";
                                SQL += "CASH=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='CASH'),";
                                SQL += "DBT_BCA=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='DEBIT'),";
                                SQL += "TN_BCA=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='TUNAI'),";
                                SQL += "KPN_IDM=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='KPN_IDM'),";
                                SQL += "KPN_NIDM=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='KPN_NIDM'),";
                                SQL += "DISCOUNT=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='DISCOUNT'),";
                                SQL += "VARIANCE=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='VARIANCE'),";
                                SQL += "JML_STRK=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='JML_STRK'),";
                                SQL += "BUKA=(SELECT SUBSTR(`DESC`,6) FROM " + PRS_FILE + " WHERE `KODE`='14'),";
                                SQL += "TUTUP=(SELECT SUBSTR(`DESC`,7) FROM " + PRS_FILE + " WHERE `KODE`='15'),";
                                SQL += "LOG_KIRIM=(SELECT STR_TO_DATE(TANGGAL,'%d-%m-%Y') FROM " + PRS_FILE + " WHERE `KODE`='28'),";
                                SQL += "TGL_KIRIM=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='TGL_KIRIM'),";
                                SQL += "STK_OUT=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='STK_OUT'),";
                                SQL += "BRG_HLG=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='BRG_HLG'),";
                                SQL += "BRG_RSK=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='BRG_RSK'),";
                                SQL += "KTG_KCL=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `KODE`='21' AND SUBKODE='01'),";
                                SQL += "KTG_SDG=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `KODE`='21' AND SUBKODE='02'),";
                                SQL += "KTG_BSR=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `KODE`='21' AND SUBKODE='03'),";
                                SQL += "SLS14601=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `KODE`='30' AND SUBKODE=''),";
                                SQL += "PPN14601=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `KODE`='31' AND SUBKODE=''),";
                                SQL += "KREDIT=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `KODE`='33' AND SUBKODE=''),";
                                SQL += "PENGGANTIAN=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `KODE`='34' AND SUBKODE=''),";
                                SQL += "PPN_BBS=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `KODE`='36' AND SUBKODE='');";
                                SQL += "UPDATE REKAPSLS" + Periode + " A,SLP B SET A.STRUK=B.JML_STRK WHERE A.SHOP=B.TOKO AND A.TANGGAL=B.TANGGAL AND A.SHOP='" + TOKO + "' AND A.TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                // CAST('" . $tutup	. "' AS TIME)
                                //2016-03-08 PENAMBAHAN JAM OPERATIONAL TOKO
                                SQL += "INSERT INTO OPRTOKO" + Periode + "(TOKO,NAMA,BUKA" + TGL + ",TUTUP" + TGL + ") VALUES ('" + TOKO + "','" + GetToko(TOKO, "NAMA_TOKO") + "',(SELECT SUBSTR(`DESC`,6) FROM " + PRS_FILE + " WHERE `KODE`='14'),(SELECT SUBSTR(`DESC`,6) FROM " + PRS_FILE + " WHERE `KODE`='15')) ";
                                SQL += "ON DUPLICATE KEY UPDATE BUKA" + TGL + " = (SELECT SUBSTR(`DESC`,6) FROM " + PRS_FILE + " WHERE `KODE`='14'),TUTUP" + TGL + " = (SELECT SUBSTR(`DESC`,6) FROM " + PRS_FILE + " WHERE `KODE`='15');";
                                SQL += "INSERT INTO TRENDSTOCKOUT" + Periode + "(SHOP,NAMA," + TG + ") VALUES ('" + TOKO + "','" + GetToko(TOKO, "NAMA_TOKO") + "',(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='STK_OUT')) ";
                                SQL += "ON DUPLICATE KEY UPDATE " + TG + " = (SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='STK_OUT');";
                                /******************** PROSES SLS ******************************/

                                SQL += "INSERT INTO SLS" + Periode + "(TOKO,NAMA,SLS" + TGL + ",STK" + TGL + ",MGR" + TGL + ") VALUES('" + TOKO + "','" + GetToko(TOKO, "NAMA_TOKO") + "',(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='SALES'),(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='JML_STRK'),ROUND((SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='SALES')-(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='SLS_HPP'))) ";
                                SQL += "ON DUPLICATE KEY UPDATE SLS" + TGL + "=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='SALES'),STK" + TGL + "=(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='JML_STRK'),MGR" + TGL + "=ROUND((SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='SALES')-(SELECT RUPIAH FROM " + PRS_FILE + " WHERE `DESC`='SLS_HPP'));";
                                //
                                break;
                            case "WT":
                                string WT = FILE + TANGGAL.ToString("MMdd") + TOKO.Substring(0, 1) + "." + TOKO.Substring(1, 3);
                                SQL = "DROP TABLE IF EXISTS WT" + TMP + ";";
                                SQL += "CREATE TEMPORARY TABLE WT" + TMP + " LIKE WTRAN" + Periode + ";";
                                SQL += "INSERT IGNORE WT" + TMP + "(RCID, RTYPE, DOCNO, `DIV`, PRDCD, QTY, PRICE, GROSS, PPN, DOCNO2, ISTYPE, INVNO, TOKO, KETERANGAN, TGL1, TGL2, DOCNO3, SHOP, GROSS_JUAL, PRICE_JUAL,FILE_SUMBER)";
                                SQL += "SELECT RECID AS RCID, RTYPE, DOCNO, `DIV`, PRDCD, QTY, PRICE, GROSS, PPN, DOCNO2, ISTYPE, INVNO, TOKO, KETERANGAN, STR_TO_DATE(TGL1,'%d-%m-%Y'), STR_TO_DATE(TGL2,'%d-%m-%Y'), DOCNO3, SHOP, GROSS_JUAL, PRICE_JUAL,'" + WT + "' FROM " + PRS_FILE + ";";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='BARANG RETUR PERFORMA' WHERE KETERANGAN LIKE '0101%';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='BARANG RUSAK DRY' WHERE KETERANGAN LIKE '0309%';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='BARANG RUSAK DRY' WHERE KETERANGAN LIKE '0304%';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='BARANG RUSAK TELUR' WHERE KETERANGAN LIKE '0311%';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='BARANG RUSAK BUAH' WHERE KETERANGAN LIKE '0305%';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='BARANG RUSAK ROTI' WHERE KETERANGAN LIKE '04070201%';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='BARANG RUSAK ROTI' WHERE KETERANGAN LIKE '04070202%';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='BARANG RUSAK MAJALAH' WHERE KETERANGAN LIKE '04070300%';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='BERITA ACARA' WHERE KETERANGAN LIKE 'BERITA%';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='BARANG RETUR BAIK' WHERE KETERANGAN LIKE '0401%';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='BARANG RETUR BAIK' WHERE KETERANGAN LIKE '0402%';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='BARANG RETUR BAIK' WHERE KETERANGAN LIKE '04070101%';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='BARANG RETUR BAIK' WHERE KETERANGAN LIKE '04070102%';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='NKL ITEM' WHERE KETERANGAN LIKE 'SI%' AND rtype='x';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='NKL HARIAN' WHERE KETERANGAN LIKE 'SO%' AND rtype='x';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='NKL BULANAN' WHERE KETERANGAN LIKE 'SB%' AND rtype='x';";
                                SQL += "UPDATE WT" + TMP + " SET TTYPE='NKL OTOMATIS' WHERE KETERANGAN LIKE 'TAG N%' and rtype='x';";
                                SQL += "UPDATE WT" + TMP + " SET FILE_SUMBER='" + WT + "';";
                                SQL += "DELETE FROM WTRAN" + Periode + " WHERE SHOP='" + TOKO + "' AND TGL1='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                SQL += "INSERT IGNORE WTRAN" + Periode + " SELECT * FROM WT" + TMP + ";";
                                SQL += "CREATE TEMPORARY TABLE WTX" + TMP + " SELECT PRDCD,SUM(QTY) AS QTY FROM " + PRS_FILE + " WHERE RTYPE='B';";
                                SQL += "INSERT INTO HISPB" + Periode + "(SHOP,PRDCD,NPB" + TGL + ") SELECT '" + TOKO + "',PRDCD,QTY FROM WTX" + TMP + " ";
                                SQL += "ON DUPLICATE KEY UPDATE NPB" + TGL + "=WTX" + TMP + ".QTY;";
                                break;
                            case "ST":
                                string STAUT = "STAUT" + int.Parse(TANGGAL.ToString("dd")).ToString();
                                SQL = "";
                                SQL += "ALTER TABLE " + PRS_FILE + " ADD COLUMN `TAG` VARCHAR(1) DEFAULT '';";
                                SQL += "ALTER TABLE " + PRS_FILE + " ADD PRIMARY KEY (`PRDCD`);";
                                SQL += "UPDATE " + PRS_FILE + " A, PRODMAST" + Periode + " B SET A.TAG=B.PTAG WHERE A.PRDCD=B.PRDCD;";
                                /******************* INSERT TABEL STOCKOUT *******************/
                                //SQL += "DELETE FROM STM" + Periode + " WHERE SHOP='" + TOKO + "' AND TGL_DATA='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                //, , STAUT1, STAUT2, STAUT3, STAUT4, STAUT5, STAUT6, STAUT7, STAUT8, STAUT9, STAUT10, STAUT11, STAUT12, STAUT13, STAUT14, STAUT15, STAUT16, STAUT17, STAUT18, STAUT19, STAUT20, STAUT21, STAUT22, STAUT23, STAUT24, STAUT25, STAUT26, STAUT27, STAUT28, STAUT29, STAUT30, STAUT31, ACOST, TP_GDG, SPD
                                SQL += "INSERT INTO STOCKOUT" + Periode + "(SHOP,PRDCD,ACOST, TP_GDG, SPD," + STAUT + ",PRICE,TAG) SELECT '" + TOKO + "',PRDCD,ACOST,TP_GDG,SPD,STAUT,PRICE,TAG FROM " + PRS_FILE + " WHERE STAUT = '1' ";
                                SQL += "ON DUPLICATE KEY UPDATE ACOST=" + PRS_FILE + ".ACOST, TP_GDG=" + PRS_FILE + ".TP_GDG, SPD=" + PRS_FILE + ".SPD," + STAUT + "=" + PRS_FILE + ".STAUT,PRICE=" + PRS_FILE + ".PRICE,TAG=" + PRS_FILE + ".TAG;";
                                //, , , , , QTY1, MAX1, STS1, PKMG1, STAUT1, QTY2, MAX2, STS2, PKMG2, STAUT2, QTY3, MAX3, STS3, PKMG3, STAUT3, QTY4, MAX4, STS4, PKMG4, STAUT4, QTY5, MAX5, STS5, PKMG5, STAUT5, QTY6, MAX6, STS6, PKMG6, STAUT6, QTY7, MAX7, STS7, PKMG7, STAUT7, QTY8, MAX8, STS8, PKMG8, STAUT8, QTY9, MAX9, STS9, PKMG9, STAUT9, QTY10, MAX10, STS10, PKMG10, STAUT10, QTY11, MAX11, STS11, PKMG11, STAUT11, QTY12, MAX12, STS12, PKMG12, STAUT12, QTY13, MAX13, STS13, PKMG13, STAUT13, QTY14, MAX14, STS14, PKMG14, STAUT14, QTY15, MAX15, STS15, PKMG15, STAUT15, QTY16, MAX16, STS16, PKMG16, STAUT16, QTY17, MAX17, STS17, PKMG17, STAUT17, QTY18, MAX18, STS18, PKMG18, STAUT18, QTY19, MAX19, STS19, PKMG19, STAUT19, QTY20, MAX20, STS20, PKMG20, STAUT20, QTY21, MAX21, STS21, PKMG21, STAUT21, QTY22, MAX22, STS22, PKMG22, STAUT22, QTY23, MAX23, STS23, PKMG23, STAUT23, QTY24, MAX24, STS24, PKMG24, STAUT24, QTY25, MAX25, STS25, PKMG25, STAUT25, QTY26, MAX26, STS26, PKMG26, STAUT26, QTY27, MAX27, STS27, PKMG27, STAUT27, QTY28, MAX28, STS28, PKMG28, STAUT28, QTY29, MAX29, STS29, PKMG29, STAUT29, QTY30, MAX30, STS30, PKMG30, STAUT30, QTY31, MAX31, STS31, PKMG31, STAUT31, 
                                SQL += "INSERT IGNORE STMAST" + Periode + "(SHOP,PRDCD,CAT_COD,BEGBAL,SPD,QTY" + TGL + ",MAX" + TGL + ",STS" + TGL + ",PKMG" + TGL + ",STAUT" + TGL + ",STKQTY, TGL_STK, PKM, PKMG, ACOST, LCOST, PLCOST, PRCOST, TP_GDG, RP_ADJ_K, RP_ADJ_L, MIN_DISP, KAP_DISP) ";
                                SQL += "SELECT '" + TOKO + "',PRDCD,CAT_COD,BEGBAL,SPD,QTY,MAX,'1',PKM_G,STAUT,QTY,'" + TANGGAL.ToString("yyyy-MM-dd") + "',MAX,PKM_G,ACOST, LCOST, PLCOST, PRCOST, TP_GDG, RP_ADJ_K, RP_ADJ_L, MIN_DISP, KAP_DISP FROM " + PRS_FILE + " ";
                                SQL += "ON DUPLICATE KEY UPDATE CAT_COD=" + PRS_FILE + ".CAT_COD,BEGBAL=" + PRS_FILE + ".BEGBAL,SPD=" + PRS_FILE + ".SPD,QTY" + TGL + "=" + PRS_FILE + ".QTY,MAX" + TGL + "=" + PRS_FILE + ".MAX,STS" + TGL + "='1',PKMG" + TGL + "=" + PRS_FILE + ".PKM_G,STAUT" + TGL + "=" + PRS_FILE + ".STAUT ";
                                SQL += ",STKQTY=" + PRS_FILE + ".QTY, TGL_STK='" + TANGGAL.ToString("yyyy-MM-dd") + "', PKM=" + PRS_FILE + ".MAX, PKMG=" + PRS_FILE + ".PKM_G, ACOST=" + PRS_FILE + ".ACOST, LCOST=" + PRS_FILE + ".LCOST, PLCOST=" + PRS_FILE + ".PLCOST, PRCOST=" + PRS_FILE + ".PRCOST, TP_GDG=" + PRS_FILE + ".TP_GDG, RP_ADJ_K=" + PRS_FILE + ".RP_ADJ_K, RP_ADJ_L=" + PRS_FILE + ".RP_ADJ_L, MIN_DISP=" + PRS_FILE + ".MIN_DISP, KAP_DISP=" + PRS_FILE + ".KAP_DISP;";
                                //SQL += "UPDATE SLP SET STK_OUT_DRY=(SELECT SUM(staut) FROM " + PRS_FILE + " WHERE PRDCD NOT IN(SELECT PRDCD FROM PRODMAST" + Periode + " WHERE KONS = 'Y' OR PTAG IN('T','V') OR CAT_COD LIKE '4%') AND TOKO='" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "');";
                                SQL += "UPDATE SLP SET STK_OUT_DRY=(SELECT SUM(staut) FROM " + PRS_FILE + " WHERE PRDCD NOT IN(SELECT PRDCD FROM PRODMAST" + Periode + " WHERE KONS = 'Y' OR PTAG IN('T','V') OR CAT_COD LIKE '4%')) WHERE TOKO='" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                //UPDATE DATA HISPB
                                SQL += "INSERT INTO HISPB" + Periode + "(SHOP,PRDCD,QTY" + TGL + ",PKM" + TGL + ") SELECT '" + TOKO + "',PRDCD,QTY,`MAX` FROM " + PRS_FILE + " ";
                                SQL += "ON DUPLICATE KEY UPDATE QTY" + TGL + "=" + PRS_FILE + ".QTY,PKM" + TGL + "=" + PRS_FILE + ".`MAX`;";
                                break;
                            case "NTB":
                                SQL = "DELETE FROM NTB" + Periode + " WHERE TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "' AND TOKO='" + TOKO + "';";
                                /******************* INSERT TABEL STOCKOUT *******************/
                                //SQL += "DELETE FROM STM" + Periode + " WHERE SHOP='" + TOKO + "' AND TGL_DATA='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                //, , STAUT1, STAUT2, STAUT3, STAUT4, STAUT5, STAUT6, STAUT7, STAUT8, STAUT9, STAUT10, STAUT11, STAUT12, STAUT13, STAUT14, STAUT15, STAUT16, STAUT17, STAUT18, STAUT19, STAUT20, STAUT21, STAUT22, STAUT23, STAUT24, STAUT25, STAUT26, STAUT27, STAUT28, STAUT29, STAUT30, STAUT31, ACOST, TP_GDG, SPD
                                SQL += "INSERT INTO NTB" + Periode + " SELECT STATION, NO_STRUK, TOKO, STR_TO_DATE(TANGGAL,'%d-%m-%Y'), KD_PROMO, `GROUP`, NOURUT, JNS_NTAMBAH, PLU_NTAMBAH, QTY_NTAMBAH, RP_NTAMBAH from " + PRS_FILE + ";";
                                break;
                            case "LQS":
                                SQL = "";
                                SQL += "UPDATE ABSENFILE" + Periode + " SET " + TG + "=(SELECT IF(COUNT(*)=0,'0','1') FROM " + PRS_FILE + ") WHERE SHOP='" + TOKO + "' AND FILE='" + FILE + "';";
                                SQL += "UPDATE ABSENFILE" + Periode + " SET TOTAL=TG1+TG2+TG3+TG4+TG5+TG6+TG7+TG8+TG9+TG10+TG11+TG12+TG13+TG14+TG15+TG16+TG17+TG18+TG19+TG20+TG21+TG22+TG23+TG24+TG25+TG26+TG27+TG28+TG29+TG30+TG31 WHERE SHOP='" + TOKO + "' AND FILE='" + FILE + "';";
                                //Log("TES", "LQS", SQL);
                                /******************* INSERT TABEL STOCKOUT *******************/
                                //SQL += "DELETE FROM STM" + Periode + " WHERE SHOP='" + TOKO + "' AND TGL_DATA='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                //, , STAUT1, STAUT2, STAUT3, STAUT4, STAUT5, STAUT6, STAUT7, STAUT8, STAUT9, STAUT10, STAUT11, STAUT12, STAUT13, STAUT14, STAUT15, STAUT16, STAUT17, STAUT18, STAUT19, STAUT20, STAUT21, STAUT22, STAUT23, STAUT24, STAUT25, STAUT26, STAUT27, STAUT28, STAUT29, STAUT30, STAUT31, ACOST, TP_GDG, SPD
                                break;
                            case "CL":
                                SQL = "";
                                SQL += "INSERT INTO RKBSPV" + Periode + " SELECT HAR_DC_CODE, HAR_FK_ASPV_CODE, HAR_FK_ASPV_NIK, HAR_FK_ASPV_NAME, '" + TANGGAL.ToString("yyyyMMdd") + "', HAR_BULAN, HAR_TAHUN, HAR_TOK_CODE, HAR_TOK_NAME, HAR_GROUP_CODE, HAR_GROUP_NAMA, HAR_ITEM_CODE, HAR_ITEM_NAMA, HAR_ITEMDTL_CODE, HAR_ITEMDTL_NAMA, HAR_KONDISI, HAR_BOBOT, HAR_PENYEBAB, HAR_FOLLOWUP, HAR_KETERANGAN, HAR_PENCAPAIAN, HAR_FLAG_TDKADA_BOBOT, HAR_JAM_MASUK, HAR_JAM_KELUAR, HAR_TGL_CHECK, HAR_PENGGANTI, HAR_PASSWORD FROM " + PRS_FILE + ";";
                                break;
                            case "CM":
                                SQL = "";
                                //HAR_DC_CODE, HAR_FK_AMGR_CODE, HAR_FK_AMGR_NIK, HAR_FK_AMGR_NAME, HAR_FK_TANGGAL, HAR_BULAN, HAR_TAHUN, HAR_TOK_CODE, HAR_TOK_NAME, HAR_GROUP_CODE, HAR_GROUP_NAMA, HAR_ITEM_CODE, HAR_ITEM_NAMA, HAR_ITEMDTL_CODE, HAR_ITEMDTL_NAMA, HAR_KONDISI, HAR_BOBOT, HAR_PENYEBAB, HAR_FOLLOWUP, HAR_KETERANGAN, HAR_PENCAPAIAN, HAR_FLAG_TDKADA_BOBOT, HAR_JAM_MASUK, HAR_JAM_KELUAR, HAR_TGL_CHECK, HAR_PENGGANTI, HAR_PASSWORD
                                SQL += "INSERT INTO RKBAM" + Periode + " SELECT HAR_DC_CODE, HAR_FK_AMGR_CODE, HAR_FK_AMGR_NIK, HAR_FK_AMGR_NAME, '" + TANGGAL.ToString("yyyyMMdd") + "', HAR_BULAN, HAR_TAHUN, HAR_TOK_CODE, HAR_TOK_NAME, HAR_GROUP_CODE, HAR_GROUP_NAMA, HAR_ITEM_CODE, HAR_ITEM_NAMA, HAR_ITEMDTL_CODE, HAR_ITEMDTL_NAMA, HAR_KONDISI, HAR_BOBOT, HAR_PENYEBAB, HAR_FOLLOWUP, HAR_KETERANGAN, HAR_PENCAPAIAN, HAR_FLAG_TDKADA_BOBOT, HAR_JAM_MASUK, HAR_JAM_KELUAR, HAR_TGL_CHECK, HAR_PENGGANTI, HAR_PASSWORD FROM " + PRS_FILE + ";";
                                break;
                            case "CA":
                                SQL = "DELETE FROM CANCEL" + Periode + " WHERE SHOP = '" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyyMMdd") + "';";
                                //STR_TO_DATE(TGL1,'%d-%m-%Y')
                                SQL += "INSERT INTO CANCEL" + Periode + "(RECID, STATION, SHIFT, RTYPE, DOCNO, SEQNO, `DIV`, CAT_COD, PRDCD, QTY, QTY1, PRICE, HPP, GROSS, DISCOUNT, SHOP, TANGGAL, JAM, KONS, TTYPE, PTAG, PROMOSI, PPN, KASIR_NAME) SELECT RECID, STATION, SHIFT, RTYPE, DOCNO, SEQNO, `DIV`, CAT_COD, PRDCD, QTY, QTY1, PRICE, HPP, GROSS, DISCOUNT, SHOP, STR_TO_DATE(TANGGAL,'%d-%m-%Y'), JAM, KONS, TTYPE, PTAG, PROMOSI, PPN, KASIR_NAME FROM " + PRS_FILE + ";";
                                break;
                            case "LDR":
                                //SQL = "DELETE FROM LAUNDRY" + Periode + " WHERE LEFT(TOKO,4) = '" + TOKO + "' AND NCDate = '" + TANGGAL.ToString("yyyyMMdd") + "';";
                                SQL = "INSERT IGNORE LAUNDRY" + Periode + "(NoNC, NoLayanan, NCDate, TOKO, SUPCO, Nama, Alamat, NoTelp, PLU, Deskrip, Qty, Harga, Rupiah, TglPengambilan, TglPembayaran, NoSJ, SJDate, NoTTB, TTBDate, Status) SELECT NoNC, NoLayanan, STR_TO_DATE(TRIM(REPLACE(REPLACE(ncdate,'00:00:00',''),'0:00:00','')),'%m/%d/%Y'), TOKO, SUPCO, Nama, Alamat, NoTelp, PLU, Deskrip, Qty, Harga, Rupiah, TglPengambilan, STR_TO_DATE(TRIM(REPLACE(REPLACE(TglPembayaran,'00:00:00',''),'0:00:00','')),'%m/%d/%Y'), NoSJ, SJDate, NoTTB, TTBDate, Status FROM " + PRS_FILE + ";";
                                break;
                            case "TL":
                                //FSTOREID, NM_TOKO, ALAMAT, KOTA, STATION, SHIFT, FTILLID, FCASHIERID, FTRANSID, FDATE, FTIME, FMSERIAL, FBASECODE, FLIVECODE, FSKU, NM_SKU, FVALUE, FLINEQTY, FPHONE, FTELCO, TGL_BPB, SUP_BPB, NO_BPB, RP_BPB, PRICE_IDM, PPNRP_IDM, KD_DC
                                SQL = "DELETE FROM TLOG" + Periode + " WHERE FSTOREID = '" + TOKO + "' AND FDATE = '" + TANGGAL.ToString("yyyyMMdd") + "';";
                                SQL = "INSERT IGNORE TLOG" + Periode + "(FSTOREID, NM_TOKO, ALAMAT, KOTA, STATION, SHIFT, FTILLID, FCASHIERID, FTRANSID, FDATE, FTIME, FMSERIAL, FBASECODE, FLIVECODE, FSKU, NM_SKU, FVALUE, FLINEQTY, FPHONE, FTELCO, TGL_BPB, SUP_BPB, NO_BPB, RP_BPB, PRICE_IDM, PPNRP_IDM, KD_DC) SELECT FSTOREID, NM_TOKO, ALAMAT, KOTA, STATION, SHIFT, FTILLID, FCASHIERID, FTRANSID, FDATE, FTIME, FMSERIAL, FBASECODE, FLIVECODE, FSKU, NM_SKU, FVALUE, FLINEQTY, FPHONE, FTELCO, TGL_BPB, SUP_BPB, NO_BPB, RP_BPB, PRICE_IDM, PPNRP_IDM, KD_DC Status FROM " + PRS_FILE + ";";
                                break;
                            case "PBSL":
                                var PB_temp = "PBVSNPB" + Periode;
                                //RECID, RTYPE, DOCNO, PRDCD, SINGKAT, DIV, QTY, TOKO, TGL_PB, PKM_AKH, LENGTH, WIDTH, HEIGHT, MINOR, GUDANG, MAX, PKM_G, QTYM1, SPD, PRICE, GROSS, PTAG, QTY_MAN, PPN, KM, JAMIN, JAMOUT, NO_SJ
                                SQL = "ALTER TABLE " + PRS_FILE + " ADD COLUMN QTY_NPB DECIMAL(9,0) DEFAULT 0,ADD COLUMN QTY_STDC DECIMAL(9,0) DEFAULT 0;";
                                SQL += "INSERT IGNORE HISPBDOC" + Periode + "(SHOP,TANGGAL,DOCNO) SELECT DISTINCT(TOKO),STR_TO_DATE(TGL_PB,'%d-%m-%Y'),DOCNO FROM " + PRS_FILE + ";";
                                SQL += "DROP TABLE IF EXISTS " + PB_temp + ";";
                                SQL += "CREATE TABLE " + PB_temp + " SELECT PLU,TOKO,QTY FROM BRDDC" + Periode + " WHERE TOKO IN(SELECT TOKO FROM " + PRS_FILE + ") AND NO_INV IN(SELECT DOCNO FROM " + PRS_FILE + ");";
                                SQL += "UPDATE " + PRS_FILE + " A, " + PB_temp + " B SET A.QTY_NPB=B.QTY WHERE A.PRDCD=B.PLU AND A.TOKO=B.TOKO;";
                                //SQL += "UPDATE " + PRS_FILE + " A, BRDDC" + Periode + " B SET A.QTY_NPB=B.QTY WHERE A.PRDCD=B.PLU AND A.TOKO=B.TOKO AND A.DOCNO=B.NO_INV;";
                                SQL += "UPDATE " + PRS_FILE + " A, STMASTDC" + Periode + " B SET A.QTY_STDC=B.QTY" + TGL + " WHERE A.PRDCD=B.PRDCD;";
                                //SQL += "DELETE QUICK FROM PBSL" + Periode + " WHERE TOKO = '" + TOKO + "' AND TGL_PB = '" + TANGGAL.ToString("yyyyMMdd") + "';";
                                //SQL += "INSERT INTO PBSL" + Periode + "(RECID, RTYPE, DOCNO, PRDCD, SINGKAT, `DIV`, QTY, TOKO, TGL_PB, PKM_AKH, LENGTH, WIDTH, HEIGHT, MINOR, GUDANG, MAX, PKM_G, QTYM1, SPD, PRICE, GROSS, PTAG, QTY_MAN, PPN, KM, JAMIN, JAMOUT, NO_SJ,QTY_NPB,QTY_STDC) SELECT RECID, RTYPE, DOCNO, PRDCD, SINGKAT, `DIV`, QTY, TOKO, STR_TO_DATE(TGL_PB,'%d-%m-%Y'), PKM_AKH, LENGTH, WIDTH, HEIGHT, MINOR, GUDANG, MAX, PKM_G, QTYM1, SPD, PRICE, GROSS, PTAG, QTY_MAN, PPN, KM, JAMIN, JAMOUT, NO_SJ, QTY_NPB ,QTY_STDC FROM " + PRS_FILE + ";";
                                SQL += "INSERT INTO HISPB" + Periode + "(SHOP,PRDCD,SINGKAT,PB" + TGL + ",MINOR" + TGL + ") SELECT TOKO,PRDCD,SINGKAT,QTY,MINOR FROM " + PRS_FILE + " ";
                                SQL += "ON DUPLICATE KEY UPDATE PB" + TGL + "=" + PRS_FILE + ".`QTY`,MINOR" + TGL + "=" + PRS_FILE + ".`MINOR`;";
                                //Log("x", "x", SQL);
                                //MessageBox.Show(SQL);
                                break;
                            case "CRDH":
                                SQL += "DELETE FROM CRDH" + Periode + " WHERE SHOP='" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                SQL += "INSERT IGNORE CRDH" + Periode + " (RECID, STATION, SHIFT, RTYPE, DOCNO, SEQNO, `DIV`, PRDCD, QTY, PRICE, GROSS, DISCOUNT, SHOP, TANGGAL, JAM, KONS, TTYPE, HPP, PROMOSI, PPN, PTAG, CAT_COD, NM_VCR, NO_VCR, BKP, SUB_BKP, KDBIN, NOKARTU) ";
                                SQL += "SELECT RECID, STATION, SHIFT, RTYPE, DOCNO, SEQNO, `DIV`, PRDCD, QTY, PRICE, GROSS, DISCOUNT, SHOP, STR_TO_DATE(TANGGAL,'%d-%m-%Y'), JAM, KONS, TTYPE, HPP, PROMOSI, PPN, PTAG, CAT_COD, NM_VCR, NO_VCR, BKP, SUB_BKP, KDBIN,NOKARTU FROM " + PRS_FILE + ";";
                                break;
                            case "TTD":
                                //SQL += "DELETE FROM CRDH" + Periode + " WHERE SHOP='" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                SQL += "INSERT INTO TTD" + Periode + " (NOTTD,TANGGAL,GUDANG,NONRB,TANGGALNRB,NOBA,TANGGALBA,NODELIVERYVAN,KOLI,KONTAINER) ";
                                SQL += "SELECT NOTTD,STR_TO_DATE(TANGGAL,'%d-%m-%Y'),GUDANG,NONRB,STR_TO_DATE(TANGGALNRB,'%d-%m-%Y'),NOBA,TANGGALBA,NODELIVERYVAN,KOLI,KONTAINER FROM " + PRS_FILE + " ";
                                SQL += "ON DUPLICATE KEY UPDATE TANGGAL=STR_TO_DATE(" + PRS_FILE + ".TANGGAL, '%d-%m-%Y'),NOTTD=" + PRS_FILE + ".NOTTD,NOBA=" + PRS_FILE + ".NOBA,NODELIVERYVAN=" + PRS_FILE + ".NODELIVERYVAN,KOLI=" + PRS_FILE + ".KOLI,KONTAINER=" + PRS_FILE + ".KONTAINER;";
                                break;
                            case "DBT":
                                SQL += "DELETE FROM DBT" + Periode + " WHERE TOKO='" + TOKO + "' AND TANGGAL='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                SQL += "INSERT INTO DBT" + Periode + " (RECID,STATION,SHIFT,DOCNO,TANGGAL,TOKO,SALESRP,KDBANK,KD_BIN,NMBANK,NOMOR,DEBIT,AMBILTN,`Type`,TRX_ID,TRX_USER,STS_KRM,TP_TRAN,TRANDATE,TRANTIME,APPRO_CD,TRC_NO,TRM_ID,MERCH_ID,MEMBERID,EXPIRYDT,BATH_NO,KASIR_NAME,FEE,TIPE) ";
                                SQL += "SELECT RECID,STATION,SHIFT,DOCNO,STR_TO_DATE(TANGGAL,'%d-%m-%Y'),TOKO,SALESRP,KDBANK,KD_BIN,NMBANK,NOMOR,DEBIT,AMBILTN,`Type`,TRX_ID,TRX_USER,STS_KRM,TP_TRAN,STR_TO_DATE(TRANDATE,'%d-%m-%Y'),TRANTIME,APPRO_CD,TRC_NO,TRM_ID,MERCH_ID,MEMBERID,EXPIRYDT,BATH_NO,KASIR_NAME,FEE,TIPE FROM " + PRS_FILE + ";";
                                break;
                            case "CIN":
                                SQL += "DELETE FROM CIN" + Periode + " WHERE TOKO='" + TOKO + "' AND TGLTRXID='" + TANGGAL.ToString("yyyy-MM-dd") + "';";
                                SQL += "INSERT INTO CIN" + Periode + " (TOKO,SHIFT,STATION,DOCNO,PRDCD,QTY,CASHIN,FEE,NO_HP,NO_TRXID,TGLTRXID,JAMTRXID,KD_TRX,DBT,NO_DBT,JENIS,KET_JNS,ID_PEL,NO_TID,NO_PAN,NO_TRACE,NO_APPRO,NO_BATCH) ";
                                SQL += "SELECT '" + TOKO + "',SHIFT,STATION,DOCNO,PRDCD,QTY,CASHIN,FEE,NO_HP,NO_TRXID,STR_TO_DATE(TGLTRXID,'%d-%m-%Y'),JAMTRXID,KD_TRX,DBT,NO_DBT,JENIS,KET_JNS,ID_PEL,NO_TID,NO_PAN,NO_TRACE,NO_APPRO,NO_BATCH FROM " + PRS_FILE + ";";
                                break;
                        }
                        if (FILE != "LQS")
                        {
                            SQL += "UPDATE ABSENFILE" + Periode + " SET " + TG + "='1' WHERE SHOP='" + TOKO + "' AND FILE='" + FILE + "';";
                            SQL += "UPDATE ABSENFILE" + Periode + " SET TOTAL=";
                            for (int i = 1; i <= 31; i++)
                            {
                                if (i != 31)
                                {
                                    SQL += "IF(TG" + i.ToString() + "='',0,TG" + i.ToString() + ")+";
                                }
                                else
                                {
                                    SQL += "IF(TG" + i.ToString() + "='',0,TG" + i.ToString() + ")";
                                }
                            }
                        }
                        if (SQL != "")
                        {
                            using (MySqlConnection mConnection = db)
                            {
                                if (mConnection.State != ConnectionState.Open)
                                {
                                    mConnection.Open();
                                }
                                using (MySqlTransaction trans = mConnection.BeginTransaction())
                                {
                                    using (MySqlCommand myCmd = new MySqlCommand(SQL, mConnection, trans))
                                    {
                                        myCmd.CommandTimeout = 0;
                                        myCmd.CommandType = CommandType.Text;
                                        myCmd.ExecuteNonQuery();
                                        trans.Commit();
                                    }
                                }
                            }
                        }

                    }
                }
                return true;
            }
            catch (Exception err)
            {
                //Log(err.ToString() + SQL);
                //MessageBox.Show(err.Message);
                Log("PROSES FILE " + FILE + SQL, TOKO, err.ToString());
                return false;
            }
        }

        #endregion

        #region Import BRD DC
        public void ProsesBRD(DateTime Tgl)
        {
            SQL = "SELECT * FROM ABSENFILEDC" + Tgl.ToString("MMyyyy") + ";";
            using (DataTable dt = GetDataTable(SQL))
            {
                ListCekAbsenDC = dt;
                //Fungsi.Pesan(dt.Rows.Count.ToString(), "ERR");
            }
            DataTable BRD = new DataTable();
            OracleConnection ora = new OracleConnection(OracleDC);
            try
            {
                
                using (MySqlConnection mConnection = db)
                {
                    if (mConnection.State != ConnectionState.Open)
                    {
                        mConnection.Open();
                    }
                    using (MySqlTransaction trans = mConnection.BeginTransaction())
                    {
                        string TG = "TG" + int.Parse(Tgl.ToString("dd")).ToString();
                        string TGL = int.Parse(Tgl.ToString("dd")).ToString();
                        string PRS_FILE = "BRD" + Tgl.ToString("yyyyMMdd");
                        string PRD = Tgl.ToString("MMyyyy");
                        string ST = "STM" + Tgl.ToString("yyyy").Substring(3, 1) + Tgl.ToString("MMdd") + ".CSV";
                        SQL = "INSERT IGNORE ABSENFILEDC" + PRD + "(SHOP,`FILE`) VALUES('" + Cabang + "','BRD');";
                        SQL += "INSERT IGNORE ABSENFILEDC" + PRD + "(SHOP,`FILE`) VALUES('" + Cabang + "','STDC');";
                        SQL += "INSERT IGNORE ABSENFILEDC" + PRD + "(SHOP,`FILE`) VALUES('" + Cabang + "','PROD');";
                        SQL += "INSERT IGNORE ABSENFILEDC" + PRD + "(SHOP,`FILE`) VALUES('" + Cabang + "','SUP');";
                        SQL += "INSERT IGNORE ABSENFILEDC" + PRD + "(SHOP,`FILE`) VALUES('" + Cabang + "','MTOKO');";
                        SQL += "INSERT IGNORE ABSENFILEDC" + PRD + "(SHOP,`FILE`) VALUES('" + Cabang + "','MSTXHG');";
                        //SQL += "INSERT IGNORE ABSENFILEDC" + PRD + "(SHOP,`FILE`) VALUES('" + Cabang + "','SUP');";
                        using (MySqlCommand myCmd = new MySqlCommand(SQL, mConnection, trans))
                        {
                            myCmd.CommandTimeout = 0;
                            myCmd.CommandType = CommandType.Text;
                            myCmd.ExecuteNonQuery();
                            trans.Commit();
                        }
                        if (CekAbsenDC("BRD", Tgl))
                        {
                            //MessageBox.Show("BRD");
                            SQL = "SELECT TOKO, DOCNO,RTYPE, TO_CHAR(TGL_DOCNO, 'YYYY-MM-DD HH24:Mi:SS') AS TGL_DOCNO, NO_INV, TO_CHAR(TGL_INV, 'YYYY-MM-DD') AS TGL_INV, PLU, QTY  FROM DC_BRDTOKO_V ";
                            SQL += "WHERE TO_CHAR(TGL_DOCNO, 'YYYY-MM-DD')='" + Tgl.ToString("yyyy-MM-dd") + "'";
                            ora.Open();
                            OracleCommand cmdora = new OracleCommand(SQL, ora);
                            OracleDataAdapter da = new OracleDataAdapter(cmdora);
                            da.Fill(BRD);
                            DataTableToCSV(BRD, FolderTemporary + "\\" + PRS_FILE + ".CSV", "|");
                            if (Konversi_CSV("BRD", FolderTemporary + "\\" + PRS_FILE + ".CSV", "|",PRD))
                            {
                                //SQL = "DELETE QUICK FROM BRDDC" + PRD + " WHERE DATE(TGL_DOCNO)='" + Tgl.ToString("yyyy-MM-dd") + "';";
                                SQL = "INSERT IGNORE BRDDC" + PRD + " SELECT * FROM BRD" + PRD + "_temp;";
                                //SQL += "INSERT IGNORE TRANSDC" + PRD + "(SHOP,PRDCD,NPB" + TGL + ") SELECT TOKO,PLU,QTY FROM BRD" + Periode + "_temp WHERE RTYPE='NPB TOKO';";
                                //SQL += "UPDATE TRANSDC" + PRD + " A,BRD" + Periode + "_temp B SET A.NPB" + TGL + "=B.QTY WHERE A.SHOP=B.TOKO AND A.PRDCD=B.PLU;";
                                //SQL += "UPDATE PBSL" + PRD + " A, BRDDC" + PRD + " B SET A.QTY_NPB=B.QTY WHERE A.TOKO=B.TOKO AND A.PRDCD=B.PLU AND A.DOCNO=B.NO_INV;";
                                SQL += "UPDATE ABSENFILEDC" + PRD + " SET TG" + TGL + "='1' WHERE `FILE`='BRD';";
                                using (MySqlCommand myCmd = new MySqlCommand(SQL, mConnection, trans))
                                {
                                    myCmd.CommandTimeout = 0;
                                    myCmd.CommandType = CommandType.Text;
                                    myCmd.ExecuteNonQuery();
                                    //trans.Commit();
                                }
                            }
                        }
                        else
                        {
                            //MessageBox.Show("TIDAK BRD");
                        }
                        if (CekAbsenDC("STDC", Tgl))
                        {
                            if (DownloadV3(ST, FolderTemporary))
                            {
                                if (Konversi_CSV("STDC", FolderTemporary + "\\" + ST, ",",PRD))
                                {
                                    string ST_TMP = "STDC" + PRD + "_TEMP";
                                    SQL = "INSERT INTO STMASTDC" + PRD + "(PRDCD,LOKASI,`DIV`,QTY" + TGL + ",MAX" + TGL + ",LCOST) SELECT PRDCD,LOKASI,`DIV`,IF(QTY = '',0,QTY),IF(`MAX` = '',0,MAX),LCOST FROM " + ST_TMP + " ";
                                    SQL += "ON DUPLICATE KEY UPDATE QTY" + TGL + "=IF(" + ST_TMP + ".QTY = '',0," + ST_TMP + ".QTY),MAX" + TGL + "=IF(" + ST_TMP + ".MAX = '',0," + ST_TMP + ".MAX),LCOST=IF(" + ST_TMP + ".LCOST = '',0," + ST_TMP + ".LCOST);";
                                    //SQL += "INSERT INTO TRANSDC" + Periode + "(SHOP,PRDCD,NPB" + TGL + ") SELECT TOKO,PLU,QTY FROM " + ST_TMP + " WHERE RTYPE='NPB TOKO' ";
                                    //SQL += "ON DUPLICATE KEY UPDATE NPB" + TGL + "=BRD" + Periode + "_temp.QTY;";
                                    //Log("", "", SQL);
                                    //SQL += "UPDATE TRANSDC" + Periode + " A,STMASTDC" + PRD + " B SET A.STDC" + TGL + "=B.QTY" + TGL + " WHERE A.PRDCD=B.PRDCD;";
                                    //SQL += "UPDATE PBSL" + PRD + " A, STMASTDC" + PRD + " B SET A.QTY_STDC=B.QTY" + TGL + " WHERE A.PRDCD=B.PRDCD;";
                                    //SQL += "UPDATE PBSL" + PRD + " A, STOCKOUT" + PRD + " B SET A.STAUT=B.STAUT" + TGL + " WHERE A.PRDCD=B.PRDCD AND A.TOKO=B.SHOP;";
                                    SQL += "UPDATE ABSENFILEDC" + PRD + " SET TG" + TGL + "='1' WHERE `FILE`='STDC';";
                                    using (MySqlCommand myCmd = new MySqlCommand(SQL, mConnection, trans))
                                    {
                                        myCmd.CommandTimeout = 0;
                                        myCmd.CommandType = CommandType.Text;
                                        myCmd.ExecuteNonQuery();
                                        //trans.Commit();
                                    }
                                }
                            }
                        }
                        if (CekAbsenDC("PROD", Tgl))
                        {
                            if (DownloadV3("PRODMAST.CSV", FolderTemporary))
                            {
                                bool s = Konversi_CSV("PRODMASTDC", FolderTemporary + "\\PRODMAST.CSV", ",",PRD);
                                if (s)
                                {
                                    string PRD_TMP = "PRODMASTDC" + PRD + "_TEMP";
                                    //SQL = "INSERT IGNORE PRODMAST" + Periode + "(RECID, WARE_HOUSE, CAT_COD, PRDCD, `DESC`, SINGKAT, MERK, NAMA, FLAVOUR, KEMASAN, SIZE, BKP, DESC2, FRAC, UNIT, ACOST, RCOST, LCOST, PRICE_A, PRICE_B, PRICE_C, PRICE_D, PRICE_E, PRICE_F, `DIV`, CTGR, KONS, SUPCO, PTAG, TGL_HARGA1, TGL_HARGA2, TGL_HARGA3, TGL_HARGA4, TGL_HARGA5, TGL_HARGA6, REORDER, BRG_AKTIF, TGL_TAMBAH) ";
                                    //SQL += "SELECT RECID, WARE_HOUSE, CAT_COD, PRDCD, `DESC`, SINGKAT, MERK, NAMA, FLAVOUR, KEMASAN, SIZE, BKP, DESC2, FRAC, UNIT, ACOST, RCOST, LCOST, PRICE_A, PRICE_B, PRICE_C, PRICE_D, PRICE_E, PRICE_F, `DIV`, CTGR, KONS, SUPCO, PTAG, TGL_HARGA1, TGL_HARGA2, TGL_HARGA3, TGL_HARGA4, TGL_HARGA5, TGL_HARGA6, REORDER, BRG_AKTIF, TGL_TAMBAH FROM " + PRD_TMP + ";";

                                    //SQL += "UPDATE ABSENFILEDC" + PRD + " SET TG" + TGL + "='1' WHERE `FILE`='PROD';";
                                    
                                    DataTable dt = GetDataTable("SELECT * FROM " + PRD_TMP + ";");
                                    Log("UPDATE PRODMAST",PRD_TMP, "-->" + dt.Rows.Count.ToString() );
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        
                                        //MessageBox.Show(dr["PRDCD"].ToString());
                                        
                                        SQL = "insert into mprod" + PRD + "(prdcd,nama,merk,FLAVOUR,PTAG,SUPCO,kodenas,divs_code,dept_code,catg_code) values (@prdcd,@nama,@merk,@flavour,@ptag,@supco,@catcode,@div,@dep,@kat)";
                                        SQL += "on duplicate key update ";
                                        SQL += "nama=@nama,";
                                        SQL += "merk=@merk,";
                                        SQL += "FLAVOUR=@flavour,";
                                        SQL += "PTAG=@ptag,";
                                        SQL += "SUPCO=@supco,";
                                        SQL += "dept_code=@dep,";
                                        SQL += "catg_code=@kat,";
                                        SQL += "kodenas=@catcode,";
                                        SQL += "`divs_code`=@div;";
                                        //RECID, WARE_HOUSE, CAT_COD, PRDCD, DESC, SINGKAT, MERK, NAMA, FLAVOUR, KEMASAN, SIZE, BKP, DESC2, FRAC, UNIT, ACOST, RCOST, LCOST, PRICE_A, PRICE_B, PRICE_C, PRICE_D, PRICE_E, PRICE_F, DIV, CTGR, KONS, SUPCO, PTAG, TGL_HARGA1, TGL_HARGA2, TGL_HARGA3, TGL_HARGA4, TGL_HARGA5, TGL_HARGA6, REORDER, BRG_AKTIF
                                        SQL += "insert ignore prodmast" + PRD + "(WARE_HOUSE, CAT_COD, PRDCD, `DESC`, SINGKAT, MERK, NAMA, FLAVOUR, KEMASAN, SIZE, BKP, DESC2, FRAC, UNIT, ACOST, RCOST, LCOST, KONS) ";
                                        SQL += "VALUES (@WARE_HOUSE, @catcode, @prdcd, @DESC, @SINGKAT, @MERK, @NAMA1, @FLAVOUR, @KEMASAN, @SIZE, @BKP, @DESC2, @FRAC, @UNIT, @ACOST, @RCOST, @LCOST, @KONS)";
                                        SQL += "on duplicate key update ";
                                        SQL += "nama=@nama1,";
                                        SQL += "merk=@merk,";
                                        SQL += "FLAVOUR=@flavour,";
                                        SQL += "PTAG=@ptag,";
                                        SQL += "SUPCO=@supco,";
                                        SQL += "CAT_COD=@catcode,";
                                        SQL += "`DESC`=@DESC,";
                                        SQL += "WARE_HOUSE=@WARE_HOUSE,";
                                        SQL += "SINGKAT=@SINGKAT,";
                                        SQL += "KEMASAN=@KEMASAN,";
                                        SQL += "SIZE=@SIZE,";
                                        SQL += "BKP=@BKP,";
                                        SQL += "DESC2=@DESC2,";
                                        SQL += "FRAC=@FRAC,";
                                        SQL += "UNIT=@UNIT,";
                                        SQL += "ACOST=@ACOST,";
                                        SQL += "RCOST=@RCOST,";
                                        SQL += "LCOST=@LCOST,";
                                        SQL += "KONS=@KONS;";
                                        //cmd = new MySqlCommand(SQL, mConnection);
                                        
                                            
                                            
                                            //myCmd.ExecuteNonQuery();
                                            //trans.Commit();
                                        
                                        try
                                        {

                                            using (MySqlCommand myCmd = new MySqlCommand(SQL, mConnection))
                                            {
                                                myCmd.CommandTimeout = 0;
                                                myCmd.Parameters.AddWithValue("@prdcd", dr["PRDCD"].ToString());
                                                myCmd.Parameters.AddWithValue("@nama", dr["PRDCD"].ToString() + "-" + dr["DESC2"].ToString());
                                                myCmd.Parameters.AddWithValue("@nama1", dr["nama"].ToString());
                                                myCmd.Parameters.AddWithValue("@merk", dr["MERK"].ToString());
                                                myCmd.Parameters.AddWithValue("@flavour", dr["FLAVOUR"].ToString());
                                                myCmd.Parameters.AddWithValue("@ptag", dr["ptag"].ToString());
                                                myCmd.Parameters.AddWithValue("@supco", dr["supco"].ToString());
                                                myCmd.Parameters.AddWithValue("@catcode", dr["cat_cod"].ToString());
                                                myCmd.Parameters.AddWithValue("@div", int.Parse(dr["cat_cod"].ToString().Substring(0,1)).ToString("00"));
                                                myCmd.Parameters.AddWithValue("@dep", int.Parse(dr["cat_cod"].ToString().Substring(1, 2)).ToString("00"));
                                                myCmd.Parameters.AddWithValue("@kat", int.Parse(dr["cat_cod"].ToString().Substring(3, 2)).ToString("00"));
                                                myCmd.Parameters.AddWithValue("@DESC", dr["DESC"].ToString());
                                                myCmd.Parameters.AddWithValue("@WARE_HOUSE", dr["WARE_HOUSE"].ToString());
                                                myCmd.Parameters.AddWithValue("@SINGKAT", dr["SINGKAT"].ToString());
                                                myCmd.Parameters.AddWithValue("@KEMASAN", dr["KEMASAN"].ToString());
                                                myCmd.Parameters.AddWithValue("@SIZE", dr["SIZE"].ToString());
                                                myCmd.Parameters.AddWithValue("@BKP", dr["BKP"].ToString());
                                                myCmd.Parameters.AddWithValue("@DESC2", dr["DESC2"].ToString());
                                                myCmd.Parameters.AddWithValue("@FRAC", dr["FRAC"].ToString());
                                                myCmd.Parameters.AddWithValue("@UNIT", dr["UNIT"].ToString());
                                                myCmd.Parameters.AddWithValue("@ACOST", dr["ACOST"].ToString());
                                                myCmd.Parameters.AddWithValue("@RCOST", dr["RCOST"].ToString());
                                                myCmd.Parameters.AddWithValue("@LCOST", dr["LCOST"].ToString());
                                                myCmd.Parameters.AddWithValue("@KONS", dr["KONS"].ToString());
                                                myCmd.ExecuteNonQuery();
                                            }
                                        }
                                        catch (Exception err)
                                        {
                                            Log("PROSES PRODMAST " + SQL, "EIS", err.ToString());
                                        }
                                    }
                                    SQL = "update mprod" + PRD + " a , tblkatg b set a.divs_code=b.kddiv,a.dept_code=b.kddept,a.catg_code=b.kdkatg,a.catg_name=b.nmkatg where a.kodenas=b.catcod;";
                                    SQL += "update mprod" + PRD + " a , tbldept b set a.dept_name=b.dep_name where a.dept_code=b.dep_code;";
                                    SQL += "update mprod" + PRD + " a , tbldiv b set a.divs_name=b.nmdiv where a.divs_code=b.kddiv;";
                                   
                                    SQL += "UPDATE ABSENFILEDC" + PRD + " SET TG" + TGL + "='1' WHERE `FILE`='PROD';";
                                    using (MySqlCommand myCmd = new MySqlCommand(SQL, mConnection, trans))
                                    {
                                        myCmd.CommandTimeout = 0;
                                        myCmd.CommandType = CommandType.Text;
                                        myCmd.ExecuteNonQuery();
                                        //trans.Commit();
                                    }

                                }
                            }
                        }
                        if (CekAbsenDC("SUP", Tgl))
                        {
                            if (DownloadV3("SUPMAST.CSV", FolderTemporary))
                            {
                                bool s = Konversi_CSV("SUPMAST", FolderTemporary + "\\SUPMAST.CSV", ",",PRD);
                                if (s)
                                {
                                    string SUP_TMP = "SUPMAST" + PRD + "_TEMP";
                                    SQL = "INSERT INTO SUPMAST" + PRD + "(SUPCO,SNAMA,ALAMAT1,KOTA) SELECT SUPCO,SNAMA,SALM,SKOTA FROM " + SUP_TMP + " ";
                                    SQL += "ON DUPLICATE KEY UPDATE SNAMA="+ SUP_TMP + ".SNAMA,KOTA=" + SUP_TMP + ".SKOTA; ";
                                    //SQL += "INSERT INTO TRANSDC" + Periode + "(SHOP,PRDCD,NPB" + TGL + ") SELECT TOKO,PLU,QTY FROM " + ST_TMP + " WHERE RTYPE='NPB TOKO' ";
                                    //SQL += "ON DUPLICATE KEY UPDATE NPB" + TGL + "=BRD" + Periode + "_temp.QTY;";
                                    //Log("", "", SQL);
                                    //SQL += "UPDATE TRANSDC" + Periode + " A,STMASTDC" + PRD + " B SET A.STDC" + TGL + "=B.QTY" + TGL + " WHERE A.PRDCD=B.PRDCD;";
                                    //SQL += "UPDATE PBSL" + PRD + " A, STMASTDC" + PRD + " B SET A.QTY_STDC=B.QTY" + TGL + " WHERE A.PRDCD=B.PRDCD;";
                                    //SQL += "UPDATE PBSL" + PRD + " A, STOCKOUT" + PRD + " B SET A.STAUT=B.STAUT" + TGL + " WHERE A.PRDCD=B.PRDCD AND A.TOKO=B.SHOP;";
                                    SQL += "UPDATE ABSENFILEDC" + PRD + " SET TG" + TGL + "='1' WHERE `FILE`='SUP';";
                                    using (MySqlCommand myCmd = new MySqlCommand(SQL, mConnection, trans))
                                    {
                                        myCmd.CommandTimeout = 0;
                                        myCmd.CommandType = CommandType.Text;
                                        myCmd.ExecuteNonQuery();
                                        //trans.Commit();
                                    }
                                }
                            }
                        }
                        if (CekAbsenDC("MSTXHG", Tgl))
                        {
                            if (DownloadV3("MSTXHG" + Tgl.ToString("MM") + ".CSV", FolderTemporary))
                            {
                                bool s = Konversi_CSV("MSTXHG", FolderTemporary + "\\MSTXHG" + Tgl.ToString("MM") + ".CSV", ",",PRD);
                                if (s)
                                {
                                    string SUP_TMP = "MSTXHG" + PRD + "_TEMP";
                                    SQL = "TRUNCATE MSTXHG" + PRD + ";";
                                    SQL += "INSERT INTO MSTXHG" + PRD + "(TOKO,LOKASI,DOCNO,TANGGAL,PRDCD,QTY,PRICE,GROSS,RTYPE) SELECT TOKO,LOKASI,DOCNO,STR_TO_DATE(TANGGAL1,'%m/%d/%Y'),PRDCD,QTY,PRICE,GROSS,RTYPE FROM " + SUP_TMP + ";";
                                    //SQL += "ON DUPLICATE KEY UPDATE SNAMA=" + SUP_TMP + ".SNAMA,KOTA=" + SUP_TMP + ".SKOTA; ";
                                    //SQL += "INSERT INTO TRANSDC" + Periode + "(SHOP,PRDCD,NPB" + TGL + ") SELECT TOKO,PLU,QTY FROM " + ST_TMP + " WHERE RTYPE='NPB TOKO' ";
                                    //SQL += "ON DUPLICATE KEY UPDATE NPB" + TGL + "=BRD" + Periode + "_temp.QTY;";
                                    //Log("", "", SQL);
                                    //SQL += "UPDATE TRANSDC" + Periode + " A,STMASTDC" + PRD + " B SET A.STDC" + TGL + "=B.QTY" + TGL + " WHERE A.PRDCD=B.PRDCD;";
                                    //SQL += "UPDATE PBSL" + PRD + " A, STMASTDC" + PRD + " B SET A.QTY_STDC=B.QTY" + TGL + " WHERE A.PRDCD=B.PRDCD;";
                                    //SQL += "UPDATE PBSL" + PRD + " A, STOCKOUT" + PRD + " B SET A.STAUT=B.STAUT" + TGL + " WHERE A.PRDCD=B.PRDCD AND A.TOKO=B.SHOP;";
                                    SQL += "UPDATE ABSENFILEDC" + PRD + " SET TG" + TGL + "='1' WHERE `FILE`='MSTXHG';";
                                    using (MySqlCommand myCmd = new MySqlCommand(SQL, mConnection, trans))
                                    {
                                        myCmd.CommandTimeout = 0;
                                        myCmd.CommandType = CommandType.Text;
                                        myCmd.ExecuteNonQuery();
                                        //trans.Commit();
                                    }
                                }
                            }
                        }
                        if (CekAbsenDC("MTOKO", Tgl))
                        {
                            DataTable ORA = GetTokoOracle();
                            if (DataTableToCSV(ORA, FolderTemporary + "\\TOKO_ORA.CSV", "|"))
                            {
                                bool s = Konversi_CSV("TOKO_ORA", FolderTemporary + "\\TOKO_ORA.CSV", "|",PRD);
                                if (s)
                                {
                                    string TOKO_ORA = "TOKO_ORA" + PRD + "_TEMP";
                                    SQL = "INSERT INTO MTOKO" + PRD;
                                    SQL += " (TOKO_CODE, KD_ORGAN1, NM_ORGAN1, KD_ORGAN2, NM_ORGAN2, KD_ORGAN3, NM_ORGAN3, KD_ORGAN4, NM_ORGAN4, KD_ORGAN5, NM_ORGAN5, TYPE_TOKO, NAMA_TOKO, ALAMAT, JENIS, TGL_BUKA, STATUS,TIDAK_HITUNG_PB) ";
                                    SQL += "SELECT KODETOKO,'1','DIREKTUR OPR','','R S M','','BM " + Cabang + "',SUBSTR(AMGR_NAME,1,10),SUBSTR(AMGR_NAME, 12),SUBSTR(ASPV_NAME,1,10),SUBSTR(ASPV_NAME, 12),TOK_TYPE,CONCAT(" + TOKO_ORA + ".KODETOKO,'-'," + TOKO_ORA + ".NAMATOKO),ALAMATTOKO,IF(LEFT(KODETOKO,1)='T','0','1'),TGLBUKA,NULL,PB FROM " + TOKO_ORA +" ";
                                    SQL += "ON DUPLICATE KEY UPDATE NAMA_TOKO=CONCAT(" + TOKO_ORA + ".KODETOKO,'-'," + TOKO_ORA + ".NAMATOKO),ALAMAT=" + TOKO_ORA + ".ALAMATTOKO,KD_ORGAN4=SUBSTR(" + TOKO_ORA + ".AMGR_NAME,1,10),KD_ORGAN5=SUBSTR(" + TOKO_ORA + ".ASPV_NAME,1,10),NM_ORGAN4=SUBSTR(" + TOKO_ORA + ".AMGR_NAME, 12),NM_ORGAN5=SUBSTR(" + TOKO_ORA + ".ASPV_NAME, 12),TIDAK_HITUNG_PB=" + TOKO_ORA + ".PB;";
                                    //SQL += "UPDATE MTOKO" + PRD + " SET KD_ORGAN4='0000000001',KD_ORGAN5='0000000002',NM_ORGAN4='DEVELOPMENT',NM_ORGAN5='DEVELOPMENT' WHERE TOKO_CODE='TZSW';";
                                    //SQL += "UPDATE MTOKO" + PRD + " A, WILAYAH B SET A.KOTA=B.KABUPATEN WHERE A.TOKO_CODE=B.TOKO;";
                                    SQL += "UPDATE MTOKO" + PRD + " SET NM_ORGAN4='UNKNOWN',NM_ORGAN5='UNKNOWN' WHERE KD_ORGAN4='00000XXX-U' OR KD_ORGAN5='00000XXX-U';";
                                    SQL += "UPDATE MTOKO" + PRD + " SET NM_ORGAN5=CONCAT('VACANT ',RIGHT(NM_ORGAN5,1)) WHERE KD_ORGAN5 LIKE '%V%';";
                                    SQL += "DROP TABLE IF EXISTS TOKO_ORACLE;";
                                    SQL += "CREATE TABLE TOKO_ORACLE SELECT * FROM " + TOKO_ORA + ";";
                                    SQL += "UPDATE ABSENFILEDC" + PRD + " SET TG" + TGL + "='1' WHERE `FILE`='MTOKO';";
                                    using (MySqlCommand myCmd = new MySqlCommand(SQL, mConnection, trans))
                                    {
                                        myCmd.CommandTimeout = 0;
                                        myCmd.CommandType = CommandType.Text;
                                        myCmd.ExecuteNonQuery();
                                        //trans.Commit();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Log("PROSES BRD DC " + SQL, "EIS", err.ToString());
            }
            finally
            {
                ora.Close();
            }
        }

        #region Proses Sinkron PB VS NPB
        public void ProsesPBvsNPB()
        {
            try
            {
                using (MySqlConnection mConnection = new MySqlConnection(ServerMySQL))
                {
                    if (mConnection.State != ConnectionState.Open)
                    {
                        mConnection.Open();
                    }
                    SQL = "SELECT * FROM HISPBDOC" + Periode + " WHERE PROSES != '1';";
                    cmd = new MySqlCommand(SQL, mConnection);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    DataTable his = new DataTable();
                    da.Fill(his);
                    //DataTable his = GetDataTable(SQL);
                    if (his.Rows.Count != 0)
                    {
                        foreach (DataRow dr in his.Rows)
                        {
                            string TGL = DateTime.Parse(dr["TANGGAL"].ToString()).ToString("yyyyMMdd");
                            int TGL1 = int.Parse(TGL.Substring(6, 2));
                            string TG = TGL1.ToString();
                            string DOCNO = dr["DOCNO"].ToString();
                            string TOKO = dr["SHOP"].ToString();
                            using (DataTable npb = GetDataNPB(DOCNO, TOKO))
                            {
                                if (npb.Rows.Count != 0)
                                {
                                    //MessageBox.Show(npb.Rows.Count.ToString() + "-" + TOKO);
                                    foreach (DataRow nx in npb.Rows)
                                    {
                                        if (mConnection.State != ConnectionState.Open)
                                        {
                                            mConnection.Open();
                                        }
                                        SQL = "UPDATE HISPB" + Periode + " SET NPB" + TGL1 + "='" + nx["QTY"] + "' WHERE SHOP='" + TOKO + "' AND PRDCD='" + nx["PLU"] + "';";
                                        using (MySqlTransaction trans = mConnection.BeginTransaction())
                                        {
                                            using (MySqlCommand myCmd = new MySqlCommand(SQL, mConnection, trans))
                                            {
                                                myCmd.CommandTimeout = 0;
                                                myCmd.CommandType = CommandType.Text;
                                                myCmd.ExecuteNonQuery();
                                                trans.Commit();
                                            }
                                        }
                                    }
                                    SQL = "UPDATE HISPBDOC" + Periode + " SET PROSES='1' WHERE SHOP='" + TOKO + "' AND TANGGAL='" + TGL + "' AND DOCNO='" + DOCNO + "';";
                                    using (MySqlTransaction trans = mConnection.BeginTransaction())
                                    {
                                        using (MySqlCommand myCmd = new MySqlCommand(SQL, mConnection, trans))
                                        {
                                            myCmd.CommandTimeout = 0;
                                            myCmd.CommandType = CommandType.Text;
                                            myCmd.ExecuteNonQuery();
                                            trans.Commit();
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Log("PROSES SINKRON PB VS NPB", "EIS", err.ToString() + SQL);
            }
        }

        public void UpdateStockOutDry(DateTime Tgl)
        {
            try
            {
                using (MySqlConnection mConnection = db)
                {
                    SQL = "SELECT TOKO FROM SLP WHERE TANGGAL='" + Tgl.ToString("yyyyMMdd") + "' AND STK_OUT_DRY IS NULL;";
                    //Log("SS", "XX", SQL);
                    string TG = "TG" + int.Parse(Tgl.ToString("dd")).ToString();
                    string TGL = int.Parse(Tgl.ToString("dd")).ToString();
                    string PRD = Tgl.ToString("MMyyyy");
                    using (DataTable STO = GetDataTable(SQL))
                    {
                        if (STO.Rows.Count != 0)
                        {
                            foreach (DataRow dr in STO.Rows)
                            {
                                if (mConnection.State != ConnectionState.Open)
                                {
                                    mConnection.Open();
                                }
                                string TOKO = dr["TOKO"].ToString();
                                //SQL = "UPDATE SLP SET STK_OUT_DRY=";
                                //SQL = "UPDATE SLP SET STK_OUT_DRY=(SELECT SUM(staut" + TGL + ") FROM STOCKOUT" + PRD + " WHERE PRDCD NOT IN(SELECT PRDCD FROM PRODMAST" + PRD + " WHERE KONS != 'Y' OR PTAG NOT IN('T','V') OR CAT_COD NOT LIKE '4%') AND TOKO='" + TOKO + "' AND TANGGAL='" + Tgl.ToString("yyyy-MM-dd") + "');";
                                SQL = "UPDATE SLP SET STK_OUT_DRY=(SELECT SUM(staut" + TGL + ") FROM STOCKOUT" + PRD + " WHERE PRDCD NOT IN(SELECT PRDCD FROM PRODMAST" + PRD + " WHERE KONS = 'Y' OR PTAG IN('T','V') OR CAT_COD LIKE '4%') AND SHOP='" + TOKO + "') WHERE TOKO='" + TOKO + "' AND TANGGAL='" + Tgl.ToString("yyyy-MM-dd") + "';";
                                using (MySqlTransaction trans = mConnection.BeginTransaction())
                                {
                                    using (MySqlCommand myCmd = new MySqlCommand(SQL, mConnection, trans))
                                    {
                                        myCmd.CommandTimeout = 0;
                                        myCmd.CommandType = CommandType.Text;
                                        myCmd.ExecuteNonQuery();
                                        trans.Commit();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Log("PROSES UPDATE STOCK OUT", "EIS", err.ToString() + SQL);
            }

        }

        private DataTable GetDataNPB(string INV,string TOKO)
        {
            DataTable NPB = new DataTable();
            try
            {
                using (OracleConnection mConnection = new OracleConnection(OracleDC))
                {
                    SQL = "SELECT TOKO, NO_INV, PLU, QTY FROM DC_BRDTOKO_V ";
                    SQL += "WHERE NO_INV='" + INV + "' AND TOKO='" + TOKO + "' AND RTYPE='NPB TOKO'";
                    mConnection.Open();
                    OracleCommand cmdora = new OracleCommand(SQL, mConnection);
                    OracleDataAdapter da = new OracleDataAdapter(cmdora);
                    da.Fill(NPB);
                }
            }
            catch (Exception err)
            {
                Log("PROSES GET DATA NPB", "EIS", err.ToString() + SQL);
            }
            return NPB;
        }
        #endregion


        #endregion

        public bool DataTableToCSV(DataTable dtDataTable, string strFilePath, string koma)
        {

            StreamWriter sw = new StreamWriter(strFilePath, false);
            try
            {
                for (int i = 0; i < dtDataTable.Columns.Count; i++)
                {
                    sw.Write(dtDataTable.Columns[i]);
                    if (i < dtDataTable.Columns.Count - 1)
                    {
                        sw.Write(koma);
                    }
                }
                sw.Write(sw.NewLine);
                foreach (DataRow dr in dtDataTable.Rows)
                {
                    for (int i = 0; i < dtDataTable.Columns.Count; i++)
                    {
                        if (!Convert.IsDBNull(dr[i]))
                        {
                            string value = dr[i].ToString();
                            if (value.Contains(koma))
                            {
                                value = String.Format("\"{0}\"", value);
                                sw.Write(value);
                            }
                            else
                            {
                                sw.Write(ToISO(dr[i].ToString()));
                            }
                        }
                        if (i < dtDataTable.Columns.Count - 1)
                        {
                            sw.Write(koma);
                        }
                    }
                    sw.Write(sw.NewLine);
                }
                sw.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static string ToISO(string tg)
        {
            var result = "";
            DateTime dt = new DateTime();
            try
            {
                if (tg.Length > 8)
                {
                    dt = DateTime.Parse(tg);
                    result = dt.ToString("yyyy-MM-dd HH:mm:ss");
                }
                else
                {
                    result = tg;
                }
            }
            catch (Exception)
            {
                result = tg;
            }
            return result;
        }

        private byte[] downloadedData;
        public bool Download(string filename,string localfile)
        {
            string host = "ftp://" + FtpDC + "/DC/" + Cabang.ToUpper() + "/ORA";
            string user = "ftp" + Cabang.ToLower();
            string pass = "xftp" + Cabang.ToLower() + "x";
            downloadedData = new byte[0];
            string ftp = host + "/" + filename;
            try
            {
                //Optional
                //MessageBox.Show(ftp);
                //Create FTP request
                //Note: format is ftp://server.com/file.ext
                FtpWebRequest request = FtpWebRequest.Create(ftp) as FtpWebRequest;
                //Optional
                Application.DoEvents();
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(user, pass);
                request.UsePassive = false;
                request.UseBinary = true;
                request.KeepAlive = true; //don't close the connection
                int dataLength = (int)request.GetResponse().ContentLength;
                //Optional
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create(ftp) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(user, pass);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false; //close the connection when done

                //Set up progress bar
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                //Note: adjust the streams here to download directly to the hard drive

                Uri uri = new Uri(ftp);
                string fl = Path.GetFileName(uri.LocalPath);
                MemoryStream memStream = new MemoryStream();
                FileStream writeStream = new FileStream(localfile + "\\" + fl, FileMode.Create);
                byte[] buffer = new byte[1024]; //downloads in chuncks
                while (true)
                {
                    Application.DoEvents(); //prevent application from crashing
                    //Try to read the data
                    int bytesRead = reader.Read(buffer, 0, buffer.Length);
                    if (bytesRead == 0)
                    {
                        //Nothing was read, finished downloading

                        break;
                    }
                    else
                    {
                        //Write the downloaded data
                        //memStream.Write(buffer, 0, bytesRead);
                        writeStream.Write(buffer, 0, bytesRead);
                        //Update the progress bar
                    }
                }

                //Convert the downloaded stream to a byte array
                downloadedData = memStream.ToArray();
                //Clean up
                reader.Close();
                memStream.Close();
                response.Close();
                writeStream.Close();
                //MessageBox.Show("Downloaded Successfully");
                return true;
            }
            catch (Exception err)
            {
                Log("DOWNLOAD", filename, err.Message + ftp);
                return false;
            }
        }

        public bool DownloadV2(string filename, string localfile)
        {
            try
            {
                //string host = "ftp://" + FtpDC + "/DC/" + Cabang.ToUpper() + "/ORA/" + filename;
                string user = "ftp" + Cabang.ToLower();
                string pass = "xftp" + Cabang.ToLower() + "x";
                string inputfilepath = localfile;
                //string ftphost = "192.168.39.169";
                //string ftpfilepath = "/" + Cabang.ToUpper() + "/DATA/" + filename;
                string ftpfullpath = "ftp://" + FtpDC + "/DC/" + Cabang.ToUpper() + "/ORA/" + filename;
                using (WebClient request = new WebClient())
                {
                    request.Credentials = new NetworkCredential(user, pass);
                    byte[] fileData = request.DownloadData(ftpfullpath);

                    using (FileStream file = File.Create(inputfilepath))
                    {
                        file.Write(fileData, 0, fileData.Length);
                        file.Close();
                    }
                    //MessageBox.Show("Download Complete");
                }
                return true;
            }
            catch (Exception err)
            {
                Log("DOWNLOAD", filename, err.Message);
                //Console.WriteLine(err.ToString());
                //Console.ReadLine();
                return false;
            }
        }

        public bool DownloadV3(string filename, string localfile)
        {
            try
            {
                string user = "ftp" + Cabang.ToLower();
                string pass = "xftp" + Cabang.ToLower() + "x";
                FTPConnection ftp = new FTPConnection();
                ftp.ServerAddress = FtpDC;
                ftp.UserName = user;
                ftp.Password = pass;
                ftp.Connect();
                ftp.ChangeWorkingDirectory("DC/" + Cabang.ToUpper() + "/ORA");
                ftp.DownloadFile(localfile, filename);
                ftp.Close();
                return true;
            }
            catch (Exception err)
            {
                Log("DOWNLOAD", filename, err.Message);
                return false;
                //Console.WriteLine(ex.ToString());
            }
        }

        private  DataTable GetTokoOracle()
        {
            string Server_Ora = "Data Source = (DESCRIPTION =  (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.2.82)(PORT = 1521)) ) (CONNECT_DATA = (SERVICE_NAME = dbidmho) ));User Id=hodb;password=idm2000;";
            DataTable dt = new DataTable();
            OracleConnection ora = new OracleConnection(Server_Ora);
            var SQL = "";
            try
            {
                SQL = "SELECT tok_code AS Kodetoko,SUBSTR(NVL(a.tok_name,' '),1,25)||'/'||NVL(b.str_kode_pajak,' ') AS NamaToko," +
                    " SUBSTR(NVL(a.tok_alamat,' '),1,84) AS AlamatToko, a.tok_kirim AS KodeGudang," +
                    " SUBSTR(c.aspv_code,1,5) AS kdorgan,Tok_tgl_buka AS tglBuka,tok_kota as KotaToko," +
                    " Tok_type,tok_type_hrg,tok_type_rak,tok_jadwal_buah,tok_jadwal_elpiji,TOK_TIDAK_HITUNG_PB as pb," +
                    " tok_jadwal_conv,tok_tgl_pkm,tok_cid_code,tok_24,tok_convinience,tok_old_code,tok_tgl_tutup," +
                    " tok_tgl_berlaku_hrg, tok_tgl_berlaku_cid,f.tgl_fk_tok_id," +
                    " G.aspv_name,G.amgr_name,a.tok_flag_disp" +
                    " FROM HO_TOKO_T a,ho_full_strata_v b," +
                    " (SELECT NVL(a1,b1) AS aspv_id," +
                    " NVL(a2,b2) AS aspv_code," +
                    " NVL(a3,b3) AS aspv_name" +
                    " FROM" +
                    " (SELECT aspv_id AS b1,aspv_code AS b2,aspv_name AS b3" +
                    " FROM ho_full_organ_v) a," +
                    " (SELECT aspv_id AS a1,aspv_code AS a2,aspv_name AS a3" +
                    " FROM ho_full_organ_usulan_v) b" +
                    " WHERE a.b1=b.a1 (+)" +
                    " ) c," +
                    " Ho_full_lokasi_v d, HO_PEMILIK_TOKO e," +
                    " (select tgl_fk_tok_id from ho_tgltutup_t where tgl_tutup = trunc(sysdate)) f," +
                    " HO_FULL_ORGAN_V G" +
                    " WHERE a.tok_fk_str_dc_id = b.dc_id (+)" +
                    " AND a.tok_fk_org_aspv_id = c.aspv_id (+)" +
                    " AND a.tok_fk_lok_kodepos_id = d.kodepos_id (+)" +
                    " AND a.tok_code_frc = e.kode(+)" +
                    " AND a.tok_id = F.tgl_fk_tok_id(+)" +
                    " AND nvl(tok_recid,' ')<>'1'" +
                    " AND nvl(tok_Kirim,' ')<>' '" +
                    //" AND tok_tgl_buka <=trunc(sysdate)" +
                    " AND TOK_KIRIM='" + Cabang +"'" +
                    " AND A.TOK_FK_ORG_ASPV_ID = G.ASPV_ID(+)" +
                    " ORDER BY a.tok_code";
                ora.Open();
                OracleCommand cmd = new OracleCommand(SQL, ora);
                OracleDataAdapter da = new OracleDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception err)
            {
                //Log("PROSES AMBIL DATA ORACLE " + err.ToString() + "\n\r" + SQL, true);
            }
            finally
            {
                ora.Close();
            }
            return dt;
        }

    }
}
