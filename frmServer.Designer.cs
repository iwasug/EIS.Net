﻿namespace EIS.Net
{
    partial class frmServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nsTheme1 = new NSTheme();
            this.nsControlButton1 = new NSControlButton();
            this.nsGroupBox1 = new NSGroupBox();
            this.nsLabel3 = new NSLabel();
            this.txtServer = new NSTextBox();
            this.nsLabel1 = new NSLabel();
            this.txtPort = new NSTextBox();
            this.nsLabel2 = new NSLabel();
            this.txtUser = new NSTextBox();
            this.nsLabel4 = new NSLabel();
            this.txtPassword = new NSTextBox();
            this.nsLabel5 = new NSLabel();
            this.txtDatabase = new NSTextBox();
            this.btnCekKoneksi = new NSButton();
            this.nsTheme1.SuspendLayout();
            this.nsGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // nsTheme1
            // 
            this.nsTheme1.AccentOffset = 0;
            this.nsTheme1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.nsTheme1.BorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.nsTheme1.Colors = new Bloom[0];
            this.nsTheme1.Controls.Add(this.nsControlButton1);
            this.nsTheme1.Controls.Add(this.nsGroupBox1);
            this.nsTheme1.Customization = "";
            this.nsTheme1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nsTheme1.Font = new System.Drawing.Font("Verdana", 8F);
            this.nsTheme1.Image = null;
            this.nsTheme1.Location = new System.Drawing.Point(0, 0);
            this.nsTheme1.Movable = true;
            this.nsTheme1.Name = "nsTheme1";
            this.nsTheme1.NoRounding = false;
            this.nsTheme1.Padding = new System.Windows.Forms.Padding(10, 40, 10, 10);
            this.nsTheme1.Sizable = true;
            this.nsTheme1.Size = new System.Drawing.Size(239, 394);
            this.nsTheme1.SmartBounds = true;
            this.nsTheme1.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.nsTheme1.TabIndex = 0;
            this.nsTheme1.Text = "Setup MySQL Server";
            this.nsTheme1.TransparencyKey = System.Drawing.Color.Empty;
            this.nsTheme1.Transparent = false;
            // 
            // nsControlButton1
            // 
            this.nsControlButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nsControlButton1.ControlButton = NSControlButton.Button.Close;
            this.nsControlButton1.Location = new System.Drawing.Point(212, 6);
            this.nsControlButton1.Margin = new System.Windows.Forms.Padding(0);
            this.nsControlButton1.MaximumSize = new System.Drawing.Size(18, 20);
            this.nsControlButton1.MinimumSize = new System.Drawing.Size(18, 20);
            this.nsControlButton1.Name = "nsControlButton1";
            this.nsControlButton1.Size = new System.Drawing.Size(18, 20);
            this.nsControlButton1.TabIndex = 1;
            this.nsControlButton1.Text = "nsControlButton1";
            // 
            // nsGroupBox1
            // 
            this.nsGroupBox1.Controls.Add(this.btnCekKoneksi);
            this.nsGroupBox1.Controls.Add(this.txtDatabase);
            this.nsGroupBox1.Controls.Add(this.txtPassword);
            this.nsGroupBox1.Controls.Add(this.txtUser);
            this.nsGroupBox1.Controls.Add(this.txtPort);
            this.nsGroupBox1.Controls.Add(this.txtServer);
            this.nsGroupBox1.Controls.Add(this.nsLabel5);
            this.nsGroupBox1.Controls.Add(this.nsLabel4);
            this.nsGroupBox1.Controls.Add(this.nsLabel2);
            this.nsGroupBox1.Controls.Add(this.nsLabel1);
            this.nsGroupBox1.Controls.Add(this.nsLabel3);
            this.nsGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nsGroupBox1.DrawSeperator = false;
            this.nsGroupBox1.ForeColor = System.Drawing.Color.White;
            this.nsGroupBox1.Location = new System.Drawing.Point(10, 40);
            this.nsGroupBox1.Name = "nsGroupBox1";
            this.nsGroupBox1.Padding = new System.Windows.Forms.Padding(10, 40, 10, 10);
            this.nsGroupBox1.Size = new System.Drawing.Size(219, 344);
            this.nsGroupBox1.SubTitle = "MySQL Server";
            this.nsGroupBox1.TabIndex = 0;
            this.nsGroupBox1.Text = "nsGroupBox1";
            this.nsGroupBox1.Title = "Setup";
            // 
            // nsLabel3
            // 
            this.nsLabel3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel3.Location = new System.Drawing.Point(13, 43);
            this.nsLabel3.Name = "nsLabel3";
            this.nsLabel3.Size = new System.Drawing.Size(194, 13);
            this.nsLabel3.TabIndex = 2;
            this.nsLabel3.Text = "nsLabel1";
            this.nsLabel3.Value1 = "Ip Server :";
            this.nsLabel3.Value2 = "";
            // 
            // txtServer
            // 
            this.txtServer.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtServer.Location = new System.Drawing.Point(13, 62);
            this.txtServer.MaxLength = 32767;
            this.txtServer.Multiline = false;
            this.txtServer.Name = "txtServer";
            this.txtServer.ReadOnly = false;
            this.txtServer.Size = new System.Drawing.Size(194, 23);
            this.txtServer.TabIndex = 3;
            this.txtServer.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtServer.UseSystemPasswordChar = false;
            // 
            // nsLabel1
            // 
            this.nsLabel1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel1.Location = new System.Drawing.Point(13, 91);
            this.nsLabel1.Name = "nsLabel1";
            this.nsLabel1.Size = new System.Drawing.Size(194, 13);
            this.nsLabel1.TabIndex = 2;
            this.nsLabel1.Text = "nsLabel1";
            this.nsLabel1.Value1 = "Port :";
            this.nsLabel1.Value2 = "";
            // 
            // txtPort
            // 
            this.txtPort.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtPort.Location = new System.Drawing.Point(13, 110);
            this.txtPort.MaxLength = 32767;
            this.txtPort.Multiline = false;
            this.txtPort.Name = "txtPort";
            this.txtPort.ReadOnly = false;
            this.txtPort.Size = new System.Drawing.Size(194, 23);
            this.txtPort.TabIndex = 4;
            this.txtPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtPort.UseSystemPasswordChar = false;
            // 
            // nsLabel2
            // 
            this.nsLabel2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel2.Location = new System.Drawing.Point(13, 139);
            this.nsLabel2.Name = "nsLabel2";
            this.nsLabel2.Size = new System.Drawing.Size(194, 13);
            this.nsLabel2.TabIndex = 2;
            this.nsLabel2.Text = "nsLabel1";
            this.nsLabel2.Value1 = "User :";
            this.nsLabel2.Value2 = "";
            // 
            // txtUser
            // 
            this.txtUser.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtUser.Location = new System.Drawing.Point(13, 158);
            this.txtUser.MaxLength = 32767;
            this.txtUser.Multiline = false;
            this.txtUser.Name = "txtUser";
            this.txtUser.ReadOnly = false;
            this.txtUser.Size = new System.Drawing.Size(194, 23);
            this.txtUser.TabIndex = 5;
            this.txtUser.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtUser.UseSystemPasswordChar = false;
            // 
            // nsLabel4
            // 
            this.nsLabel4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel4.Location = new System.Drawing.Point(13, 187);
            this.nsLabel4.Name = "nsLabel4";
            this.nsLabel4.Size = new System.Drawing.Size(194, 13);
            this.nsLabel4.TabIndex = 2;
            this.nsLabel4.Text = "nsLabel1";
            this.nsLabel4.Value1 = "Password :";
            this.nsLabel4.Value2 = "";
            // 
            // txtPassword
            // 
            this.txtPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtPassword.Location = new System.Drawing.Point(13, 206);
            this.txtPassword.MaxLength = 32767;
            this.txtPassword.Multiline = false;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.ReadOnly = false;
            this.txtPassword.Size = new System.Drawing.Size(194, 23);
            this.txtPassword.TabIndex = 6;
            this.txtPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // nsLabel5
            // 
            this.nsLabel5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsLabel5.Location = new System.Drawing.Point(13, 235);
            this.nsLabel5.Name = "nsLabel5";
            this.nsLabel5.Size = new System.Drawing.Size(194, 13);
            this.nsLabel5.TabIndex = 2;
            this.nsLabel5.Text = "nsLabel1";
            this.nsLabel5.Value1 = "Database :";
            this.nsLabel5.Value2 = "";
            // 
            // txtDatabase
            // 
            this.txtDatabase.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtDatabase.Location = new System.Drawing.Point(13, 254);
            this.txtDatabase.MaxLength = 32767;
            this.txtDatabase.Multiline = false;
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.ReadOnly = false;
            this.txtDatabase.Size = new System.Drawing.Size(193, 23);
            this.txtDatabase.TabIndex = 7;
            this.txtDatabase.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtDatabase.UseSystemPasswordChar = false;
            // 
            // btnCekKoneksi
            // 
            this.btnCekKoneksi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCekKoneksi.Location = new System.Drawing.Point(13, 283);
            this.btnCekKoneksi.Name = "btnCekKoneksi";
            this.btnCekKoneksi.Size = new System.Drawing.Size(193, 48);
            this.btnCekKoneksi.TabIndex = 8;
            this.btnCekKoneksi.Text = "Cek Koneksi";
            this.btnCekKoneksi.Click += new System.EventHandler(this.btnCekKoneksi_Click);
            // 
            // frmServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(239, 394);
            this.Controls.Add(this.nsTheme1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmServer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmServer";
            this.nsTheme1.ResumeLayout(false);
            this.nsGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private NSTheme nsTheme1;
        private NSGroupBox nsGroupBox1;
        private NSControlButton nsControlButton1;
        private NSButton btnCekKoneksi;
        private NSTextBox txtDatabase;
        private NSTextBox txtPassword;
        private NSTextBox txtUser;
        private NSTextBox txtPort;
        private NSTextBox txtServer;
        private NSLabel nsLabel5;
        private NSLabel nsLabel4;
        private NSLabel nsLabel2;
        private NSLabel nsLabel1;
        private NSLabel nsLabel3;
    }
}